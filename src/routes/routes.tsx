import { Route, Routes } from "react-router-dom";
import { Home, Profile, ServiceView } from "../features";
import OAuth2RedirectHandler from "../features/Auth/OAuth2RedirectHandler";
import MyOrder from "../features/MyOrder/MyOrder";
import OrderDetail from "../features/OrderDetail/OrderDetail";
import Layout from "../Layout";
import { authRoles as Role } from "../shared/constants/authRoles";
import PrivateSeller from "../features/seller/PrivateSeller";
import PrivateManager from "../features/manager/PrivateManager";
import { lazy, Suspense } from "react";
import PaymentHandler from "../features/Purchase/Component/PaymentHandler";
import ViewUserDetail from "../features/UserList/Component/ViewUserDetail";
import ViewRefundDetail from "../features/RefundRequest/Component/ViewRefundDetail";

//
const SearchResultList = lazy(
  () => import("./../features/Searching/SearchResultList")
);
const ViewShopDetail = lazy(
  () => import("./../features/Shop/ViewShopDetail/ViewShopDetail")
);
const Cart = lazy(() => import("./../features/Cart"));
const Purchase = lazy(() => import("./../features/Purchase"));
const AboutUs = lazy(() => import("./../features/AboutUs"));
const NotFound = lazy(() => import("./../features/NotFound/NotFound"));

// Seller Pages
const NewService = lazy(() => import("./../features/seller/NewService"));
const UpdateService = lazy(() => import("./../features/seller/UpdateService"));
const ShopServiceList = lazy(
  () => import("./../features/seller/ShopServiceList")
);
const OrderRecordList = lazy(
  () => import("./../features/seller/OrderRecordList")
);
const OrderRecordDetail = lazy(
  () => import("./../features/seller/OrderRecordDetail")
);

//Mod Admin Pages
const ModeratorList = lazy(() => import("./../features/ModeratorList"));
const ShopList = lazy(() => import("./../features/ShopList"));
const UserList = lazy(() => import("./../features/UserList"));
// const ViewUserDetail = lazy(() => import("./../features/ViewUserDetail"));
const RefundRequestList = lazy(() => import("./../features/RefundRequest"));
// const ViewRefundDetail = lazy(() => import("./../features/ViewRefundDetail"));
const ServiceAppraisalList = lazy(
  () => import("./../features/ServiceAppraisalList")
);

const routes: React.FC = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/oauth2/redirect" element={<OAuth2RedirectHandler />} />
          <Route path="/payment/redirect" element={<PaymentHandler />} />
          <Route path="/about" element={<AboutUs />} />
          <Route path="/service/:serviceId" element={<ServiceView />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/purchase" element={<Purchase />} />
          <Route path="/shop/:shopId" element={<ViewShopDetail />} />
          <Route path="/search" element={<SearchResultList />} />
          <Route path="/myorder" element={<MyOrder />} />
          <Route path="/orderdetail/:orderId" element={<OrderDetail />} />
          <Route path="*" element={<NotFound />} />
        </Route>

        <Route
          path="seller"
          element={<PrivateSeller acceptedRole={Role.SELLER} />}
        >
          <Route index element={<ShopServiceList />} />
          <Route path="service/list" element={<ShopServiceList />} />
          <Route path="service/new" element={<NewService />} />
          <Route path="service/:serviceId" element={<UpdateService />} />
          <Route path="order/list" element={<OrderRecordList />} />
          <Route path="order/:orderId" element={<OrderRecordDetail />} />
        </Route>

        <Route
          path="manager"
          element={<PrivateManager acceptedRole={Role.MODERATOR} />}
        >
          <Route index element={<UserList />} />
          <Route path="modlist" element={<ModeratorList />} />
          <Route path="shoplist" element={<ShopList />} />
          <Route path="userlist" element={<UserList />} />
          <Route path="user/:userId" element={<ViewUserDetail />} />
          <Route path="refundrequestlist" element={<RefundRequestList />} />
          <Route path="refund/:id" element={<ViewRefundDetail />} />
          <Route
            path="serviceappraisallist/:shopid"
            element={<ServiceAppraisalList />}
          />
          <Route
            path="serviceappraisallist"
            element={<ServiceAppraisalList />}
          />
        </Route>
      </Routes>
    </Suspense>
  );
};

export default routes;
