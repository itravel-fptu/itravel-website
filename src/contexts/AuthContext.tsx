import React, { useEffect, useState } from "react";

import { IUser } from "../shared/types/UserType";

import { getCurrentUser } from "../shared/services/AuthService";
import { ACCESS_TOKEN } from "../constants";
import { AxiosError } from "axios";

interface IAuthContext {
  user: IUser | null;
  login: Function;
  logout: Function;
  loadCurrentlyLoggedInUser(game: Function): Promise<void>;
  isLoading: boolean;
}

const AuthContext = React.createContext<IAuthContext>(null!);

export const AuthProvider = ({ children }: any) => {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

function useProvideAuth(): IAuthContext {
  const [user, setUser] = useState<IUser | null>(null!);
  const [isLoading, setIsLoading] = useState(false);

  const login = async (callback: Function) => {
    await getCurrentUser<IUser[]>().then((res) => {
      console.log(res.data[0]);
      setUser(res.data[0]);
      console.log("User", user);
    });
  };

  const logout = (callback: Function) => {
    localStorage.removeItem(ACCESS_TOKEN);
    setUser(null);
    callback();
  };

  const loadCurrentlyLoggedInUser = async (callback: Function) => {
    setIsLoading(true);
    await getCurrentUser<IUser>().then((res) => {
      setUser(res.data);
      callback(res.data);
    }).catch((error: AxiosError) => {
      console.error("Error: ", error);
      localStorage.removeItem(ACCESS_TOKEN);
      if (user) setUser(null);
      console.log("Call back null");
      callback(null);
    });
    setIsLoading(false);
  };

  useEffect(() => {
    console.log("USER: ", user);
  }, [user]);

  return { user, login, logout, loadCurrentlyLoggedInUser, isLoading };
}

export default AuthContext;
