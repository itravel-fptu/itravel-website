import { Outlet } from "react-router-dom";
import { Footer, Header } from "./parts";
import MessengerCustomerChat from "react-messenger-customer-chat";
import { FACEBOOK_PAGE_ID, MESSENGER_APP_ID } from "./shared/constants";
import MessengerChat from "./features/MessengerChat/MessengerChat";

const Layout: React.FC = () => {
  return (
    <>
      <Header />
      {/* 
        Uncomment bellow line when release the product,
        the messenger chat is not working for the localhost domain 
       */}
      {process.env.NODE_ENV === "production" && (
        <div>
          {/* <MessengerCustomerChat
            pageID="108656715151707"
            appId="1172289956916525"
            language="vi-VN"
          /> */}
          <MessengerChat />
        </div>
      )}
      <Outlet />
      <Footer />
    </>
  );
};

export default Layout;
