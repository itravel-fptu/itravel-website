import "./purchase.scss";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  FormControlLabel,
  InputLabel,
  Radio,
  RadioGroup,
  Snackbar,
  SnackbarCloseReason,
  TextField,
} from "@mui/material";
import PurchaseItem from "./Component/PurchaseItem";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import request from "../../shared/services/ApiService";
import { PurchaseItemType } from "../../shared/types/PurchaseItemType";
import { PAYMENT_REDIRECT_URI } from "../../constants";
export interface PaymentResponse {
  payUrl: string;
  message: string;
}
const Purchase: React.FC = () => {
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [paymentMethod, setPaymentMethod] = useState("Paypal");
  const [open, setOpen] = useState(false);
  const [openPayment, setOpenPayment] = useState(false);
  const [isIgnore, setIsIgnore] = useState(true);
  const [orderBillId, setOrderBillId] = useState("");
  // const [cartItems, setCartItems] = useState([] as number[]);
  const [isEmptyName, setIsEmptyName] = useState(false);
  const [isEmptyPhoneNumber, setIsEmptyPhoneNumber] = useState(false);
  const [isWrongPhoneNumber, setIsWrongPhoneNumber] = useState(false);
  const [loading, setLoading] = useState(true);
  const [openNotify, setOpenNotify] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };

  var purchaseItems = JSON.parse(
    localStorage.getItem("purchaseItems") || "{}"
  ) as PurchaseItemType[];

  let navigate = useNavigate();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsIgnore(true);
    navigate(-1);
  };
  const handleClickOpenPayment = () => {
    setOpenPayment(true);
  };

  const handleClosePayment = () => {
    setOpenPayment(false);
    // setIsIgnore(true);
    // navigate(-1);
  };

  async function createOrder() {
    let response = await request<string>("POST", `/sp/order`, null, {
      cartIds: purchaseItems.map((item) => item.id),
      fullName: name,
      phoneNumber: phoneNumber,
    })
      .then((res) => {
        localStorage.setItem("purchaseItems", "");
        localStorage.setItem("countItem", "");
        localStorage.setItem("calculateTotal", "");
        setOpenPayment(true);
        setOrderBillId(res.data.toString());
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/purchase", { replace: true });
        // console.log("respond: ")
      });
    setLoading(true);
  }
  useEffect(() => {
    console.log(purchaseItems);
    if (!purchaseItems.length) {
      setOpen(true);
    }
  }, []);

  let location = useLocation();
  async function payment() {
    let response = await request<PaymentResponse>("POST", `/pal/paypal/pay`, {
      orderBillId: orderBillId,
      redirectUrl: PAYMENT_REDIRECT_URI,
    })
      .then((res) => {
        // let path = `/myorder`;
        // navigate(path);
        let url = res.data.payUrl;
        if (url !== "") {
          window.location.replace(url);
          // var win = window.open(url, "_blank");
          // win?.focus();
        } else {
          setOpenNotify(true);
        }
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/purchase", { replace: true });
      });
    setLoading(true);
    // console.log(response);
  }

  const handleName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
    setIsIgnore(false);
  };

  const handlePhoneNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhoneNumber(e.target.value);
    setIsIgnore(false);
  };

  const handlePaymentMethod = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPaymentMethod(e.target.value);
    setIsIgnore(false);
  };
  const validPhoneNumber = new RegExp(
    /(([03+[2-9]|05+[6|8|9]|07+[0|6|7|8|9]|08+[1-9]|09+[1-4|6-9]]){3})+[0-9]{7}\b/g
  );
  const handleOrder = () => {
    if (name === "") {
      setIsEmptyName(true);
    } else setIsEmptyName(false);
    if (phoneNumber === "") {
      setIsEmptyPhoneNumber(true);
    } else if (!validPhoneNumber.test(phoneNumber)) {
      setIsEmptyPhoneNumber(false);
      setIsWrongPhoneNumber(true);
    } else {
      setIsEmptyPhoneNumber(false);
      setIsWrongPhoneNumber(false);
    }
    // setCartItems(purchaseItems.map((item) => item.id));
    if (!isEmptyName && !isEmptyPhoneNumber && !isWrongPhoneNumber) {
      // uploadItem();
      setLoading(false);
      createOrder();
      setIsIgnore(true);
    }

    //send name, phoneNumber, purchaseItem, purchaseCountItems, purchaseTotal, paymentMethod
  };
  const handleMyOrder = () => {
    let path = `/myorder`;
    navigate(path);
  };
  const handlePayment = () => {
    setLoading(false);
    setOpenPayment(false);
    payment();
  };
  return (
    <div className="purchase">
      <Container className="purchase-container">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h2>Điền thông tin</h2>
        {/* <form onSubmit={handleOrder}> */}
        <div className="purchase-content">
          <div className="user-info">
            <h2>Thông tin liên lạc</h2>
            <div className="field">
              <div>
                <InputLabel className="label">
                  Họ và tên<span className="required">*</span>
                </InputLabel>
                <TextField
                  className="textfield"
                  error={isEmptyName}
                  helperText={
                    isEmptyName ? "Vui lòng điền vào trường này." : ""
                  }
                  // required
                  value={name}
                  onChange={handleName}
                />
              </div>
              <br></br>
              <div>
                <InputLabel className="label">
                  Số điện thoại<span className="required">*</span>
                </InputLabel>
                <TextField
                  className="textfield"
                  // required
                  error={isEmptyPhoneNumber || isWrongPhoneNumber}
                  helperText={
                    isEmptyPhoneNumber
                      ? "Vui lòng điền vào trường này."
                      : isWrongPhoneNumber
                      ? "Vui lòng điền đúng số điện thoại."
                      : ""
                  }
                  value={phoneNumber}
                  onChange={handlePhoneNumber}
                />
              </div>
            </div>
          </div>
          <br></br>
          <div className="order-info">
            <h2>Thông tin đơn hàng</h2>
            <div className="header-table">
              <p id="p-product">Sản phẩm</p>
              <p>Ngày bắt đầu sử dụng</p>
              <p>Đơn giá</p>
              <p>Số lượng</p>
              <p>Số tiền</p>
            </div>
            <div>
              {!purchaseItems.length ? (
                <p className="align-center">Không có sản phẩm nào!</p>
              ) : (
                purchaseItems.map((item: any) => (
                  <PurchaseItem key={item.id} item={item} />
                ))
              )}
              {/* {} */}
            </div>
          </div>
          <br></br>
          <div className="payment-method">
            <h2>Cách thức thanh toán</h2>
            <RadioGroup
              className="radio-group"
              row
              aria-labelledby="demo-radio-buttons-group-label"
              // defaultValue="Paypal"
              name="radio-buttons-group"
              onChange={handlePaymentMethod}
              value={paymentMethod}
            >
              <FormControlLabel
                value="Paypal"
                control={<Radio />}
                label={
                  <div className="label-radio">
                    <img className="paypal" />
                    <p>Thanh toán bằng ví Paypal</p>
                  </div>
                }
              />
              <FormControlLabel
                value="Momo"
                disabled
                control={<Radio />}
                label={
                  <div className="label-radio">
                    <img className="momo" />
                    <p>Thanh toán bằng ví Momo</p>
                  </div>
                }
              />
            </RadioGroup>
          </div>
          <br></br>
          <div className="checkout">
            <div className="checkout-content">
              <p>
                Tổng thanh toán (
                {Number(localStorage.getItem("countItem")).toLocaleString(
                  undefined,
                  {
                    maximumFractionDigits: 0,
                  }
                )}{" "}
                sản phẩm):{" "}
                {Number(localStorage.getItem("calculateTotal")).toLocaleString(
                  undefined,
                  {
                    maximumFractionDigits: 0,
                  }
                )}
                đ
              </p>
              <Button
                type="submit"
                size="small"
                disableElevation
                variant="contained"
                onClick={handleOrder}
              >
                Đặt hàng
              </Button>
            </div>
          </div>
        </div>
        {/* </form> */}
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Không có sản phẩm nào. Vui lòng quay lại.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} autoFocus>
              ok
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={openPayment}
          onClose={handleClosePayment}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Đặt hàng thành công! Bạn có muốn tiến hành thanh toán ngay không?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handlePayment} autoFocus>
              Thanh toán
            </Button>
            <Button onClick={handleMyOrder}>Xem đơn hàng</Button>
          </DialogActions>
        </Dialog>

        <Snackbar
          open={openNotify}
          autoHideDuration={6000}
          onClose={handleCloseNotify}
          message="Thanh toán không thành công! Vui lòng thử lại sau."
          // action={action}
        />
      </Container>
    </div>
  );
};

export default Purchase;
