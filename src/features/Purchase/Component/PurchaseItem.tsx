import moment from "moment";
import { PurchaseItemType } from "../../../shared/types/PurchaseItemType";
import "./purchase-item.scss";

type Props = {
  item: PurchaseItemType;
};
const PurchaseItem: React.FC<Props> = ({ item }) => {
  return (
    <div className="purchase-item">
      <div className="img-title">
        <div className="img-container">
          <img src={item.image} />
        </div>
        <div className="title">
          <b>{item.mainServiceTitle}</b>
          <p>{item.subServiceTitle}</p>
          <p>Shop: {item.mainServiceShopName}</p>
        </div>
      </div>
      <div className="start-date">
        {moment.utc(item.dateStart).format("DD/MM/YYYY")}
      </div>
      <div className="price">
        <p>{item.price}đ</p>
      </div>
      <div className="amount">
        <p>{item.quantity}</p>
      </div>
      <div className="total-price">
        <p>{item.price * item.quantity}đ</p>
      </div>
    </div>
  );
};

export default PurchaseItem;
