import "./shop-list.scss";
import variables from "../../assets/scss/_variables.scss";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  createTheme,
  IconButton,
  InputBase,
  Pagination,
  Snackbar,
  SnackbarCloseReason,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  ThemeProvider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import InfoIcon from "@mui/icons-material/Info";
import { useEffect, useState } from "react";
import { FilterAlt } from "@mui/icons-material";
import ClearIcon from "@mui/icons-material/Clear";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { StatusType } from "../../shared/types/StatusType";
import { useNavigate } from "react-router-dom";
import styled from "@emotion/styled";
import { ACCESS_TOKEN } from "../../constants";
import request from "../../shared/services/ApiService";
import moment from "moment";

export interface Shop {
  address: string;
  avatar: string;
  createdAt: Date;
  description: string;
  id: number;
  ownerId: number;
  ownerName: string;
  serviceCount: number;
  shopName: string;
  status: string;
}
export interface ShopResult {
  page: number;
  shopInfos: Shop[];
  size: number;
  totalItem: number;
  totalPage: number;
}
const ShopList: React.FC = () => {
  const [rows, setRows] = useState([] as Shop[]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [loading, setLoading] = useState(false);
  const [isFilter, setIsFilter] = useState(false);
  const [openNotify, setOpenNotify] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    await request<ShopResult>("GET", `/sp/mod/shop`, {
      page: pageNumber,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.shopInfos.map((item) => item));
        setTotalPage(data.totalPage);
        console.log(data);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/shoplist", { replace: true });
      });
    setLoading(true);
  }
  useEffect(() => {
    setLoading(false);
    isFilter ? filter() : fetchData();
    return () => {
      setRows([]);
    };
  }, [pageNumber]);
  async function lockShop(id: number) {
    await request("PUT", `/sp/mod/shop/` + id, {
      shopId: id,
      status: "LOCKED",
    })
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/shoplist", { replace: true });
      });
    filterValue.length === 0 ? fetchData() : filter();
  }
  async function unlockShop(id: number) {
    await request("PUT", `/sp/mod/shop/` + id, {
      shopId: id,
      status: "ACTIVE",
    })
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/shoplist", { replace: true });
      });
    filterValue.length === 0 ? fetchData() : filter();
  }
  async function search() {
    await request<ShopResult>("GET", `/sp/shop/name/` + searchText, {
      page: pageNumber,
      shopName: searchText,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.shopInfos.map((item) => item));
        setTotalPage(data.totalPage);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/shoplist", { replace: true });
      });
    // fetchData();
  }
  async function filter() {
    let response = await request<ShopResult>(
      "POST",
      `/sp/mod/shop/status`,
      {
        page: pageNumber,
        size: 6,
      },
      {
        statuses: filterValue,
      }
    )
      .then((res) => {
        let data = res.data;
        setRows(data.shopInfos.map((item) => item));
        setTotalPage(data.totalPage);
        setIsFilter(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/shoplist", { replace: true });
      });
    setLoading(true);
    // fetchData();
  }
  const [open, setOpen] = useState(false);
  const [statusList, setStatusList] = useState([
    { id: 1, title: "ACTIVE", isChoose: false },
    { id: 2, title: "DELETED", isChoose: false },
    { id: 3, title: "LOCKED", isChoose: false },
  ] as StatusType[]);
  const [filterValue, setFilterValue] = useState([] as string[]);
  const handleClickOpen = () => {
    setOpen(open ? false : true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [searchText, setSearchText] = useState("");
  const handleSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    let text = e.target.value;
    setSearchText(text);
    if (text === "") {
      fetchData();
    }
  };
  const handleEnter = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (e.key === "Enter") search();
  };

  const handleStatus = (id: number) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleRemoveOption = (title: string) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.title === title) {
          const arr = filterValue.filter((val) => val !== item.title);
          setFilterValue(arr);
          return [...ack, { ...item, isChoose: false }];
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleApplyFilter = () => {
    setLoading(false);
    setOpen(false);
    if (filterValue.length !== 0) {
      setPageNumber(1);
      filter();
    } else {
      setIsFilter(false);
      fetchData();
    }
    // console.log(filterValue);
  };
  let navigate = useNavigate();
  const handleViewDetail = (shopId: number) => {
    let path = "/shop/" + shopId;
    navigate(path);
  };
  const handleViewService = (shopId: number) => {
    let path = "/manager/serviceappraisallist/" + shopId;
    navigate(path);
  };
  const handleRemoveAllFilter = () => {
    setLoading(false);
    setOpen(false);
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        // const arr = filterValue.filter((val) => val !== item.title);
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setFilterValue([]);
    fetchData();
  };
  const handleChangePage = (page: number) => {
    // console.log("page", page);
    setPageNumber(page);
  };
  const theme = createTheme({
    palette: {
      primary: { main: variables.colorMain },
      info: { main: "#fff" },
    },
    components: {
      MuiTableHead: {
        styleOverrides: {
          root: {
            backgroundColor: variables.colorMain,
            position: "sticky",
            top: 0,
          },
        },
      },
    },
  });
  const StyledTableRow = styled(TableRow)(() => ({
    "&:nth-of-type(even)": {
      backgroundColor: variables.colorRowOdd,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));
  return (
    // <div className="shop-list">
    <ThemeProvider theme={theme}>
      <Container className="shop-container">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h2>Danh sách cửa hàng</h2>
        <div className="search-filter">
          <div className="search-bar">
            <IconButton onClick={search}>
              <SearchIcon className="search-icon" />
            </IconButton>
            <InputBase
              type="search"
              style={{ width: 400 }}
              placeholder="Tìm kiếm theo tên cửa hàng"
              value={searchText}
              onChange={handleSearchText}
              onKeyDown={handleEnter}
            />
          </div>
          <Button
            className="btn-filter"
            variant="contained"
            onClick={handleClickOpen}
          >
            <FilterAlt />
            Bộ lọc
          </Button>
        </div>
        <div className="filter" hidden={open ? false : true}>
          <IconButton onClick={handleClose} className="btn-close">
            <ClearIcon fontSize="small" />
          </IconButton>
          <div className="filter-value-container">
            {filterValue.length !== 0 ? (
              <p className="selected-title">Đã chọn:</p>
            ) : null}
            {filterValue.length !== 0
              ? filterValue.map((statusValue) => (
                  <div className="filter-value" key={statusValue}>
                    <p>{statusValue}</p>{" "}
                    <IconButton
                      onClick={() => handleRemoveOption(statusValue)}
                      className="btn-close"
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  </div>
                ))
              : null}
            {filterValue.length !== 0 ? (
              <Button onClick={handleRemoveAllFilter}>Xóa tất cả</Button>
            ) : null}
          </div>
          <p className="status-title">Tình trạng</p>
          {statusList.map((status) => (
            <Button
              key={status.id}
              className={
                status.isChoose
                  ? "btn-filter-option isChoose"
                  : "btn-filter-option"
              }
              variant="contained"
              color="info"
              onClick={() => handleStatus(status.id)}
            >
              {status.title}
            </Button>
          ))}
          <Button
            className="btn-apply-filter"
            variant="contained"
            onClick={handleApplyFilter}
          >
            Lọc
          </Button>
        </div>

        <div className="table-container">
          <TableContainer className="container-table">
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell width={"50"}>ID</TableCell>
                  {/* <TableCell width={"120"} align="center">
                    Hình ảnh
                  </TableCell> */}
                  <TableCell width={"150"}>Tên cửa hàng</TableCell>
                  <TableCell width={"150"}>Chủ cửa hàng</TableCell>
                  <TableCell width={"150"}>Địa chỉ</TableCell>
                  <TableCell width={"150"}>Ngày đăng ký</TableCell>
                  <TableCell width={"130"} align="center">
                    Tình trạng
                  </TableCell>
                  <TableCell width={"150"} align="center">
                    Dịch vụ
                  </TableCell>
                  <TableCell width={"130"} align="center">
                    Tùy chọn
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {rows.length === 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <h3>Không có cửa hàng nào</h3>
                    </TableCell>
                  </TableRow>
                ) : (
                  rows.map((row) => (
                    <StyledTableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      {/* <TableCell className="cell-img" align="center">
                        <img src={row.image} />
                      </TableCell> */}
                      <TableCell>
                        <div className="long-text">{row.shopName}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.ownerName}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.address}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">
                          {moment(row.createdAt).format("hh:mm DD-MM-YYYY")}
                        </div>
                      </TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center">
                        <Button
                          className="btn-act"
                          onClick={() => handleViewService(row.id)}
                          // title="Xem dịch vụ"
                        >
                          Xem dịch vụ
                        </Button>
                      </TableCell>
                      <TableCell align="center" style={{ padding: 0 }}>
                        <Button
                          className="btn-act"
                          onClick={() => handleViewDetail(row.id)}
                          title="Chi tiết"
                        >
                          <InfoIcon />
                        </Button>

                        {row.status === "LOCKED" ? (
                          <Button
                            onClick={() => unlockShop(row.id)}
                            title="Mở khóa"
                          >
                            <LockOpenIcon />
                          </Button>
                        ) : (
                          <Button onClick={() => lockShop(row.id)} title="Khóa">
                            <LockIcon />
                          </Button>
                        )}
                      </TableCell>
                    </StyledTableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Pagination
              className="table-pagination"
              count={totalPage}
              page={pageNumber}
              onChange={(event: React.ChangeEvent<unknown>, page: number) =>
                handleChangePage(page)
              }
              variant="outlined"
              color="primary"
              shape="rounded"
            />
          </TableContainer>
          {/* </Box> */}
        </div>
        <Snackbar
          open={openNotify}
          autoHideDuration={1000}
          onClose={handleCloseNotify}
          message="Cập nhật thành công!"
          // action={action}
        />
      </Container>
    </ThemeProvider>
    // </div>
  );
};

export default ShopList;
