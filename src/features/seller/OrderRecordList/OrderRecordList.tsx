import { useEffect, useState } from "react";
import { Button, CircularProgress, Container } from "@mui/material";
import DateRangePicker from "./components/DateRangePicker";
import "./order-record-list.scss";
import moment from "moment";
import OrderRecordTable from "./components/OrderRecordTable";
import { IOrderRecord, orderRecordList } from "./FakeData";
import {
  getShopOrderRecordList,
  OrderRecordListResultType,
} from "./OrderRecordListController";
import { ShopType } from "../../../shared/types/ShopType";
import { useQuery } from "react-query";
import { useOutletContext } from "react-router-dom";

const OrderRecordList = () => {
  const { shopDetails, shopId }: { shopDetails: ShopType; shopId: number } =
    useOutletContext();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [dateStartFilter, setDateStartFilter] = useState(
    moment().subtract(1, "days").format("YYYY-MM-DD")
  );
  const [dateEndFilter, setDateEndFilter] = useState(
    moment().format("YYYY-MM-DD")
  );
  const {
    data: shopOrderRecordList,
    refetch,
    isLoading,
    isError,
  } = useQuery<OrderRecordListResultType>(
    "shopOrderRecordList",
    () => getShopOrderRecordList(shopId),
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  useEffect(() => {
    if (shopId !== 0) refetch();
  }, [shopId, page, rowsPerPage]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  if (isLoading) {
    return (
      <div className="isLoading flex-row">
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (isError) {
    return (
      <div className="isLoading flex-row">
        Đã có lỗi xảy ra! Vui lòng thử lại sau
      </div>
    );
  }

  return (
    <Container className="order-record-list">
      <div className="title">Danh sách đơn hàng</div>
      <div className="date-range-filter flex-row flex-jc-sb">
        <DateRangePicker
          dateStart={dateStartFilter}
          dateEnd={dateEndFilter}
          setDateStart={setDateStartFilter}
          setDateEnd={setDateEndFilter}
        />
        <Button className="search-btn" variant="contained" color="primary">
          Tìm kiếm
        </Button>
      </div>
      <div className="table">
        {shopOrderRecordList && (
          <OrderRecordTable
            orderRecordList={shopOrderRecordList}
            refetchOrderRecordList={refetch}
            rowsPerPage={rowsPerPage}
            page={page}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        )}
      </div>
    </Container>
  );
};

export default OrderRecordList;
