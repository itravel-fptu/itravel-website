export interface IOrderRecord {
  id: number;
  accountName: string;
  createdAt: string;
  totalPrice: number;
  status: string;
}

export const orderRecordList: IOrderRecord[] = [
  {
    id: 123,
    accountName: "Nguyen Van A",
    createdAt: "04/04/2022",
    totalPrice: 250000,
    status: "FINISHED",
  },
  {
    id: 145,
    accountName: "Nguyen Van A",
    createdAt: "04/04/2022",
    totalPrice: 123800,
    status: "NEW",
  },
  {
    id: 174,
    accountName: "Trịnh Xuân An",
    createdAt: "23/04/2022",
    totalPrice: 175000,
    status: "PROCESSING",
  },
  {
    id: 178,
    accountName: "Vũ Quang Tuấn",
    createdAt: "23/04/2022",
    totalPrice: 300000,
    status: "CANCEL",
  },
  {
    id: 179,
    accountName: "Trần Thế Quang",
    createdAt: "24/04/2022",
    totalPrice: 80000,
    status: "REFUND",
  },
];
