import { ACCESS_TOKEN } from "../../../constants";
import request from "../../../shared/services/ApiService";

export type OrderRecordListResultType = {
  page: number;
  results: OrderRecordType[];
  size: number;
  totalItem: number;
  totalPage: number;
};

export type OrderRecordType = {
  createdAt: string;
  fullName: string;
  id: number;
  orderBillId: string;
  orderItemInfos: [
    {
      id: number;
      image: string;
      mainServiceId: number;
      mainServiceTitle: string;
      price: number;
      quantity: number;
      shopId: number;
      shopName: string;
      subServiceId: number;
      subServiceTitle: string;
      usedEnd: string;
      usedStart: string;
    }
  ];
  phoneNumber: string;
  status: string;
  totalPrice: number;
};

export const getShopOrderRecordList = async (
  shopId: number
): Promise<OrderRecordListResultType> => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);

  if (!token) {
    return Promise.reject("No access token set.");
  }

  const url = `/pal/order/shop/${shopId}`;
  const results = await request<OrderRecordListResultType>("GET", url).then(
    (res) => res.data
  );
  return results;
};
