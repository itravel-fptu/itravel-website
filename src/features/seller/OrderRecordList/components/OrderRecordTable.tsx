import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Paper,
} from "@mui/material";
import { ReactElement } from "react";
import { Link } from "react-router-dom";
import { ORDER_STATUS } from "../../../../shared/constants/status";
import {
  OrderRecordListResultType,
  OrderRecordType,
} from "../OrderRecordListController";

interface Column {
  id:
    | "No."
    | "id"
    | "fullName"
    | "createdAt"
    | "totalPrice"
    | "status"
    | "options";
  label: string;
  minWidth?: number;
  align?: "right" | "center";
  format?: (value: number) => string;
}

const columns: Column[] = [
  {
    id: "No.",
    label: "STT",
    minWidth: 50,
    format: (value: number) => value.toString(),
  },
  { id: "id", label: "Mã đơn", minWidth: 70 },

  {
    id: "fullName",
    label: "Người đặt",
    minWidth: 150,
  },
  {
    id: "createdAt",
    label: "Ngày đặt",
    minWidth: 150,
  },
  {
    id: "totalPrice",
    label: "Tổng tiền",
    minWidth: 140,
    format: (value: number) => value.toLocaleString("vi-VN"),
  },
  {
    id: "status",
    label: "Trạng thái",
    minWidth: 140,
  },
  {
    id: "options",
    label: "Tuỳ chọn",
    minWidth: 150,
  },
];

interface Data {
  id: number;
  fullName: string;
  createdAt: string;
  totalPrice: number;
  status: string;
  options: ReactElement;
}

const renderCell = (index: number, row: Data, column: Column) => {
  let value;
  if (column.id === "No.") {
    value = index + 1;
  } else {
    value = row[column.id];
  }

  if (column.id === "status") {
    const status = row[column.id];
    value = ORDER_STATUS[status as keyof typeof ORDER_STATUS];

    return (
      <TableCell align={column.align} key={column.id}>
        <div
          className={`order-status__${status
            .replace(/\s+/g, "-")
            .toLowerCase()}`}
        >
          {value}
        </div>
      </TableCell>
    );
  }

  return (
    <TableCell align={column.align} key={column.id}>
      {column.format && typeof value === "number"
        ? column.format(value)
        : value}
    </TableCell>
  );
};

type ShopOrderRecordTablePropsType = {
  orderRecordList: OrderRecordListResultType;
  page: number;
  rowsPerPage: number;
  handleChangePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => void;
  refetchOrderRecordList: (options?: any) => Promise<any>;
};

const OrderRecordTable: React.FC<ShopOrderRecordTablePropsType> = ({
  orderRecordList,
  page,
  rowsPerPage,
  handleChangePage,
  handleChangeRowsPerPage,
  refetchOrderRecordList,
}) => {
  function createData(orderRecord: OrderRecordType): Data {
    const options = (
      <div>
        <Link to={`./../${orderRecord.id}`} className="link-update">
          Xem chi tiết
        </Link>
      </div>
    );
    return { ...orderRecord, options };
  }

  const rows: Data[] = orderRecordList?.results.map((orderRecord) =>
    createData(orderRecord)
  );

  return (
    <div className="order-record-table">
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 450 }}>
          <Table aria-label="sticky table">
            <TableHead className="order-record-table__head">
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align="justify"
                    style={{ top: 57, minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody className="order-record-table__body">
              {rows.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={12} align="center">
                    <h3>Hiện cửa hàng chưa có đơn hàng nào!</h3>
                  </TableCell>
                </TableRow>
              ) : (
                rows.map((row, index) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => renderCell(index, row, column))}
                    </TableRow>
                  );
                })
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={orderRecordList.totalItem}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

export default OrderRecordTable;
