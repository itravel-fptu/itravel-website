import { DatePicker } from "@mui/lab";
import AdapterMoment from "@mui/lab/AdapterMoment";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { TextField } from "@mui/material";

type DateRangePickerPropsType = {
  dateStart: string;
  dateEnd: string;
  setDateStart: any;
  setDateEnd: any;
};

const DateRangePicker: React.FC<DateRangePickerPropsType> = ({
  dateStart,
  dateEnd,
  setDateStart,
  setDateEnd,
}) => {
  return (
    <div className="date-range-input">
      <div className="helperText">Định dạng: dd/mm/yyyy</div>
      <div className="event-range__input">
        <LocalizationProvider dateAdapter={AdapterMoment}>
          <DatePicker
            label="Ngày bắt đầu"
            className="date-picker"
            value={dateStart}
            onChange={(newValue: any) => {
              setDateStart(newValue);
            }}
            mask="__-__-____"
            inputFormat={"DD-MM-YYYY"}
            renderInput={(params: any) => <TextField {...params} />}
          />
          <span className="to-label">tới</span>
          <DatePicker
            label="Ngày kết thúc"
            className="date-picker"
            value={dateEnd}
            onChange={(newValue: any) => {
              setDateEnd(newValue);
            }}
            mask="__-__-____"
            inputFormat={"DD-MM-YYYY"}
            renderInput={(params: any) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </div>
    </div>
  );
};

export default DateRangePicker;
