import { CircularProgress } from "@mui/material";
import Stack from "@mui/material/Stack";
import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { Outlet, useNavigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import { Header } from "../../parts";
import SellerSidebar from "../../shared/Component/SellerSidebar";
import { ShopType } from "../../shared/types/ShopType";

import { IUser } from "../../shared/types/UserType";
import { getShopDetails } from "./SellerServices";

type PrivateSellerType = {
  acceptedRole: string;
};

const PrivateSeller: React.FC<PrivateSellerType> = ({ acceptedRole }) => {
  let auth = useAuth();
  let navigate = useNavigate();
  const [shopId, setShopId] = useState(0);

  const { data: shopDetails, refetch } = useQuery<ShopType>(
    "shopDetails",
    getShopDetails,
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  useEffect(() => {
    const loadCurrentUser = async () => {
      await auth
        .loadCurrentlyLoggedInUser((user: IUser) => {
          if (!user) {
            navigate("/", { replace: true });
          } else if (
            user.roles.filter((role) => role.roleName === acceptedRole)
              .length === 0
          ) {
            console.error("Unauthorized");
            navigate("/", { replace: true });
          }
          refetch();
        })
        .catch((error) => {
          console.log("Error: ", error);
          navigate("/", { replace: true });
        });
    };
    loadCurrentUser();
  }, []);

  useEffect(() => {
    if (shopDetails && shopDetails.id) setShopId(shopDetails.id);
  }, [shopDetails]);

  if (auth.isLoading)
    return (
      <div className="isLoading flex-row">
        <CircularProgress />
      </div>
    );

  return (
    <div>
      <Header />
      <Stack direction="row">
        <SellerSidebar />
        <Outlet context={{ shopDetails, shopId }} />
      </Stack>
    </div>
  );
};

export default PrivateSeller;
