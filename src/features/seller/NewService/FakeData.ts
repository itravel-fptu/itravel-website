import { CityType } from "../../../shared/types/CityType";
import { CategoryCardType } from "../../../shared/types/ServiceType";

export const categories: CategoryCardType[] = [
  {
    id: 1,
    title: "Điểm tham quan & vui chơi",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path:
      "https://i.insider.com/5d38b0b336e03c401422cdf8?width=750&format=jpeg&auto=webp",
  },
  {
    id: 2,
    title: "Dịch vụ ăn uống",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",

    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 3,
    title: "Hoạt động thể thao",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 4,
    title: "Phương tiện di chuyển",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 5,
    title: "Sự kiện",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 6,
    title: "Thư giãn & làm đẹp",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
];

export const cities: CityType[] = [
  {
    id: 1,
    name: "Đà Nẵng",
  },
  {
    id: 2,
    name: "Quảng Nam",
  },
  {
    id: 3,
    name: "Hội An",
  },
  {
    id: 4,
    name: "Bắc Ninh",
  },
];
