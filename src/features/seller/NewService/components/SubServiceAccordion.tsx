import {
  Accordion,
  AccordionDetails,
  AccordionSummary
} from "@mui/material";
import { NewSubServiceType } from "../../../../shared/types/ServiceType";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { DataGrid, GridCellEditCommitParams, GridColumns } from "@mui/x-data-grid";

type SubServiceAccordionProps = {
  subServices: NewSubServiceType[];
  handleDataEditCommit: (e: GridCellEditCommitParams, updateItemIndex: number) => void;
  isInputError: boolean[]
};

const SubServiceAccordion: React.FC<SubServiceAccordionProps> = ({
  subServices, handleDataEditCommit, isInputError
}) => {

  return (
    <div>
      {subServices.map((subService, index) => (
        <div key={index} className={isInputError[index] ? "sub-service__accordion-error" : "sub-service__accordion"}>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              {subService.title === ""
                ? `Dịch vụ phụ ${index + 1} *`
                : subService.title}
            </AccordionSummary>
            <AccordionDetails>
              <div>
                <DataGrid
                  {...dataGridProps}
                  rows={createRows(subService, index)}
                  columns={columns}
                  onCellEditCommit={(e) => handleDataEditCommit(e, index)}
                />
              </div>
            </AccordionDetails>
          </Accordion>
        </div>
      ))}
    </div>
  );
};

const dataGridProps = {
  autoHeight: true,
  disableColumnMenu: true,
  disableColumnFilter: true,
  disableColumnSelector: true,
  disableDensitySelector: true,
  hideFooter: true,
  showCellRightBorder: true,
  disableSelectionOnClick: true,
  filterable: false,
  sortable: false,
};

const createRows = (subService: NewSubServiceType, index: number) => {
  let rows: NewSubServiceType[] = [];
  return [...rows, { ...subService, id: index }];
};

const columns: GridColumns = [
  {
    field: "price",
    headerName: "Giá",
    type: "number",
    editable: true,
    minWidth: 150,
    align: "right",
  },
  {
    field: "stockAmount",
    headerName: "Số lượng",
    type: "number",
    editable: true,
    minWidth: 150,
    align: "right",
  },
];

export default SubServiceAccordion;
