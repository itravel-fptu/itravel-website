import { ChangeEvent, useEffect, useState } from "react";
// MUI Components
import Box from "@mui/material/Box";
import { Alert, Button, Snackbar } from "@mui/material";
// MUI Assets
import AddPhotoAlternateIcon from "@mui/icons-material/AddPhotoAlternate";
import CloseIcon from "@mui/icons-material/Close";
import { ImageType } from "../NewService";

type ImageUploaderType = {
  selectedImage: ImageType;
  handleSelectImage: (e: ChangeEvent<HTMLInputElement>, id: number) => void;
  handleRemoveImage: (id: number) => void;
  error: boolean;
};

const ImageUploader: React.FC<ImageUploaderType> = ({
  selectedImage,
  handleSelectImage,
  handleRemoveImage,
  error,
}) => {
  return (
    <div>
      {selectedImage && selectedImage.image_data !== "" ? (
        <Box textAlign="center" className="img-select">
          <CloseIcon
            className="img-select__icon-remove"
            onClick={() => handleRemoveImage(selectedImage.id)}
            color="info"
          />
          <img src={selectedImage.image_data} className="img-select__preview" />
        </Box>
      ) : (
        <Box component="div" className="img-select">
          <input
            id="select-image"
            accept="image/*"
            type="file"
            style={{ display: "none" }}
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              console.log("ID trong ham con", selectedImage.id);
              handleSelectImage(e, selectedImage.id);
            }}
          />
          <label
            htmlFor="select-image"
            className={`img-select__label ${
              error ? "img-select__label-error" : ""
            }`}
          >
            <AddPhotoAlternateIcon
              color={error ? "error" : "primary"}
              className="img-select__icon-add"
            />
          </label>
        </Box>
      )}
    </div>
  );
};

export default ImageUploader;
