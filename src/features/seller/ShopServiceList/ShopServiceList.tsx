import { useEffect, useState } from "react";
import { useNavigate, useOutletContext } from "react-router-dom";
import { useQuery } from "react-query";
// Components
import SearchBar from "./components/SearchBar";
import ShopServiceTable from "./components/ShopServiceTable";

import Container from "@mui/material/Container";
import { Button } from "@mui/material";
// Styles & Assets
import "./shop-service-list.scss";
import AddIcon from "@mui/icons-material/Add";
// Data & Controller
import {
  getShopServiceList,
  ShopServiceListResultType,
} from "./ShopServiceController";
import { ShopType } from "../../../shared/types/ShopType";

const ShopServiceList = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const {shopDetails, shopId}: {shopDetails: ShopType, shopId: number} = useOutletContext();

  const { data: shopServiceList, refetch } =
    useQuery<ShopServiceListResultType>(
      "shopServiceList",
      () => getShopServiceList(shopId, rowsPerPage, page+1, keywordSearch),
      {
        refetchOnWindowFocus: false,
        enabled: false,
      }
    );

  useEffect(() => {
    refetch();
  }, [shopId, rowsPerPage, page]);

  const [keywordSearch, setKeywordSearch] = useState("");
  const navigate = useNavigate();

  const handleSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      setKeywordSearch(event.currentTarget.value);
    }
  };

  useEffect(() => {
    refetch();
  }, [keywordSearch])

  const handleClickAddNew = () => {
    navigate("/seller/service/new");
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Container className="shop-service-list">
      <div className="title">Danh sách dịch vụ</div>
      <div className="tool flex-row flex-jc-sb">
        <SearchBar handleSearch={handleSearch} />
        <Button
          onClick={handleClickAddNew}
          className="btn-add"
          variant="contained"
          size="small"
        >
          <AddIcon />
        </Button>
      </div>
      <div>
        {shopServiceList && (
          <ShopServiceTable
            shopServiceList={shopServiceList}
            refetchServiceList={refetch}
            rowsPerPage={rowsPerPage}
            page={page}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        )}
      </div>
    </Container>
  );
};

export default ShopServiceList;
