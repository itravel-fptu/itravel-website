import request from "../../../shared/services/ApiService";

export interface IShopService {
  categoryId: number;
  categoryName: string;
  cityId: number;
  cityName: string;
  createdAt: string;
  id: number;
  images: string[];
  price: number;
  address: string;
  rateAverage: number;
  rateCount: number;
  shopId: number;
  shopShopName: string;
  status: string;
  title: string;
}

export type ShopServiceListResultType = {
  page: number;
  results: IShopService[];
  totalPages: number;
  totalResults: number;
};

export const getShopServiceList = async (
  shopId: number,
  size: number,
  page: number,
  name?: string
): Promise<ShopServiceListResultType> => {
  const url = `/sp/shop/${shopId}/service`;

  type paramType = {
    page: number;
    size: number;
    name?: string;
  };

  const params: paramType = {
    page: page,
    size: size,
  };

  if (name !== "") params.name = name;

  const results = await request<ShopServiceListResultType>(
    "GET",
    url,
    params
  ).then((res) => res.data);

  return results;
};

export const deleteShopService = async (serviceId: number): Promise<any> => {
  const url = `/sp/shop/service/${serviceId}`;

  const results = await request<any>("DELETE", url).then((res) => res.data);

  return results;
};
