export interface ServiceDataType {
  id: number;
  title: string;
  address: string;
  category: string;
  status: string;
  imagePath: string;
  createdAt: string;
}
