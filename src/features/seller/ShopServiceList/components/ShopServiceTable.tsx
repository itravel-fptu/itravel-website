import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Paper,
} from "@mui/material";
import moment from "moment";
import { ReactElement, useState } from "react";
import { useMutation } from "react-query";
import { Link } from "react-router-dom";
import { SERVICE_STATUS } from "../../../../shared/constants/status";

import {
  deleteShopService,
  IShopService,
  ShopServiceListResultType,
} from "../ShopServiceController";

import {
  ConfirmDialog,
  AlertDialog,
} from "./../../../../shared/Component/Seller";

type ShopServiceTablePropsType = {
  shopServiceList: ShopServiceListResultType;
  page: number;
  rowsPerPage: number;
  handleChangePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => void;
  refetchServiceList: (options?: any) => Promise<any>;
};

const resultImagePlaceholder = require("./../../../../assets/images/service-placeholder-image.jpg");

const ShopServiceTable: React.FC<ShopServiceTablePropsType> = ({
  shopServiceList,
  page,
  rowsPerPage,
  handleChangePage,
  handleChangeRowsPerPage,
  refetchServiceList,
}) => {
  const [confirmDialog, setConfirmDialog] = useState({
    open: false,
    title: "",
    message: "",
  });

  const [alertDialog, setAlertDialog] = useState({
    type: "success",
    open: false,
    message: "",
  } as { type: "success" | "error"; open: boolean; message: string });

  const [deleteServiceId, setDeleteServiceId] = useState(-1);

  const deleteServiceMutation = useMutation(
    (serviceId: number) => deleteShopService(serviceId),
    {
      onError: (error, variables, context) => {
        setAlertDialog((prev) => ({
          ...prev,
          open: true,
          type: "error",
          message: `Đã có lỗi xảy ra. Vui lòng thử lại sau! ${error}`,
        }));
      },
      onSuccess: (data, variables, context) => {
        setAlertDialog((prev) => ({
          ...prev,
          open: true,
          type: "success",
          message: "Đã xoá dịch vụ!",
        }));
      },
      onSettled: () => {
        refetchServiceList();
      },
    }
  );

  function createData(service: IShopService): Data {
    let action: string = "";
    if (service.status !== "DELETED") action = "Xoá dịch vụ";

    const image = service.images[0] || resultImagePlaceholder;
    const {
      id,
      title,
      address,
      categoryName: category,
      status,
      createdAt,
    } = service;

    const options = (
      <div>
        <Link to={`/seller/service/${service.id}`} className="link-update">
          Chỉnh sửa
        </Link>

        {action !== "" && (
          <div
            className="service-action"
            onClick={() => handleDeleteService(service)}
          >
            {action}
          </div>
        )}
      </div>
    );
    return {
      id,
      title,
      address,
      image,
      category,
      status,
      createdAt,
      options,
    };
  }

  const rows: Data[] = shopServiceList.results?.map((service) =>
    createData(service)
  );

  const handleDeleteService = (service: IShopService) => {
    setDeleteServiceId(service.id);
    setConfirmDialog((prev: any) => ({
      ...prev,
      open: true,
      message: `Bạn chắc chắn muốn xoá dịch vụ "${service.title}" này?`,
    }));
  };

  const handleConfirmDeleteClick = () => {
    if (deleteServiceId !== -1) {
      deleteServiceMutation.mutate(deleteServiceId);
    }
    setDeleteServiceId(-1);
    setConfirmDialog((prev: any) => ({
      ...prev,
      open: false,
    }));
  };

  return (
    <div className="shop-service-table">
      <AlertDialog
        type={alertDialog.type}
        message={alertDialog.message}
        open={alertDialog.open}
        handleConfirmClick={() => {
          setAlertDialog((prev) => ({ ...prev, open: false }));
        }}
      />
      <ConfirmDialog
        open={confirmDialog.open}
        title={confirmDialog.title}
        message={confirmDialog.message}
        handleOpen={() => {
          setConfirmDialog((prev) => ({ ...prev, open: true }));
        }}
        handleClose={() => {
          setConfirmDialog((prev) => ({ ...prev, open: false }));
        }}
        handleConfirmClick={handleConfirmDeleteClick}
      />
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ height: 450 }}>
          <Table aria-label="sticky table">
            <TableHead className="shop-service-table__head">
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align="justify"
                    style={{ top: 57, minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody className="shop-service-table__body">
              {rows.length === 0 ? (
                <TableRow>
                  <TableCell colSpan={12} align="center">
                    <h3>Hiện cửa hàng chưa có dịch vụ nào!</h3>
                  </TableCell>
                </TableRow>
              ) : (
                rows.map((row, index) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => renderCell(index, row, column))}
                    </TableRow>
                  );
                })
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={shopServiceList.totalResults}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

interface Column {
  id:
    | "No."
    | "image"
    | "title"
    | "address"
    | "category"
    | "status"
    | "createdAt"
    | "options";
  label: string;
  minWidth?: number;
  align?: "right" | "center";
  format?: (value: number) => string;
}

const columns: Column[] = [
  {
    id: "No.",
    label: "STT",
    minWidth: 50,
    format: (value: number) => value.toString(),
  },
  { id: "image", label: "Ảnh", minWidth: 70 },
  { id: "title", label: "Dịch vụ", minWidth: 200 },
  {
    id: "address",
    label: "Địa điểm",
    minWidth: 150,
  },
  {
    id: "category",
    label: "Danh mục",
    minWidth: 130,
  },
  {
    id: "status",
    label: "Trạng thái",
    minWidth: 140,
  },
  {
    id: "createdAt",
    label: "Ngày tạo",
    minWidth: 120,
  },
  {
    id: "options",
    label: "Tuỳ chọn",
    minWidth: 150,
  },
];

interface Data {
  id: number;
  image: string;
  title: string;
  address: string;
  category: string;
  status: string;
  createdAt: string;
  options: ReactElement;
}

const renderCell = (index: number, row: Data, column: Column) => {
  let value;
  if (column.id === "No.") {
    value = index + 1;
  } else {
    value = row[column.id];
  }

  if (column.id === "image")
    return (
      <TableCell key={column.id}>
        <img src={row.image} className="service-image" alt={"Ảnh dịch vụ"} />
      </TableCell>
    );

  if (column.id === "status") {
    const status = row[column.id];
    value = SERVICE_STATUS[status as keyof typeof SERVICE_STATUS];

    return (
      <TableCell key={column.id} align={column.align}>
        <div
          className={`service-status__${status
            .replace(/\_/g, "-")
            .toLowerCase()}`}
        >
          {value}
        </div>
      </TableCell>
    );
  }

  if (column.id === "createdAt") {
    return (
      <TableCell key={column.id} align={column.align}>
        {moment(value as string).format("DD/MM/YYYY")}
      </TableCell>
    );
  }

  return (
    <TableCell key={column.id} align={column.align}>
      {column.format && typeof value === "number"
        ? column.format(value)
        : value}
    </TableCell>
  );
};

export default ShopServiceTable;
