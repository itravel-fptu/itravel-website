import SearchIcon from "@mui/icons-material/Search";

type SearchBarPropType = {
  handleSearch: (event: React.KeyboardEvent<HTMLInputElement>) => void;
};

const SearchBar: React.FC<SearchBarPropType> = ({ handleSearch }) => {
  return (
    <div className="search-bar flex-row flex-jc-sb">
      <input
        type="text"
        placeholder="Tìm kiếm dịch vụ"
        onKeyDown={handleSearch}
      />
      <div className="search-bar__button">
        <SearchIcon fontSize="medium" sx={{ color: "#fff", m: "auto" }} />
      </div>
    </div>
  );
};

export default SearchBar;
