import { AxiosResponse } from "axios";
import { ACCESS_TOKEN } from "../../constants";
import request from "../../shared/services/ApiService";
import { NewServiceType } from "../../shared/types/ServiceType";
import { ShopType } from "../../shared/types/ShopType";

export const createNewShopService = (
  shopId: number,
  serviceData: NewServiceType
) => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);

  if (!token) {
    return Promise.reject("No access token set.");
  }

  const url = `/sp/shop/${shopId}/service`;
  return request("POST", url, null, serviceData);
};

export const getShopDetails = async (): Promise<ShopType> => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);

  if (!token) {
    return Promise.reject("No access token set.");
  }

  const url = `/sp/shop`;

  const shopDetail = await request<ShopType>("GET", url).then(
    (res) => res.data
  );

  return shopDetail;
};
