import { ChangeEvent, useEffect, useState } from "react";

// Components
import ImageUploader from "./components/ImageUploader";
import SubServiceAccordion from "./components/SubServiceAccordion";

// MUI Components
import {
  TextField,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
  IconButton,
  Button,
  Backdrop,
} from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import Container from "@mui/material/Container";
import { GridCellEditCommitParams } from "@mui/x-data-grid";
import { InputLabel } from "@mui/material";
import AdapterMoment from "@mui/lab/AdapterMoment";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
// Types
import {
  IUpdateSubService,
  UpdateServiceType,
} from "../../../shared/types/ServiceType";

// Styles
import "./update-service.scss";

import { CATEGORIES } from "./../../../shared/constants/category";

// MUI Assets
import DeleteIcon from "@mui/icons-material/Delete";

import { CITIES } from "../../../shared/constants";
import { useNavigate, useParams } from "react-router-dom";

import {
  ToastMessage,
  ConfirmDialog,
  AlertDialog,
} from "../../../shared/Component/Seller";

import moment from "moment";
import DatePicker from "@mui/lab/DatePicker";
import { useMutation, useQuery } from "react-query";
import {
  IServiceDetail,
  getServiceDetails,
  updateShopService,
} from "./ServiceController";

const IMAGE_FILE_SIZE_MAX: number = 1024 * 1024 * 3;

export type ImageType = {
  id: number;
  image_data: string;
};

const UpdateService: React.FC = () => {
  const { serviceId } = useParams();

  const { data: serviceDetail, isLoading: isGetServiceDetailLoading } =
    useQuery<IServiceDetail>(
      ["serviceDetail", serviceId],
      () => getServiceDetails(Number(serviceId)),
      {
        onError: (error: any) => {
          navigate("./../..");
        },
      }
    );

  useEffect(() => {
    setServiceInfo((prev) => ({
      ...prev,
      title: serviceDetail?.title || "",
      categoryId: serviceDetail?.categoryId || 1,
      address: serviceDetail?.address || "",
      duration: serviceDetail?.duration || 1,
      cityId: serviceDetail?.cityId || 1,
      description: serviceDetail?.description || "",
      eventStart: serviceDetail?.eventStart || "",
      eventEnd: serviceDetail?.eventEnd || "",
    }));

    if (serviceDetail?.images && serviceDetail.images.length > 0) {
      // images.map(async (image, index) => {
      //   if (index < serviceDetail.images.length) {
      //     let fileReader = new FileReader();
      //     let imageBlob = await fetch(serviceDetail.images[index]).then(
      //       (response) => {
      //         console.log("RESPONSE BLOB: ", response.blob());
      //         return response.blob();
      //       }
      //     );
      //     fileReader.readAsDataURL(imageBlob);
      //     fileReader.onload = () => {
      //       setImages((prev) => {
      //         return prev.map((image) =>
      //           image.id === index
      //             ? { ...image, image_data: fileReader.result as string }
      //             : image
      //         );
      //       });
      //     };
      //   }
      // });
    }

    setSubServices(
      serviceDetail?.subServices.map((subServices) => ({
        ...subServices,
        action: "update",
      })) || []
    );
  }, [serviceDetail]);

  const updateServiceMutation = useMutation(
    (serviceData: UpdateServiceType) =>
      updateShopService(Number(serviceId), serviceData),
    {
      onError: (error, variables, context) => {
        setAlertDialog((prev) => ({
          ...prev,
          type: "error",
          message: `Đã có lỗi xảy ra. Vui lòng thử lại sau! ${error}`,
        }));
      },
      onSuccess: (data, variables, context) => {
        setAlertDialog((prev) => ({
          ...prev,
          type: "success",
          message: "Chỉnh sửa dịch vụ thành công",
        }));
      },
      onSettled: (data, error, variables, context) => {
        setAlertDialog((prev) => ({ ...prev, open: true }));
      },
    }
  );

  const [serviceInfo, setServiceInfo] = useState({
    title: "",
    categoryId: 1,
    address: "",
    duration: 0,
    cityId: 1,
    description: "",
    eventStart: moment().format("YYYY-MM-DD"),
    eventEnd: moment().format("YYYY-MM-DD"),
    status: "ACTIVE",
  });

  const [images, setImages] = useState(
    imageUploaders.map((imageUploader) => {
      return { id: imageUploader.id, image_data: "" };
    }) as ImageType[]
  );

  const [subServices, setSubServices] = useState<IUpdateSubService[]>([]);
  const [subServicesRemove, setSubServicesRemove] = useState<
    IUpdateSubService[]
  >([]);

  const [isInputError, setIsInputError] = useState({
    title: false,
    address: false,
    description: false,
    images: false,
    duration: false,
  });

  const [subServicesError, setSubServicesError] = useState([] as boolean[]);

  const [toastMessage, setToastMessage] = useState({
    open: false,
    message: "",
  });

  const [confirmDialog, setConfirmDialog] = useState({
    open: false,
    title: "",
    message: "",
  });

  const [alertDialog, setAlertDialog] = useState({
    type: "success",
    open: false,
    message: "",
  } as { type: "success" | "error"; open: boolean; message: string });

  const navigate = useNavigate();

  useEffect(() => {
    // Init First Sub-service
    handleAddNewSubService();
  }, []);

  const handleAddNewSubService = () => {
    let subService: IUpdateSubService = {
      id: 0,
      title: "",
      price: 0,
      stockAmount: 0,
      action: "update",
    };

    setSubServices([...subServices, subService]);

    setSubServicesError((prev) => [...prev].concat(false));
  };

  const handleRemoveSubService = (index: number) => {
    if (subServices.length === 1) return;

    let removeSubService = subServices.filter(
      (subService, subServiceIndex) => subServiceIndex === index
    )[0];
    removeSubService.action = "delete";

    setSubServicesRemove((prev) => prev.concat(removeSubService));

    setSubServices(
      subServices.filter(
        (subService, subServiceIndex) => subServiceIndex !== index
      )
    );

    setSubServicesError((prev) =>
      [...prev].filter((subError, subErrorIndex) => subErrorIndex !== index)
    );
  };

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setServiceInfo((prev) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSelectChange = (event: SelectChangeEvent) => {
    setServiceInfo((prev) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubServiceInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    index: number
  ) => {
    setSubServices(
      subServices.map((subService, subServiceIndex) => {
        if (subServiceIndex === index) {
          subService.title = event.currentTarget.value;
        }
        return subService;
      })
    );
  };

  const handleSelectImage = (e: ChangeEvent<HTMLInputElement>, id: number) => {
    if (e.target.files && e.target.files[0]) {
      if (e.target.files[0].size > IMAGE_FILE_SIZE_MAX) {
        setToastMessage({ open: true, message: "File ảnh không vượt quá 3MB" });
        return;
      }
      let fileReader = new FileReader();
      fileReader.readAsDataURL(e.target.files[0]);
      fileReader.onload = () => {
        setImages((prev) => {
          return prev.map((image) =>
            image.id === id
              ? { ...image, image_data: fileReader.result as string }
              : image
          );
        });
      };
    }
  };

  const handleRemoveImage = (id: number) => {
    setImages(
      images.map((image) => {
        if (image.id === id) {
          image.image_data = "";
        }
        return image;
      })
    );
  };

  const handleDataEditCommit = (
    e: GridCellEditCommitParams,
    updateItemIndex: number
  ) => {
    setSubServices(
      subServices.map((subService: IUpdateSubService, index: number) => {
        if (index === updateItemIndex) {
          return { ...subService, [e.field]: e.value };
        }
        return subService;
      })
    );
  };

  const handleCancel = () => {
    const confirmDialog = {
      open: true,
      title: "Xác nhận",
      message: "Bạn có muốn huỷ thay đổi?",
    };
    setConfirmDialog(confirmDialog);
  };

  const handleUpdateService = () => {
    let updateService: UpdateServiceType = {
      title: serviceInfo.title,
      description: serviceInfo.description,
      address: serviceInfo.address,
      cityId: serviceInfo.cityId,
      categoryId: serviceInfo.categoryId,
      duration: Number(serviceInfo.duration),
      eventStart: serviceInfo.eventStart,
      eventEnd: serviceInfo.eventEnd,
      status: serviceInfo.status,
      images: images
        .filter((image) => image.image_data !== "")
        .map((image) => image.image_data),
      subServices: subServices,
    };

    if (subServicesRemove.length !== 0) {
      updateService.subServices = subServices.concat(subServicesRemove);
    }

    if (!validateInput(updateService)) return;

    console.log("SUBMIT: ", updateService);
    updateServiceMutation.mutate(updateService);
  };

  const validateInput = (updateService: UpdateServiceType): boolean => {
    let isValid = true;

    for (const [key, value] of Object.entries(updateService)) {
      if (key === "title" || key === "description" || key === "address") {
        if (value === "") {
          setIsInputError((prev) => ({ ...prev, [key]: true }));
          isValid = false;
        } else {
          if (isInputError[key]) {
            setIsInputError((prev) => ({ ...prev, [key]: false }));
          }
        }
      }

      if (key === "duration") {
      }

      // if (key === "images") {
      //   if (newService.images.length === 0) {
      //     setIsInputError((prev) => ({ ...prev, [key]: true }));
      //     isValid = false;
      //   } else {
      //     if (isInputError[key]) {
      //       setIsInputError((prev) => ({ ...prev, [key]: false }));
      //     }
      //   }
      // }

      if (key === "subServices") {
        updateService.subServices.map((subService, index) => {
          if (subService.title === "") {
            editSubServicesError(index, true);
            isValid = false;
          } else if (subService.price < 1000 || subService.stockAmount < 0) {
            editSubServicesError(index, true);
            isValid = false;
          } else {
            if (subServicesError[index] === true) {
              editSubServicesError(index, false);
            }
          }
        });
      }
    }
    return isValid;
  };

  const editSubServicesError = (index: number, isError: boolean) => {
    setSubServicesError((prev) =>
      [...prev].map((subError, subErrorIndex) =>
        subErrorIndex === index ? isError : subError
      )
    );
  };

  useEffect(() => {
    console.log("SUBSERVICE_ERROR: ", subServicesError);
  }, [subServicesError]);

  const handleCloseToast = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setToastMessage((prev) => ({ ...prev, open: false }));
  };

  if (updateServiceMutation.isLoading) {
    return (
      <div>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </div>
    );
  }

  if (isGetServiceDetailLoading) {
    return (
      <div className="isLoading flex-row">
        <CircularProgress color="primary" />
      </div>
    );
  }

  return (
    <Container className="update-service">
      <ToastMessage
        open={toastMessage.open}
        message={toastMessage.message}
        handleClose={handleCloseToast}
      />
      <Container className="basic-input">
        <div className="page-title">Chỉnh sửa dịch vụ</div>
        <div className="name-input">
          <InputLabel className="input-label input-label__required">
            Tên dịch vụ
          </InputLabel>
          <TextField
            className="input-title"
            fullWidth
            size="small"
            name="title"
            error={isInputError.title}
            FormHelperTextProps={{ error: isInputError.title }}
            helperText={isInputError.title ? getInputErrorMessage("empty") : ""}
            inputProps={{ maxLength: 150 }}
            value={serviceInfo.title}
            onChange={handleInputChange}
          />
        </div>
        <div className="category-input">
          <div className="input-label input-label__required">
            Chọn danh mục dịch vụ
          </div>
          <FormControl sx={{ minWidth: 250 }}>
            <Select
              name="categoryId"
              value={serviceInfo.categoryId.toString()}
              onChange={handleSelectChange}
              displayEmpty
              defaultValue={"1"}
              type="number"
              size="small"
            >
              {CATEGORIES.map((category) => (
                <MenuItem key={category.id} value={category.id}>
                  {category.title}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <div className="location-input flex-row">
          <div className="address-input">
            <div className="input-label input-label__required">Địa chỉ</div>
            <TextField
              name="address"
              value={serviceInfo.address}
              onChange={handleInputChange}
              fullWidth
              size="small"
              error={isInputError.address}
              FormHelperTextProps={{ error: isInputError.address }}
              helperText={
                isInputError.address ? getInputErrorMessage("empty") : ""
              }
            />
          </div>
          <div className="city-input">
            <div className="input-label input-label__required">Thành phố</div>
            <FormControl sx={{ minWidth: 250 }}>
              <Select
                name="cityId"
                value={serviceInfo.cityId.toString()}
                onChange={handleSelectChange}
                defaultValue={"1"}
                displayEmpty
                size="small"
              >
                {CITIES.map((city) => (
                  <MenuItem key={city.id} value={city.id}>
                    {city.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        </div>
        <div className="duration-input">
          <div className="input-label input-label__required">
            Thời gian diễn ra
          </div>
          <TextField
            name="duration"
            value={serviceInfo.duration}
            onChange={handleInputChange}
            fullWidth
            size="small"
            error={isInputError.duration}
            FormHelperTextProps={{ error: isInputError.duration }}
            helperText={
              isInputError.address ? getInputErrorMessage("empty") : ""
            }
          />
        </div>
        <div className="date-input">
          <div className="input-label input-label__required">
            Thời gian diễn ra
          </div>
          <div className="helperText">Định dạng: dd/mm/yyyy</div>
          <div className="event-range__input">
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DatePicker
                label="Ngày bắt đầu"
                value={serviceInfo.eventStart}
                onChange={(newValue: any) => {
                  setServiceInfo((prev) => ({
                    ...prev,
                    eventStart: moment(newValue).format("YYYY-MM-DD"),
                  }));
                }}
                mask="__-__-____"
                inputFormat={"DD-MM-YYYY"}
                renderInput={(params: any) => <TextField {...params} />}
              />
              <span className="to-label">tới</span>
              <DatePicker
                label="Ngày kết thúc"
                value={serviceInfo.eventEnd}
                onChange={(newValue: any) => {
                  setServiceInfo((prev) => ({
                    ...prev,
                    eventEnd: moment(newValue).format("YYYY-MM-DD"),
                  }));
                }}
                mask="__-__-____"
                inputFormat={"DD-MM-YYYY"}
                renderInput={(params: any) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </div>
        </div>
        <div className="image-uploader-section">
          <div className="input-label">Hình ảnh dịch vụ</div>
          <div className="image-uploaders flex-row">
            {imageUploaders.map((imageUploader, index) => (
              <div className="image-uploader" key={index}>
                <ImageUploader
                  selectedImage={
                    images.find((image) => image.id === imageUploader.id)!
                  }
                  handleSelectImage={handleSelectImage}
                  handleRemoveImage={handleRemoveImage}
                  error={index === 0 && isInputError.images}
                />
                <div
                  className={`image-uploader__label ${
                    index === 0 ? "input-label__required" : ""
                  }`}
                >
                  {imageUploader.label}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="description-input">
          <div className="input-label input-label__required">Mô tả dịch vụ</div>
          <TextField
            name="description"
            value={serviceInfo.description}
            onChange={handleInputChange}
            fullWidth
            multiline
            rows={10}
            inputProps={{ maxLength: 3000 }}
            error={isInputError.description}
            FormHelperTextProps={{ error: isInputError.description }}
            helperText={isInputError.title ? getInputErrorMessage("empty") : ""}
          />
        </div>
      </Container>
      <Container className="advance-input">
        <div className="page-title">Thông tin chi tiết</div>
        <div className="sub-services__input">
          <div className="input-label">Danh sách dịch vụ phụ</div>
          {subServices.map((subService, index) => (
            <div key={index} className="sub-service__title-input flex-row">
              <TextField
                label={`Dịch vụ phụ ${index + 1} *`}
                size="small"
                fullWidth
                sx={{ mr: "10px" }}
                value={subServices[index].title}
                onChange={(event) => handleSubServiceInputChange(event, index)}
                error={subServicesError[index]}
                FormHelperTextProps={{ error: subServicesError[index] }}
                helperText={
                  subServicesError[index] ? getInputErrorMessage("empty") : ""
                }
              />
              <IconButton onClick={() => handleRemoveSubService(index)}>
                <DeleteIcon color="primary" />
              </IconButton>
            </div>
          ))}
          <Button className="btn-add" onClick={handleAddNewSubService}>
            Thêm dịch vụ phụ
          </Button>
          <div>
            <div className="input-label">Thông tin chi tiết dịch vụ phụ</div>
            <SubServiceAccordion
              isInputError={subServicesError}
              subServices={subServices}
              handleDataEditCommit={handleDataEditCommit}
            />
          </div>
        </div>
      </Container>
      <Container className="action-section">
        <div className="flex-row flex-jc-c">
          <Button
            variant="outlined"
            className="action-section__btn-cancel"
            onClick={handleCancel}
          >
            Huỷ
          </Button>
          <Button
            variant="contained"
            className="action-section__btn-save"
            onClick={handleUpdateService}
          >
            Lưu dịch vụ
          </Button>
        </div>
      </Container>
      <ConfirmDialog
        open={confirmDialog.open}
        title={confirmDialog.title}
        message={confirmDialog.message}
        handleClose={() => setConfirmDialog({ ...confirmDialog, open: false })}
        handleOpen={() => setConfirmDialog({ ...confirmDialog, open: true })}
        handleConfirmClick={() => navigate("./../list")}
      />
      <AlertDialog
        type={alertDialog.type}
        open={alertDialog.open}
        message={alertDialog.message}
        handleConfirmClick={
          alertDialog.type === "success"
            ? () => navigate("./../list")
            : () => setAlertDialog((prev) => ({ ...prev, open: false }))
        }
      />
    </Container>
  );
};

const imageUploaders = [
  {
    id: 0,
    label: "Ảnh bìa",
  },
  {
    id: 1,
    label: "Ảnh 1",
  },
  {
    id: 2,
    label: "Ảnh 2",
  },
  {
    id: 3,
    label: "Ảnh 3",
  },
  {
    id: 4,
    label: "Ảnh 4",
  },
  {
    id: 5,
    label: "Ảnh 5",
  },
];

const getInputErrorMessage = (errorType: string): string => {
  switch (errorType) {
    case "empty":
      return "Trường này không được để trống";
  }
  return "Lỗi! Vui lòng nhập lại giá trị khác";
};

export default UpdateService;
