import { ACCESS_TOKEN } from "../../../constants";
import request from "../../../shared/services/ApiService";
import { UpdateServiceType } from "../../../shared/types/ServiceType";

export interface IServiceDetail {
  address: string;
  categoryId: number;
  categoryName: string;
  cityId: number;
  cityName: string;
  description: string;
  duration: number;
  eventEnd: string;
  eventStart: string;
  id: number;
  images: string[];
  price: number;
  rateAverage: number;
  rateCount: number;
  shopId: number;
  shopShopName: string;
  status: string;
  subServices: ISubService[];
  title: string;
}

export type ISubService = {
  id: number;
  price: number;
  status: string;
  stockAmount: number;
  title: string;
};

export const getServiceDetails = async (
  serviceId: number
): Promise<IServiceDetail> => {
  const url = `/sp/service/${serviceId}`;
  const results = await request<IServiceDetail>("GET", url).then(
    (res) => res.data
  );
  return results;
};

export const updateShopService = async (
  serviceId: number,
  serviceData: UpdateServiceType
): Promise<IServiceDetail> => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);

  if (!token) {
    return Promise.reject("No access token set.");
  }

  const url = `/sp/shop/service/${serviceId}`;
  const results = await request<IServiceDetail>(
    "PUT",
    url,
    null,
    serviceData
  ).then((res) => res.data);
  return results;
};

export const getBase64FromUrl = async (url: string) => {
  const data = await fetch(url);
  const blob = await data.blob();
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = () => {
      const base64data = reader.result;
      resolve(base64data);
    };
  });
};
