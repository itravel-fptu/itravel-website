import request from "../../../shared/services/ApiService";

export const getOrderDetail = async () => {};

export const orderRecord = {
  fullName: "Nguyễn Văn A",
  phoneNumber: "0123456789",
  createdAt: "17/12/2022",
  status: "Hoàn thành",
  totalPrice: 250000,
  orderItems: [
    {
      id: 111,
      title: "Vé vào cổng Bà Nà - người lớn",
      quantity: 2,
      dateStart: "01/04/2022",
      dateEnd: "08/04/2022",
      totalPrice: 50000,
    },
    {
      id: 222,
      title: "Vé vào cổng Bà Nà - trẻ em",
      quantity: 3,
      dateStart: "01/04/2022",
      dateEnd: "08/04/2022",
      totalPrice: 70000,
    },
    {
      id: 333,
      title: "Vé vào cổng Bà Nà - người lớn",
      quantity: 2,
      dateStart: "12/05/2022",
      dateEnd: "19/05/2022",
      totalPrice: 55000,
    },
    {
      id: 444,
      title: "Nhảy dù Sơn Trà",
      quantity: 1,
      dateStart: "10/03/2022",
      dateEnd: "10/03/2022",
      totalPrice: 100000,
    },
  ],
};

export interface IOrderRecord {
  id: number;
  fullName: string;
  phoneNumber: string;
  createdAt: string;
  status: string;
  totalPrice: number;
  orderItems: OrderRecordItemType[];
}

export type OrderRecordDetailResultType = {
  page: 0;
  results: OrderRecordItemType[];
  size: 0;
  totalItem: 0;
  totalPage: 0;
};

export type OrderRecordItemType = {
  id: number;
  quantity: number;
  image: string;
  mainServiceId: 0;
  mainServiceTitle: string;
  price: 0;
  shopId: 0;
  shopName: string;
  subServiceId: 0;
  subServiceTitle: string;
  usedEnd: string;
  usedStart: string;
};

export const getShopOrderRecordDetail = async (
  shopId: number,
  orderId: number
): Promise<OrderRecordDetailResultType> => {
  const url = `/pal/order/shop/${shopId}/${orderId}`;

  const results = await request<OrderRecordDetailResultType>("GET", url).then(
    (res) => res.data
  );

  return results;
};
