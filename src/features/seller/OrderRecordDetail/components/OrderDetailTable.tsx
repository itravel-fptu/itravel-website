import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material";
import moment from "moment";

import { OrderRecordItemType } from "../OrderRecordDetailController";

interface Column {
  id: "No." | "title" | "quantity" | "dateStart" | "dateEnd" | "totalPrice";
  label: string;
  minWidth?: number;
  align?: "right" | "center";
  format?: (value: number) => string;
}

const columns: Column[] = [
  {
    id: "No.",
    label: "STT",
    minWidth: 50,
    format: (value: number) => value.toString(),
  },
  { id: "title", label: "Dịch vụ con", minWidth: 170 },
  {
    id: "quantity",
    label: "Số lượng",
    minWidth: 150,
    format: (value: number) => value.toString(),
  },
  {
    id: "dateStart",
    label: "Ngày bắt đầu",
    minWidth: 150,
  },
  {
    id: "dateEnd",
    label: "Ngày kết thúc",
    minWidth: 140,
  },
  {
    id: "totalPrice",
    label: "Thành tiền",
    minWidth: 150,
    format: (value: number) => value.toString(),
  },
];

interface Data {
  id: number;
  title: string;
  quantity: number;
  dateStart: string;
  dateEnd: string;
  totalPrice: number;
}

const renderCell = (index: number, row: Data, column: Column) => {
  let value;
  if (column.id === "No.") {
    value = index + 1;
  } else {
    value = row[column.id];
  }

  if (column.id === "dateStart" || column.id === "dateEnd") {
    return (
      <TableCell key={column.id} align={column.align}>
        {moment(value as string).format("DD/MM/YYYY")}
      </TableCell>
    );
  }

  if (column.id === "totalPrice") {
    return (
      <TableCell key={column.id} align={column.align}>
        {value.toLocaleString("vi-VN")}
      </TableCell>
    );
  }

  return (
    <TableCell key={column.id} align={column.align}>
      {column.format && typeof value === "number"
        ? column.format(value)
        : value}
    </TableCell>
  );
};

type OrderDetailTablePropsType = {
  orderRecordItem: OrderRecordItemType[];
};

const OrderDetailTable: React.FC<OrderDetailTablePropsType> = ({
  orderRecordItem,
}) => {
  function createData(orderItem: OrderRecordItemType): Data {
    return {
      id: orderItem.id,
      title: orderItem.subServiceTitle,
      quantity: orderItem.quantity,
      dateStart: orderItem.usedStart,
      dateEnd: orderItem.usedEnd,
      totalPrice: orderItem.price * orderItem.quantity,
    };
  }

  const rows: Data[] = orderRecordItem.map((orderItem: OrderRecordItemType) => createData(orderItem));

  return (
    <div className="order-detail-table">
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 450 }}>
          <Table aria-label="sticky table">
            <TableHead className="order-detail-table__head">
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align="justify"
                    style={{ top: 57, minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody className="order-detail-table__body">
              {rows.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => renderCell(index, row, column))}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default OrderDetailTable;
