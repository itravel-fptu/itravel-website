import { CircularProgress, Container } from "@mui/material";
import { useOutletContext, useParams } from "react-router-dom";
import {
  getShopOrderRecordDetail,
  orderRecord,
  OrderRecordDetailResultType,
} from "./OrderRecordDetailController";

import "./order-record-detail.scss";
import OrderDetailTable from "./components/OrderDetailTable";
import { useQuery } from "react-query";
import { ShopType } from "../../../shared/types/ShopType";
import { useEffect } from "react";

const OrderRecordDetail = () => {
  const { orderId } = useParams();
  const { shopDetails, shopId }: { shopDetails: ShopType; shopId: number } =
    useOutletContext();

  const {
    data: shopOrderRecordDetail,
    refetch,
    isLoading,
    isError,
  } = useQuery<OrderRecordDetailResultType>(
    ["shopOrderRecordDetail", shopId, orderId],
    () => getShopOrderRecordDetail(shopId, Number(orderId)),
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  useEffect(() => {
    if (shopId !== 0) refetch();
  }, [shopId]);
  if (isLoading) {
    return (
      <div className="isLoading flex-row">
        <CircularProgress color="primary" />
      </div>
    );
  }

  if (isError) {
    return (
      <div className="isLoading flex-row">
        Đã có lỗi xảy ra! Vui lòng thử lại sau
      </div>
    );
  }
  return (
    <Container className="order-record-detail">
      <div className="page-title">Thông tin đơn hàng</div>
      <div className="order-info">
        <div className="user">
          <span className="label">Tên người đặt: </span>
          <span>{orderRecord.fullName}</span>
        </div>
        <div className="phone">
          <span className="label">Số điện thoại: </span>
          <span>{orderRecord.phoneNumber}</span>
        </div>
        <div className="created-at">
          <span className="label">Ngày đặt: </span>
          <span>{orderRecord.createdAt}</span>
        </div>
        <div className="status">
          <span className="label">Trạng thái: </span>
          <span>{orderRecord.status}</span>
        </div>
        <div className="total-price">
          <span className="label">Tổng tiền: </span>
          <span>{`${orderRecord.totalPrice.toLocaleString("vi-VN")} VNĐ`}</span>
        </div>
      </div>
      <div>
        {shopOrderRecordDetail && (
          <OrderDetailTable orderRecordItem={shopOrderRecordDetail?.results} />
        )}
      </div>
    </Container>
  );
};

export default OrderRecordDetail;
