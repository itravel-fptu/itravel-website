import moment from "moment";
import { INotification } from "../../shared/types/NotificationType";
import "./notification.scss";

type NotificationPropType = {
  notification: INotification;
};
const NotificationItem: React.FC<NotificationPropType> = ({ notification }) => {
  const { title, message, hasSeen, createdAt } = notification;

  return (
    <div
      className={
        hasSeen
          ? "notification notification-seen flex-column"
          : "notification notification-unseen flex-column"
      }
    >
      <div className="notification__content">
        <div className="notification__title">{title}</div>
        <div className="notification__message">{message}</div>
      </div>
      <div className="notification__created-at">
        {moment(createdAt).format("DD-MM-YYYY")}
      </div>
    </div>
  );
};

export default NotificationItem;
