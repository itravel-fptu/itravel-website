import { ACCESS_TOKEN } from "../../constants";
import request from "../../shared/services/ApiService";
import { INotification } from "../../shared/types/NotificationType";

export const getNotificationList = async (): Promise<INotification[]> => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);
  if (!token) {
    return Promise.reject("No access token set.");
  }
  const url = `/ap/account/notification`;

  const results = await request<INotification[]>("GET", url).then(
    (res) => res.data
  );

  return results;
};
