import { useEffect, useRef, useState } from "react";
import { useQuery } from "react-query";
// Components
import NotificationItem from "./NotificationItem";
import { Badge } from "@mui/material";
// Data & Controller
import { INotification } from "./../../shared/types/NotificationType";
import { getNotificationList } from "./NotificationController";
// Styles & Assets
import "./notification-list.scss";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import { ReactComponent as AnnouncementIcon } from "./../../assets/images/announcement-icon.svg";

const NotificationList: React.FC = () => {
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);
  const handleTogglePopup = () => {
    setIsComponentVisible(!isComponentVisible);
  };

  const [notifications, setNotifications] = useState<INotification[]>([]);
  const { data: notificationList, refetch } = useQuery<INotification[]>(
    "notificationList",
    getNotificationList,
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  useEffect(() => {
    if (isComponentVisible) {
      refetch();
    }
  }, [isComponentVisible]);

  useEffect(() => {
    if (notificationList) setNotifications([...notificationList].reverse());
  }, [notificationList]);

  return (
    <div className="notification-list" ref={ref}>
      <div className="notification-list__icon" onClick={handleTogglePopup}>
        <Badge color="primary">
          <NotificationsNoneIcon />
        </Badge>
      </div>
      {isComponentVisible && (
        <div className="notification-list__popup">
          <div className="title">Thông báo</div>
          {notifications?.map((notification: INotification) => (
            <NotificationItem
              key={notification.id}
              notification={notification}
            />
          ))}

          {notificationList?.length === 0 ? (
            <div className="notification-list__empty flex-column flex-ai-c">
              <AnnouncementIcon width="150px" />
              <div className="message">Hiện bạn chưa có thông báo nào!</div>
            </div>
          ) : (
            ""
          )}
        </div>
      )}
    </div>
  );
};

function useComponentVisible(initialIsVisible: boolean) {
  const [isComponentVisible, setIsComponentVisible] =
    useState(initialIsVisible);
  const ref: any = useRef(null);
  useEffect(() => {
    /**
     * Change state if clicked on outside of element
     */
    function handleClickOutside(event: MouseEvent) {
      if (ref.current && !ref.current.contains(event.target)) {
        setIsComponentVisible(false);
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
  return { ref, isComponentVisible, setIsComponentVisible };
}

export default NotificationList;
