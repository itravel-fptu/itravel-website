
import MoreVertIcon from '@mui/icons-material/MoreVert';
import SendIcon from '@mui/icons-material/Send';
import { Alert, Avatar, Divider, FormControl, Grid, IconButton, Input, InputAdornment, InputLabel, Menu, MenuItem, Paper, Snackbar } from '@mui/material';
import * as React from 'react';
import { useParams } from 'react-router-dom';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import './ServiceView.scss';
import { discussCreateRequestType, DiscussType } from './ServiceViewComment';

type Props = {
    item: DiscussType;
};

const options = [
    'Xóa bình luận',
    'Sửa bình luận',
    'Ẩn bình luận',
];

const ITEM_HEIGHT = 48;

const Comments: React.FC<Props> = ({ item }) => {

    const { serviceId } = useParams();
    const [cmt, setCmt] = React.useState('');
    const cmtHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCmt(event.target.value);
    };
    const [discussData, setDiscussData] = React.useState([] as discussCreateRequestType[]);
    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    async function reverseData() {
        setDiscussData(item.discussReply.reverse());
    }
    React.useEffect(() => {
        reverseData();
    }, []);

    async function discussCreateRequest() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (cmt === '') {
            setSnackBarMsg('Bình luận không thể để trống!');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('POST', `/ap/discuss`, null, {
                discussParentId: item.id,
                mainServiceId: serviceId,
                message: cmt
            })
                .then((res) => {
                    window.location.reload()
                })
                .catch((error) => {
                    console.log('Error: ', error);
                });
        }
    }

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Paper className='paper' key={item.id}>
            <Grid container wrap='nowrap' spacing={2}>
                <Grid item>
                    <Avatar alt='Remy Sharp' src={item.account.imageLink} />
                </Grid>
                <Grid justifyContent='left' item xs zeroMinWidth>
                    <div className='left'>
                        <div className='elem1'>
                            <h4 className='user-name'>{item.account.fullName}</h4>
                            <p className='msg' style={{ textAlign: 'left' }}>
                                {item.message}
                            </p>
                            <p style={{ textAlign: 'left', color: 'gray' }}>
                                {item.createdAt.toString().slice(11, 16)}  &nbsp; {item.createdAt.toString().slice(8, 10) + item.createdAt.toString().slice(4, 8) + item.createdAt.toString().slice(0, 4)}
                            </p>
                        </div>
                    </div>
                    <div className='right'>
                        <div className='elem3'>
                            <div>
                                <IconButton
                                    aria-label='more'
                                    id='long-button'
                                    aria-controls={open ? 'long-menu' : undefined}
                                    aria-expanded={open ? 'true' : undefined}
                                    aria-haspopup='true'
                                    onClick={handleClick}
                                >
                                    <MoreVertIcon />
                                </IconButton>
                                <Menu
                                    id='long-menu'
                                    MenuListProps={{
                                        'aria-labelledby': 'long-button',
                                    }}
                                    anchorEl={anchorEl}
                                    open={open}
                                    onClose={handleClose}
                                    PaperProps={{
                                        style: {
                                            maxHeight: ITEM_HEIGHT * 4.5,
                                            width: '20ch',
                                        },
                                    }}
                                >
                                    {options.map((option) => (
                                        <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
                                            {option}
                                        </MenuItem>
                                    ))}
                                </Menu>
                            </div>
                        </div>
                    </div>
                    <FormControl className='width-full' variant='standard'>
                        <InputLabel htmlFor='a'>Trả lời</InputLabel>
                        <Input
                            id='a'
                            value={cmt}
                            onChange={cmtHandleChange}
                            endAdornment={
                                <InputAdornment position='end'>
                                    <IconButton
                                        aria-label=''
                                        onClick={discussCreateRequest}
                                    >
                                        <SendIcon />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
            </Grid>
            {discussData.map((subItem) => {
                return subItem ? (
                    <div key={subItem.id}>
                        <Divider variant='fullWidth' style={{ margin: '30px 0' }} />
                        <Grid container wrap='nowrap' spacing={2} style={{ padding: '0px 0px 0px 60px' }}>
                            <Grid item>
                                <Avatar alt='Remy Sharp' src={subItem.account.imageLink} />
                            </Grid>
                            <Grid justifyContent='left' item xs zeroMinWidth>
                                <div className='left'>
                                    <div className='elem1'>
                                        <h4 className='user-name'>{subItem.account.fullName}</h4>
                                        <p className='msg' style={{ textAlign: 'left' }}>
                                            {subItem.message}
                                        </p>
                                        <p style={{ textAlign: 'left', color: 'gray' }}>
                                            {subItem.createdAt.toString().slice(11, 16)}  &nbsp; {subItem.createdAt.toString().slice(8, 10) + subItem.createdAt.toString().slice(4, 8) + subItem.createdAt.toString().slice(0, 4)}
                                        </p>
                                    </div>
                                </div>
                                <div className='right'>
                                    <div className='elem3'>
                                        <div>
                                            <IconButton
                                                aria-label='more'
                                                id='long-button'
                                                aria-controls={open ? 'long-menu' : undefined}
                                                aria-expanded={open ? 'true' : undefined}
                                                aria-haspopup='true'
                                                onClick={handleClick}
                                            >
                                                <MoreVertIcon />
                                            </IconButton>
                                            <Menu
                                                id='long-menu'
                                                MenuListProps={{
                                                    'aria-labelledby': 'long-button',
                                                }}
                                                anchorEl={anchorEl}
                                                open={open}
                                                onClose={handleClose}
                                                PaperProps={{
                                                    style: {
                                                        maxHeight: ITEM_HEIGHT * 4.5,
                                                        width: '20ch',
                                                    },
                                                }}
                                            >
                                                {options.map((option) => (
                                                    <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
                                                        {option}
                                                    </MenuItem>
                                                ))}
                                            </Menu>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                ) : null;
            })}

            <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                    {snackBarMsg}
                </Alert>
            </Snackbar>
        </Paper>
    )
}

export default Comments
