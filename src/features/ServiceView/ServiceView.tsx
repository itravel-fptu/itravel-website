import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import GradeIcon from '@mui/icons-material/Grade';
import OtherHousesIcon from '@mui/icons-material/OtherHouses';
import PaidIcon from '@mui/icons-material/Paid';
import StarIcon from '@mui/icons-material/Star';
import { DatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { Alert, Box, Button, IconButton, Rating, Snackbar, Stack, TextField } from '@mui/material';
import * as React from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { useNavigate, useParams } from 'react-router-dom';
import Slider from 'react-slick';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import './ServiceView.scss';
import ServiceViewComment from './ServiceViewComment';
import SubItem from './SubItem';


type ServiceViewProps = {
}

const labels: { [index: string]: string } = {
    1: 'Rất tệ',
    2: 'Tệ',
    3: 'Trung bình',
    4: 'Tốt',
    5: 'Cực tốt',
};

export interface ServiceData {
    address: string;
    categoryId: number;
    categoryName: string;
    cityId: number;
    cityName: string;
    description: string;
    duration: number;
    eventEnd: Date;
    eventStart: Date;
    id: number;
    images: string[];
    shopId: number;
    shopShopName: string;
    status: string;
    subServices: [
        {
            id: number,
            price: number,
            stockAmount: number,
            title: string,
            quantity: number,
            image: string,
            mainServiceId: number,
            mainServiceShopShopName: string,
            mainServiceTitle: string,
            subServiceTitle: string,
            status: string,
        }
    ];
    title: string;
    rateCount: number;
    rateAverage: number;
}

export type ServiceType = {
    address: string;
    categoryId: number;
    categoryName: string;
    cityId: number;
    cityName: string;
    description: string;
    duration: number;
    eventEnd: Date;
    eventStart: Date;
    id: number;
    images: string[];
    shopId: number;
    shopShopName: string;
    status: string;
    subServices: [
        {
            id: number,
            price: number,
            stockAmount: number,
            title: string,
            quantity: number,
            image: string,
            mainServiceId: number,
            mainServiceShopShopName: string,
            mainServiceTitle: string,
            subServiceTitle: string
            status: string,
        }
    ];
    title: string;
    rateCount: number;
    rateAverage: number;
};

export type subServiceType = {
    id: number,
    price: number,
    stockAmount: number,
    title: string,
    quantity: number,
    image: string,
    mainServiceId: number,
    mainServiceShopShopName: string,
    mainServiceTitle: string,
    subServiceTitle: string
    status: string,
};

export interface CartAdd {
    quantity: number;
    subServiceId: number;
}

const ServiceView: React.FC<ServiceViewProps> = () => {
    const [rateValue, setRateValue] = React.useState<number | null>(4);
    const [hover, setHover] = React.useState(-1);

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    let navigate = useNavigate();
    const [serviceData, setServiceData] = React.useState({} as ServiceType);

    const [images, setImages] = React.useState([] as string[]);
    const [title, setTitle] = React.useState('');
    const [duration, setDuration] = React.useState(0);
    const [description, setDescription] = React.useState('');
    const [city, setCity] = React.useState('');
    const [address, setAdress] = React.useState('');
    const [rateCount, setRateCount] = React.useState(0);
    const [rateAverage, setRateAverage] = React.useState(0);
    const [lowestPrice, setLowestPrice] = React.useState(0);
    const [shopName, setShopName] = React.useState('');
    const [shopId, setShopId] = React.useState(0);

    const [dateStart, setDateStart] = React.useState<Date | null>(new Date());
    const dateStartHandleChange = (newDateStart: Date | null) => {
        setDateStart(newDateStart);
    };
    const [subServices, setSubServices] = React.useState([] as subServiceType[]);
    const [changeIds, setChangeIds] = React.useState([] as number[]);
    const [totalPrice, setTotalPrice] = React.useState(0);

    async function fetchData(serviceId: string) {
        await request<ServiceData>('GET', `/sp/service/` + serviceId)
            .then((res) => {
                let serviceData = res.data;
                setServiceData(() => {
                    setTitle(serviceData.title);
                    setDuration(serviceData.duration);
                    setDescription(serviceData.description);
                    setImages(serviceData.images);
                    setCity(serviceData.cityName);
                    setAdress(serviceData.address);
                    setRateCount(serviceData.rateCount);
                    setRateAverage(serviceData.rateAverage);
                    setShopName(serviceData.shopShopName);
                    setShopId(serviceData.shopId);
                    let lowPrice: number = 0;
                    serviceData.subServices.forEach(element => {
                        element.quantity = 0;
                        element.image = serviceData.images[0];
                        element.mainServiceId = serviceData.id;
                        element.mainServiceShopShopName = serviceData.shopShopName;
                        element.mainServiceTitle = serviceData.title;
                        element.subServiceTitle = element.title;
                        if (lowPrice === 0) {
                            lowPrice = element.price;
                        } else if (lowPrice > element.price) { lowPrice = element.price; }
                        setLowestPrice(lowPrice);
                    });
                    setSubServices(serviceData.subServices);
                    return serviceData;
                });
            })
            .catch((error) => {
                console.log('Error: ', error);
                setSnackBarMsg('Có lỗi xảy ra, vui lòng thử lại sau');
                setOpenErrorSnackBar(true);
            });


    }

    const { serviceId } = useParams();
    React.useEffect(() => {
        fetchData(serviceId!);
    }, []);

    const handlePlus = (id: number) => {
        if (changeIds.indexOf(id) === -1) changeIds.push(id);
        setSubServices((prev) =>
            prev.reduce((ack, item) => {
                if (item.id === id) {
                    setTotalPrice(totalPrice + item.price);
                    return [...ack, { ...item, quantity: item.quantity + 1 }];
                } else {
                    return [...ack, item];
                }
            }, [] as subServiceType[])
        );
    };

    const handleMinus = (id: number) => {
        if (changeIds.indexOf(id) === -1) changeIds.push(id);
        setSubServices((prev) =>
            prev.reduce((ack, item) => {
                if (item.id === id) {
                    if (item.quantity === 0) {
                        return [...ack, item];
                    } else {
                        setTotalPrice(totalPrice - item.price);
                        return [...ack, { ...item, quantity: item.quantity - 1 }];
                    }
                } else {
                    return [...ack, item];
                }
            }, [] as subServiceType[])
        );
    }

    async function booking(cartItems: CartAdd[]) {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (cartItems.length < 1) {
            setSnackBarMsg('Vui lòng chọn ít nhất một dịch vụ.');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('POST', `/sp/cart`, null, {
                cartItems,
                dateStart: dateStart,
            })
                .then((res) => {
                    navigate('/cart', { replace: true });
                })
                .catch((error) => {
                    console.log('Error: ', error);
                    setSnackBarMsg('Có lỗi xảy ra, đặt hàng thất bại.');
                    setOpenErrorSnackBar(true);
                });
        }
    }

    async function addToCart(cartItems: CartAdd[]) {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (cartItems.length < 1) {
            setSnackBarMsg('Vui lòng chọn ít nhất một dịch vụ.');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('POST', `/sp/cart`, null, {
                cartItems,
                dateStart: dateStart,
            })
                .then((res) => {
                    setSnackBarMsg('Thêm dịch vụ vào giỏ hàng thành công.');
                    setOpenSuccessSnackBar(true);
                })
                .catch((error) => {
                    console.log('Error: ', error);
                    setSnackBarMsg('Có lỗi xảy ra, thêm vào giỏ hàng thất bại.');
                    setOpenErrorSnackBar(true);
                });
        }
    }

    const handleAddToCart = () => {
        const newArr = subServices.filter((item) => {
            return item.quantity !== 0;
        });
        let purchaseItems = newArr.map(({ ...rest }) => {
            return rest;
        });
        let cartItems = purchaseItems.map((item) => ({
            subServiceId: item.id,
            quantity: item.quantity,
        }));
        addToCart(cartItems as CartAdd[]);
    }

    const handleBooking = () => {
        const newArr = subServices.filter((item) => {
            return item.quantity !== 0;
        });
        let purchaseItems = newArr.map(({ ...rest }) => {
            return rest;
        });
        let cartItems = purchaseItems.map((item) => ({
            subServiceId: item.id,
            quantity: item.quantity,
        }));
        booking(cartItems as CartAdd[]);
    }

    async function rating() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        await request<string>('POST', `/ap/rate`, null, {
            comment: '',
            mainServiceId: serviceData.id,
            ratePoint: rateValue
        })
            .then((res) => {
                setSnackBarMsg('Gửi đánh giá thành công.');
                setOpenSuccessSnackBar(true);
            })
            .catch((error) => {
                console.log('Error: ', error);
                setSnackBarMsg('Gửi đánh giá thất bại, có vẻ bạn đã đánh giá dịch vụ này rồi, vui lòng thử lại sau.');
                setOpenErrorSnackBar(true);
            });

    }

    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openSuccessSnackBar, setOpenSuccessSnackBar] = React.useState(false);
    const handleCloseSuccessSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccessSnackBar(false);
    };
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    return (
        <div className='service-view'>
            <div className='carousel-wrapper margin-top'>
                <div className='carousel center'>
                    <Slider {...settings}>
                        {images.map((item) => {
                            return (
                                <div key={item}>
                                    <img className='img-slider' src={item} />
                                </div>
                            );
                        })}
                    </Slider>
                </div>
            </div>

            <div className='wrapper app'>
                <h1>{title} ({duration} ngày)</h1>
                <h3 className='margin-bottom-1'>Từ {lowestPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })} vnd</h3>
                <div className='service-overview'>
                    <Rating name='read-only' value={rateAverage} size='small' readOnly /> <p className='review-number'> ({rateCount} đánh giá) </p>
                </div>
                <div >
                    <Button className='view-shop-btn'
                        size='medium'
                        startIcon={<OtherHousesIcon fontSize='small' />}
                        variant='outlined'
                        href={'/shop/' + shopId}
                    >
                        {shopName}
                    </Button>
                </div>
                <div className='row'>
                    <div className='column-left' >
                        <p>{description}</p>
                        <hr className='solid' />
                        <h3 className='margin-bottom-1'>Địa chỉ</h3>
                        <p>{address}</p>
                        <p>Thành phố {city}</p>
                        <hr className='solid' />
                        <h3 className='margin-bottom-1'>Vị trí</h3>
                        <iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3835.8639210988144!2d108.25836811485709!3d15.968481188943308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142116949840599%3A0x365b35580f52e8d5!2zxJDhuqFpIGjhu41jIEZQVCDEkMOgIE7hurVuZyAoRlBUIHVuaXZlcnNpdHkgRGEgTmFuZyk!5e0!3m2!1svi!2s!4v1649768329154!5m2!1svi!2s' height='450' className='iframe'></iframe>
                    </div>
                    <div className='column-right'>
                        <div className='sub-service-wrapper'>
                            <p className='sub-service-label'>Chọn ngày tham quan</p>
                            <div className='calendar-div calendar'>
                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                    <Stack spacing={3} className='input-left'>
                                        <DatePicker
                                            label='Bắt đầu'
                                            inputFormat='dd/MM/yyyy'
                                            disablePast={true}
                                            value={dateStart}
                                            onChange={dateStartHandleChange}
                                            renderInput={(params) => <TextField {...params} />}
                                        />
                                    </Stack>
                                </LocalizationProvider>
                            </div>
                            <p className='sub-service-label'>Chọn số lượng gói dịch vụ</p>
                            <div className='service-amount'>
                                {subServices.map((item) => {
                                    if (item.status === 'ACTIVE' && item.stockAmount !== 0) {
                                        return (
                                            <SubItem
                                                key={item.id}
                                                item={item}
                                                plus={handlePlus}
                                                minus={handleMinus}
                                            />
                                        );
                                    } else if (item.status === 'ACTIVE' && item.stockAmount === 0) {
                                        return (
                                            <div key={item.id} className='amount-div'>
                                                <p className='sub-label'>{item.title}</p>
                                                <div className=''>
                                                    <p className='sub-price-label'>
                                                        {item.price.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ (Hết hàng)
                                                    </p>
                                                </div>
                                            </div>
                                        )
                                    } else if (item.status === 'DELETED') {
                                        return (null);
                                    }
                                    else {
                                        return (
                                            <div key={item.id} className='amount-div'>
                                                <p className='sub-label'>{item.title}</p>
                                                <div className=''>
                                                    <p className='sub-price-label'>
                                                        {item.price.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ (Tạm ngừng hoạt động)
                                                    </p>
                                                </div>
                                            </div>
                                        )
                                    }
                                })}
                            </div>
                            <p className='total-price'>
                                {totalPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ
                            </p>

                            <div className='booking-div'>
                                <Button className='btn-cmt'
                                    size='medium'
                                    startIcon={<PaidIcon fontSize='large' />}
                                    variant='contained'
                                    onClick={handleBooking}
                                >
                                    Đặt ngay
                                </Button>
                                <IconButton
                                    color='primary'
                                    aria-label='add to shopping cart'
                                    onClick={handleAddToCart}
                                >
                                    <AddShoppingCartIcon />
                                </IconButton>
                                <Snackbar open={openSuccessSnackBar} autoHideDuration={2000} onClose={handleCloseSuccessSnackBar}>
                                    <Alert onClose={handleCloseSuccessSnackBar} severity='success' sx={{ width: '100%' }}>
                                        {snackBarMsg}
                                    </Alert>
                                </Snackbar>
                                <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                                    <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                                        {snackBarMsg}
                                    </Alert>
                                </Snackbar>
                            </div>
                        </div>
                    </div>
                </div>
                <hr className='solid' />

                <h3 className='margin-bottom-1'>Đánh giá</h3>
                <div className='margin-bottom-1'>
                    <Box
                        sx={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        <Rating
                            name='hover-feedback'
                            value={rateValue}
                            precision={1}
                            onChange={(event, newValue) => {
                                setRateValue(newValue);
                            }}
                            onChangeActive={(event, newHover) => {
                                setHover(newHover);
                            }}
                            emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize='inherit' />}
                        />
                        {rateValue !== null && (
                            <Box sx={{ ml: 2 }}>{labels[hover !== -1 ? hover : rateValue]}</Box>
                        )}
                    </Box>
                </div>
                <div className='rating'>
                    <div className='margin-right-2'>
                        <Button className='rating-btn'
                            size='medium'
                            startIcon={<GradeIcon fontSize='small' />}
                            variant='outlined'
                            onClick={rating}
                        >
                            Đánh giá
                        </Button>
                    </div>
                </div>
                <hr className='solid' />

                <ServiceViewComment />
            </div>
        </div >
    )
}

export default ServiceView
