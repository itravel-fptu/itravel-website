import SendIcon from '@mui/icons-material/Send';
import { Alert, Avatar, FormControl, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, Paper, Snackbar } from '@mui/material';
import * as React from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { useParams } from 'react-router-dom';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import { UserProfileData } from '../Profile/Profile';
import Comments from './Comments';
import './ServiceView.scss';

type ServiceViewCommentProps = {
}

export interface ReceivedData {
    page: number,
    totalResults: number,
    totalPages: number,
    results: [
        [
            {
                id: number,
                account: {
                    id: number,
                    fullName: string,
                    email: string,
                    imageLink: string
                },
                message: string,
                createdAt: Date,
                discussReply: [
                    {
                        id: number,
                        account: {
                            id: number,
                            fullName: string,
                            email: string,
                            imageLink: string
                        },
                        message: string,
                        createdAt: Date,
                        discussReply: null
                    }
                ]
            }
        ]
    ]
}

export type DiscussType = {
    id: number,
    account: {
        id: number,
        fullName: string,
        email: string,
        imageLink: string
    },
    message: string,
    createdAt: Date,
    discussReply: [
        {
            id: number,
            account: {
                id: number,
                fullName: string,
                email: string,
                imageLink: string
            },
            message: string,
            createdAt: Date,
            discussReply: null
        }
    ]
};

export type discussCreateRequestType = {
    id: number,
    account: {
        id: number,
        fullName: string,
        email: string,
        imageLink: string
    },
    message: string,
    createdAt: Date,
    discussReply: null
};

const ServiceViewComment: React.FC<ServiceViewCommentProps> = () => {

    const [discussData, setDiscussData] = React.useState([] as DiscussType[]);
    const [cmt, setCmt] = React.useState('');
    const [currentUserAvatar, setCurrentUserAvatar] = React.useState('');
    const cmtHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCmt(event.target.value);
    };
    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    async function fetchData(serviceId: string) {
        await request<ReceivedData>('GET', `/ap/discuss/` + serviceId)
            .then((res) => {
                let receivedData = res.data;
                setDiscussData(receivedData.results[0].reverse());
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
        await request<UserProfileData>('GET', `/account`)
            .then((res) => {
                let profileData = res.data;
                setCurrentUserAvatar(profileData.imageLink);
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }

    const { serviceId } = useParams();
    React.useEffect(() => {
        fetchData(serviceId!);
    }, []);

    async function discussCreateRequest() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (cmt === '') {
            setSnackBarMsg('Bình luận không thể để trống!');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('POST', `/ap/discuss`, null, {
                discussParentId: 0,
                mainServiceId: serviceId,
                message: cmt
            })
                .then((res) => {
                    window.location.reload()
                })
                .catch((error) => {
                    console.log('Error: ', error);
                });
        }
    }

    return (
        <div>
            <h3>Bàn luận</h3>
            <Paper className='paper'>
                <Grid container wrap='nowrap' spacing={2}>
                    <Grid item>
                        <Avatar alt='Remy Sharp' src={currentUserAvatar} />
                    </Grid>
                    <Grid justifyContent='left' item xs zeroMinWidth>
                        <FormControl className='width-full' variant='outlined'>
                            <InputLabel htmlFor='outlined-adornment-password'>Bình luận</InputLabel>
                            <OutlinedInput
                                id=''
                                endAdornment={
                                    <InputAdornment position='end'>
                                        <IconButton
                                            aria-label=''
                                            edge='end'
                                            onClick={discussCreateRequest}
                                        >
                                            <SendIcon />
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label='Bình luận'
                                value={cmt}
                                onChange={cmtHandleChange}
                            />
                        </FormControl>
                    </Grid>
                </Grid>
            </Paper>

            {discussData.length === 0 ?
                <Paper className='paper'>
                    <p className='empty-discuss'>Hiện chưa có bình luận nào, hãy trở thành người đầu tiên bình luận về dịch vụ này!</p>
                </Paper>
                : null}
            {discussData.map((item) => {
                return item ? (
                    <Comments
                        key={item.id}
                        item={item}
                    />
                ) : null;
            })}

            <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                    {snackBarMsg}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default ServiceViewComment
