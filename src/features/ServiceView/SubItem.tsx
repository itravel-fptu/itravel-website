import { Button } from '@mui/material';
import { subServiceType } from './ServiceView';
import './ServiceView.scss';


type Props = {
  item: subServiceType;
  plus: (id: number) => void;
  minus: (id: number) => void;
};
const SubItem: React.FC<Props> = ({
  item,
  plus,
  minus,
}) => {
  return (
    <div key={item.id} className='amount-div'>
      <p className='sub-label'>{item.title}</p>
      <div className='amount'>
        <Button
          size='small'
          disableElevation
          variant='contained'
          onClick={() => minus(item.id)}
        >
          -
        </Button>
        <p>
          <span className='amount-span'>{item.quantity}</span>
        </p>
        <Button
          size='small'
          disableElevation
          variant='contained'
          onClick={() => plus(item.id)}
        >
          +
        </Button>
        <p className='sub-price-label'>
          {item.price.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ
        </p>
      </div>
    </div>
  );
};

export default SubItem;
