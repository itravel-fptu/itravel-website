import SearchIcon from "@mui/icons-material/Search";

type SearchBarPropType = {
  keywordSearch: string;
  handleKeywordSearchChange: (event: React.FormEvent<HTMLInputElement>) => void;
};

const SearchBar: React.FC<SearchBarPropType> = ({
  keywordSearch,
  handleKeywordSearchChange,
}) => {
  return (
    <div className="search-bar flex-row flex-jc-sb">
      <input
        type="text"
        placeholder="Tìm kiếm địa điểm, hoạt động"
        value={keywordSearch}
        onChange={handleKeywordSearchChange}
      />
      <div className="search-bar__button">
        <SearchIcon fontSize="medium" sx={{ color: "#fff", m: "auto" }} />
      </div>
    </div>
  );
};

export default SearchBar;
