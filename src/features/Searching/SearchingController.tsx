import request from "../../shared/services/ApiService";
import { ServiceCardType } from "../../shared/types/ServiceType";

export type SearchingResultListType = {
  page: number;
  serviceInfos: ServiceCardType[];
  totalItem: number;
  totalPage: number;
};

export const filterServices = async (
  categoryIds?: number[],
  sortType?: number,
  title?: string,
  size?: number,
  page?: number
): Promise<SearchingResultListType> => {
  const url = `/sp/service/filter`;

  type requestDataType = {
    categoryIds?: number[];
    sortType?: number;
    title?: string;
    size?: number;
    page?: number;
  };

  const data: requestDataType = {
    categoryIds: categoryIds,
    sortType: sortType,
    title: title,
    size: size,
    page: page,
  };

  const results = await request<SearchingResultListType>(
    "POST",
    url,
    null,
    data
  ).then((res) => res.data);

  return results;
};
