import request from "../../shared/services/ApiService";
import { ServiceCardType } from "../Home/HomeTypes";

export async function searchServices(params?: URLSearchParams) {
  const url = "/service/search";
  const services = await request<ServiceCardType[]>("GET", url, params).then(
    (response) => response.data
  );
  return services;
}
