import { useEffect, useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import { useMutation } from "react-query";

import { CategoryCardType } from "../../shared/types/ServiceType";

import { categories as fakeCategories } from "./../../shared/services/FakeData";

// MUI Components
import Container from "@mui/material/Container";
import Checkbox from "@mui/material/Checkbox";

import {
  FormGroup,
  FormControlLabel,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
  Grid,
  Pagination,
} from "@mui/material";

// Styles
import "./search-result-list.scss";
import SearchBar from "./components/SearchBar";
import ServiceCard from "../../shared/Component/ServiceCard";
import { filterServices, SearchingResultListType } from "./SearchingController";
import ServiceCardSkeleton from "../../shared/Component/ServiceCardSkeleton/ServiceCardSkeleton";
import { serviceCardData } from "../../shared/Component/ServiceCardSkeleton/ServiceCardDataPlaceholder";

export type SearchParamsType = {
  query: string;
  category?: string[];
  sort?: string;
};

function SearchResultList() {
  const [searchParams, setSearchParams] = useSearchParams();
  const [keywordSearch, setKeywordSearch] = useState(
    searchParams.get("keyword") || ""
  );
  const [sortFilter, setSortFilter] = useState(
    searchParams.get("sortType") || "1"
  );
  const [categoriesFilter, setCategoriesFilter] = useState(
    searchParams.get("category_id")?.split(",") ||
      (["1", "2", "3", "4", "5", "6"] as string[])
  );

  const [page, setPage] = useState(Number(searchParams.get("page")) || 1);
  const [size, setSize] = useState(12);

  type searchTypes = {
    categoryIds?: number[];
    sortType?: number;
    title?: string;
    page?: number;
    size?: number;
  };

  const searchServicesMutation = useMutation(
    ({ categoryIds, sortType, title, size, page }: searchTypes) =>
      filterServices(categoryIds, sortType, title, size, page)
  );

  // Sort list by type (Most purchase, Low price, High price, Newest)
  const handleSortFilterChange = (event: SelectChangeEvent) => {
    setSortFilter(event.target.value);
  };

  useEffect(() => {
    if (sortFilter === "") {
      searchParams.delete("sortType");
    } else {
      searchParams.set("sortType", sortFilter);
    }
    setSearchParams(searchParams);
    handleFilterServices();
  }, [sortFilter]);

  // Search list by keyword
  const handleKeywordSearchChange = (
    event: React.FormEvent<HTMLInputElement>
  ): void => {
    setKeywordSearch(event.currentTarget.value);
  };

  useEffect(() => {
    if (keywordSearch === "") {
      searchParams.delete("keyword");
    } else {
      searchParams.set("keyword", keywordSearch);
    }
    setSearchParams(searchParams);

    const delayDebounceFn = setTimeout(() => {
      handleFilterServices();
    }, 1500);
    return () => clearTimeout(delayDebounceFn);
  }, [keywordSearch]);

  // Filter list by categories
  const handleCategoryCheckboxChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    changedCategoryId: number
  ): void => {
    if (event.target.checked) {
      setCategoriesFilter(
        categoriesFilter.concat(changedCategoryId.toString())
      );
    } else {
      setCategoriesFilter(
        categoriesFilter.filter(
          (categoryId) => categoryId !== changedCategoryId.toString()
        )
      );
    }
  };

  useEffect(() => {
    if (categoriesFilter.length === 0) {
      searchParams.delete("category_id");
    } else {
      searchParams.set("category_id", categoriesFilter.join());
    }
    setSearchParams(searchParams);

    handleFilterServices();
  }, [categoriesFilter]);

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setPage(value);
  };

  useEffect(() => {
    if (page > 0) searchParams.set("page", page.toString());
    setSearchParams(searchParams);
    handleFilterServices();
  }, [page]);

  const handleFilterServices = () => {
    let categoryIds: number[] = [];
    if (categoriesFilter.length === 0) {
      categoryIds = [1];
    } else {
      categoryIds = categoriesFilter.map((category: string) =>
        Number(category)
      );
    }

    let sortType: number = Number(sortFilter);
    let title: string = keywordSearch;
    let currentPage: number = page;
    let sizePerPage: number = size;
    searchServicesMutation.mutate({
      categoryIds,
      sortType,
      title,
      size: sizePerPage,
      page: currentPage,
    });
  };

  return (
    <Container className="result-page">
      <div className="result-page__title">Tìm kiếm</div>
      <div className="result-page__sub-title">
        Kết quả tìm kiếm khớp với "{keywordSearch}"
      </div>

      <div className="flex-row result-page__search-section">
        <div className="section-left">
          <div className="category-filter">
            <div className="category-filter__title">Danh mục</div>
            <FormGroup>
              {fakeCategories?.map((category: CategoryCardType) => (
                <div className="category-checkbox" key={category.id}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={categoriesFilter.includes(
                          category.id.toString()
                        )}
                        onChange={(event) =>
                          handleCategoryCheckboxChange(event, category.id)
                        }
                      />
                    }
                    label={category.title}
                  />
                </div>
              ))}
            </FormGroup>
            <hr style={{ border: ".5px solid #d6d6d6" }} />
          </div>
        </div>
        <div className="section-right">
          {/* <div className="search-results">
            {searchResults?.map((service) => {
              <div>{service.title}</div>;
            })}
          </div> */}
          <div className="section-right__top flex-row flex-jc-sb flex-ai-c">
            <SearchBar
              keywordSearch={keywordSearch}
              handleKeywordSearchChange={handleKeywordSearchChange}
            />
            <div className="sort-filter">
              <FormControl size="small" sx={{ minWidth: 180 }}>
                <Select
                  value={sortFilter}
                  onChange={handleSortFilterChange}
                  defaultValue={"1"}
                >
                  <MenuItem value={"1"}>Giá thấp đến cao</MenuItem>
                  <MenuItem value={"2"}>Giá cao đến thấp</MenuItem>
                  <MenuItem value={"3"}>Mua nhiều nhất</MenuItem>
                  <MenuItem value={"4"}>Dịch vụ mới nhất</MenuItem>
                </Select>
              </FormControl>
            </div>
          </div>
          <div className="section-right__result-section">
            {searchServicesMutation.data?.serviceInfos?.length === 0 ? (
              <div className="empty">
                Không có kết quả nào khớp với yêu cầu tìm kiếm của bạn
                <div>Đề xuất: Thử nhập từ khoá khác</div>
              </div>
            ) : (
              ""
            )}

            <Grid
              className="result-list"
              container
              columnSpacing={6}
              rowSpacing={1}
            >
              {searchServicesMutation.data?.serviceInfos?.map(
                (service, index) => (
                  <Grid item key={service.id} lg={4}>
                    <Link to={`/service/${service.id}`}>
                      <ServiceCard service={service} />
                    </Link>
                  </Grid>
                )
              )}

              {searchServicesMutation.isLoading &&
                serviceCardData.map((service, index) => (
                  <Grid item key={service.id} lg={4}>
                    <ServiceCardSkeleton />
                  </Grid>
                ))}
            </Grid>

            <div className="pagination-filter flex-row">
              <Pagination
                count={
                  searchServicesMutation.data?.totalPage
                    ? searchServicesMutation.data.totalPage
                    : 5
                }
                page={page}
                onChange={handlePageChange}
                variant="outlined"
                color="primary"
                shape="rounded"
              />
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default SearchResultList;
