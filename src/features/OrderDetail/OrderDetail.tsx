import { Alert, Snackbar, TextField } from '@mui/material';
import * as React from 'react';
import { useParams } from 'react-router-dom';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import './OrderDetail.scss';
import OrderItem from './OrderItem';

export interface OrderData {
    createdAt: Date;
    fullName: string;
    id: number;
    orderBillId: string;
    orderItemInfos: [
        {
            id: number,
            image: string,
            mainServiceId: number,
            mainServiceTitle: string,
            price: number,
            quantity: number,
            shopId: number,
            shopName: string,
            subServiceId: number,
            subServiceTitle: string,
            usedEnd: Date,
            usedStart: Date
        }
    ],
    phoneNumber: string,
    status: string,
    totalPrice: number
}

export type OrderItemType = {
    id: number,
    image: string,
    mainServiceId: number,
    mainServiceTitle: string,
    price: number,
    quantity: number,
    shopId: number,
    shopName: string,
    subServiceId: number,
    subServiceTitle: string,
    usedEnd: Date,
    usedStart: Date
};

const OrderDetail: React.FC = () => {

    const [orderItemData, setOrderItemsData] = React.useState([] as OrderItemType[]);
    const [fullName, setFullName] = React.useState('');
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [totalPrice, setTotalPrice] = React.useState(0);
    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    async function fetchData(orderId: string) {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        await request<OrderData>('GET', `/pal/order/` + orderId)
            .then((res) => {
                let orderData = res.data;
                setFullName(orderData.fullName);
                setPhoneNumber(orderData.phoneNumber);
                setTotalPrice(orderData.totalPrice);
                setOrderItemsData(orderData.orderItemInfos);
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }
    const { orderId } = useParams();
    React.useEffect(() => {
        fetchData(orderId!);
    }, []);

    return (
        <div className='order-detail'>
            <div className='wrapper center'>
                <h2 className='screen-title'>Thông tin liên lạc</h2>
                <div className='service-box'>
                    <div className='col col-input-left'>
                        <p>Họ và tên đầy đủ</p>
                    </div>
                    <div className='col col-input-right'>
                        <p>Số điện thoại liên lạc</p>
                    </div>
                    <div className='row'>
                        <div className='col col-input-left'>
                            <TextField className='input-left' id='' value={fullName} variant='outlined' size='small' disabled />
                        </div>
                        <div className='col col-input-right'>
                            <TextField className='input-left' id='' value={phoneNumber} variant='outlined' size='small' disabled />
                        </div>
                    </div>
                </div>
                <h2 className='screen-title'>Thông tin đơn hàng</h2>
                <div className='header-table'>
                    <p id='p-product'>Sản phẩm</p>
                    <p id='p-start-date'>Ngày bắt đầu sử dụng</p>
                    <p>Đơn giá</p>
                    <p id='p-amount'>Số lượng</p>
                    <p>Số tiền</p>
                    <p>Thao tác</p>
                </div>

                <div className='items'>
                    {orderItemData.length === 0 ? <h3>Thông tin đơn hàng trống!</h3> : null}
                    {orderItemData.map((item) => {
                        return item.id ? (
                            <OrderItem
                                key={item.id}
                                item={item}
                            />
                        ) : null;
                    })}
                    <div className='service-component'>
                        <div className='service-footer' >
                            <p className='total-price'>
                                Tổng thanh toán:
                                {totalPrice.toLocaleString(undefined, {
                                    maximumFractionDigits: 2,
                                })}
                                đ
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                    {snackBarMsg}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default OrderDetail