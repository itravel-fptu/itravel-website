import OtherHousesIcon from '@mui/icons-material/OtherHouses';
import { LocalizationProvider, MobileDatePicker } from '@mui/lab';
import DateAdapter from '@mui/lab/AdapterMoment';
import { Alert, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, InputLabel, MenuItem, OutlinedInput, Select, SelectChangeEvent, Snackbar, TextField } from '@mui/material';
import moment from 'moment';
import React from 'react';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import { UserProfileData } from '../Profile/Profile';
import { OrderItemType } from './OrderDetail';
import './OrderItem.scss';

const reasons = [
  'Lý do số một',
  'Lý do số hai',
  'Lý do số ba',
  'Lý do số bốn',
  'Lý do số năm',
  'Lý do số sáu',
  'Lý do khác',
];

type Props = {
  item: OrderItemType;
};

export interface RefundRequestData {
  accountId: number,
  orderItemId: number,
  reason: string,
  reportType: string
}

export type RefundRequestType = {
  accountId: number,
  orderItemId: number,
  reason: string,
  reportType: string
};


const OrderItem: React.FC<Props> = ({ item }) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [shopId, setShopId] = React.useState(item.shopId);
  const [reason, setReason] = React.useState('');
  const [description, setDescription] = React.useState('');
  const descriptionHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };
  const [currentUserID, setCurrentUserId] = React.useState(0);

  const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
  const [openSuccessSnackBar, setOpenSuccessSnackBar] = React.useState(false);
  const handleCloseSuccessSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSuccessSnackBar(false);
  };
  const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
  const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenErrorSnackBar(false);
  };

  const handleChange = (event: SelectChangeEvent) => {
    setReason(event.target.value as string);
  };

  async function createRefundRequest() {
    if (reason === '' || description === '') {
      setSnackBarMsg('Vui lòng điền tất cả thông tin để gửi yêu cầu hoàn trả!');
      setOpenErrorSnackBar(true);
    } else {
      await request<string>('POST', `/pal/refund/new`, null, {
        accountId: currentUserID,
        orderItemId: item.id,
        reason: description,
        reportType: reason
      })
        .then((res) => {
          setSnackBarMsg('Gửi yêu cầu hoàn trả thành công, chúng tôi sẽ xem xét và trả lời quý khách sớm nhất có thể.');
          handleClose();
          setOpenSuccessSnackBar(true);
        })
        .catch((error) => {
          console.log('Error: ', error);
          setSnackBarMsg('Gửi yêu cầu hoàn trả thất bại, có lỗi xảy ra.');
          handleClose();
          setOpenErrorSnackBar(true);
        });
    }
  }

  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      return Promise.reject('No access token set.');
    }
    await request<UserProfileData>('GET', `/account`)
      .then((res) => {
        let profileData = res.data;
        setCurrentUserId(profileData.id);
      })
      .catch((error) => {
        console.log('Error: ', error);
      });
  }
  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='order-item'>
      <div className='img-title'>
        <img className='img' src={item.image} />
        <div className='title'>
          <b>{item.mainServiceTitle}</b>
          <p>{item.subServiceTitle}</p>
          <Button href={'/shop/' + shopId} className='btn-left' id='btn-view-shop' variant='outlined' size='small' startIcon={<OtherHousesIcon />}>
            {item.shopName}
          </Button>
        </div>
      </div>
      <div className='start-date'>
        <LocalizationProvider dateAdapter={DateAdapter}>
          <MobileDatePicker
            inputFormat='DD/MM/yyyy'
            value={moment(item.usedStart).toDate()}
            onChange={(newDate: Date | null) => { }}
            disabled
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </div>
      <div className='price'>
        <p>
          {item.price.toLocaleString(undefined, {
            maximumFractionDigits: 2,
          })}
          đ
        </p>
      </div>
      <div className='amount'>
        <div className='center'>
          <p> {item.quantity}</p>
        </div>
      </div>
      <div className='total-price'>
        <p>
          {(item.price * item.quantity).toLocaleString(undefined, {
            maximumFractionDigits: 2,
          })}
          đ
        </p>
      </div>
      <div className='total-price'>
        <Button onClick={handleOpen} className='act' variant='outlined' size='medium'>
          Hoàn trả
        </Button>
      </div>

      <Dialog id='refund-request-form' open={open} onClose={handleClose}>
        <DialogTitle>Yêu cầu hoàn tiền dịch vụ (Mã đơn: {item.id})</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Vui lòng mô tả chính xác lí do yêu cầu hoàn tiền để được hỗ trợ nhanh nhất.
          </DialogContentText>
          <br />
          <div>
            <FormControl sx={{ width: 300 }}>
              <InputLabel id='multiple-reason-label'>Lý do</InputLabel>
              <Select
                labelId='multiple-reason-label'
                id=''
                value={reason}
                onChange={handleChange}
                input={<OutlinedInput label='Reason' />}
              >
                {reasons.map((reason) => (
                  <MenuItem
                    key={reason}
                    value={reason}
                  >
                    {reason}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <br />
          <div>
            <InputLabel className='label'>Mô tả chi tiết</InputLabel>
            <TextField
              multiline
              fullWidth={true}
              rows={5}
              value={description}
              onChange={descriptionHandleChange}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Hủy</Button>
          <Button onClick={createRefundRequest}>Xác Nhận</Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={openSuccessSnackBar} autoHideDuration={2000} onClose={handleCloseSuccessSnackBar}>
        <Alert onClose={handleCloseSuccessSnackBar} severity='success' sx={{ width: '100%' }}>
          {snackBarMsg}
        </Alert>
      </Snackbar>
      <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
        <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
          {snackBarMsg}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default OrderItem;
