import React from "react";
import { Navigate } from "react-router-dom";
import useAuth from "./../../hooks/useAuth";

const Login = () => {
  const auth = useAuth();

  if (auth.user) {
    return <Navigate to="/" replace />;
  }

  return window.location.replace("https://www.google.com");
};

export default Login;
