import "./about-us.scss";

const AboutUs: React.FC = () => {
  return (
    <div className="about">
      <img
        className="headimg"
        src={require("./../../assets/images/about-us.jpg")}
      />
      <div className="div-block">
        <p>
          <h2>iTravel là gì?</h2>
          <p className="bold-text">
            <b>Bạn muốn khám phá mọi nơi trên mảnh đất hình chữ S?</b>
          </p>
          <br></br>
          Là một nền tảng du lịch trực tuyến, iTravel sẽ cùng đồng hành đến từng
          ngõ ngách của Việt Nam và mang lại cho bạn những trải nghiệm du lịch
          đầy thú vị, tiện lợi và dễ dàng.
        </p>
      </div>
      <div className="div-img-left highlight div-block">
        <p>
          <h2>Nhiều địa điểm nổi tiếng</h2>
          <p className="bold-text">
            <b>Bạn có biết Việt Nam có bao nhiêu địa điểm nổi tiếng?</b>
          </p>
          <br></br>
          Cùng iTravel tìm kiếm, trải nghiệm và đánh giá nhiều địa điểm du lịch
          nổi tiếng trên khắp Việt Nam, từ Bắc tới Nam, từ những hang động thần
          bí đến những bãi biển tuyệt đẹp và còn rất nhiều trải nghiệm du lịch
          tuyệt vời đang chờ đón bạn.
        </p>
      </div>
      <div className="div-img-right div-block">
        <p>
          <h2>Cam kết kết lượng với giá cả tốt nhất</h2>
          <p className="bold-text">
            <b>Bạn e ngại vì chất lượng dịch vụ và chi phí du lịch?</b>
          </p>
          <br></br>
          iTravel sẽ giúp bạn tận hưởng thời gian thư giãn, nghỉ ngơi, du lịch
          thật trọn vẹn các dịch vụ chất lượng tốt nhất mà không cần phải lo
          lắng về giá cả.
        </p>
      </div>
      <div className="div-img-left highlight div-block">
        <p>
          <h2>Dễ dàng đặt hàng và thanh toán</h2>
          <p className="bold-text">
            <b>Bạn lo lắng việc đặt hàng và thanh toán gặp khó khăn?</b>
          </p>
          <br></br>
          Đặt hàng và thanh toán trên iTravel vô cùng dễ dàng, tiện lợi chỉ với
          vài cú click chuột. Chúng tôi hỗ trợ rất nhiều hình thức thanh toán
          phổ biến như Momo, Paypal,...
        </p>
      </div>
      <div className="div-img-right iframe">
        <div className=" div-block">
          <p>
            <p className="bold-text">
              <b>Công ty TNHH iTravel</b>
            </p>
            <br></br>
            Địa chỉ: Nam kỳ Khởi Nghĩa, khu đô thị FPT, Đà Nẵng, Việt Nam
            <br></br>
            Email: iTravel@gmail.com
            <br></br>
            Hotline: 0987654321
          </p>
        </div>
        <div className="div-iframe">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4561.432656023743!2d108.25758711938106!3d15.97743366736621!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421084250439e9%3A0x9b8e9f1a0f1a0ea0!2zTmFtIEvhu7MgS2jhu59pIE5naMSpYSwgS2h2IMSRw7QgdGjhu4sgRlBUIENpdHksIEjDsmEgSOG6o2ksIE5nxakgSMOgbmggU8ahbiwgxJDDoCBO4bq1bmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1640798946634!5m2!1svi!2s"
            width="600"
            height="450"
            loading="lazy"
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
