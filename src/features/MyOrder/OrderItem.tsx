
import OtherHousesIcon from '@mui/icons-material/OtherHouses';
import ReviewsIcon from '@mui/icons-material/Reviews';
import { Button } from '@mui/material';
import * as React from 'react';
import { OrderItemType } from './MyOrder';
import './MyOrder.scss';

type Props = {
    item: OrderItemType;
};

const OrderItem: React.FC<Props> = ({ item }) => {
    return (
        <div>
            <div className='item-header'>
                <div className='service-component'>
                    <p id='p-shop-name' className='vertical-center'>{item.shopName}</p>
                    <Button href={'/shop/' + item.shopId} className='btn-left' id='btn-view-shop' variant='outlined' size='small' startIcon={<OtherHousesIcon />}>
                        Xem cửa hàng
                    </Button>
                    <Button href={'/service/' + item.mainServiceId} className='btn-left' variant='outlined' startIcon={<ReviewsIcon />}>
                        Đánh giá
                    </Button>
                </div>
                <div className='service-component'>
                    <div className='item'>
                        <img className='item-image' src={item.image} />
                        <div className='item-detail'>
                            <p id='service-title'>{item.mainServiceTitle}</p>
                            <p id='service-type'>{item.subServiceTitle}</p>
                            <p id='service-amount'>Số lượng: {item.quantity}</p>
                        </div>
                    </div>
                </div>
                <hr className='solid' />
            </div>
        </div>
    )
}

export default OrderItem
