
import * as React from 'react';
import { OrderType } from './MyOrder';
import './MyOrder.scss';
import OrderItem from './OrderItem';

type Props = {
    item: OrderType;
};

const Order: React.FC<Props> = ({ item }) => {
    return (
        <div>
            {item.orderItemInfos.map((item) => {
                return item.id ? (
                    <OrderItem
                        key={item.id}
                        item={item}
                    />
                ) : null;
            })}
        </div>
    )
}

export default Order
