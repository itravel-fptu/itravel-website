import InfoIcon from '@mui/icons-material/Info';
import { Alert, Button, Snackbar, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { ACCESS_TOKEN } from '../../constants';
import request from '../../shared/services/ApiService';
import './MyOrder.scss';
import Order from './Order';

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role='tabpanel'
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography component={'span'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export interface ReceivedData {
    page: number,
    results: [
        {
            createdAt: Date,
            id: number,
            orderBillId: string,
            orderItemInfos: [
                {
                    id: number,
                    image: string,
                    mainServiceId: number,
                    mainServiceTitle: string,
                    price: number,
                    quantity: number,
                    shopId: number,
                    shopName: string,
                    subServiceId: number,
                    subServiceTitle: string,
                    usedEnd: Date,
                    usedStart: Date
                }
            ],
            status: string,
            totalPrice: number
        }
    ],
    size: number,
    totalItem: number,
    totalPage: number
}

export type OrderType = {
    createdAt: Date,
    id: number,
    orderBillId: string,
    orderItemInfos: [
        {
            id: number,
            image: string,
            mainServiceId: number,
            mainServiceTitle: string,
            price: number,
            quantity: number,
            shopId: number,
            shopName: string,
            subServiceId: number,
            subServiceTitle: string,
            usedEnd: Date,
            usedStart: Date
        }
    ],
    status: string,
    totalPrice: number
};

export type OrderItemType = {
    id: number,
    image: string,
    mainServiceId: number,
    mainServiceTitle: string,
    price: number,
    quantity: number,
    shopId: number,
    shopName: string,
    subServiceId: number,
    subServiceTitle: string,
    usedEnd: Date,
    usedStart: Date
};

const MyOrder: React.FC = () => {

    const [value, setValue] = React.useState(0);
    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const [orderData, setOrderData] = React.useState([] as OrderType[]);
    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    async function fetchData() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        await request<ReceivedData>('GET', `/pal/order`)
            .then((res) => {
                let receivedData = res.data;
                setOrderData(receivedData.results);
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }
    React.useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='my-order'>
            <div className='wrapper center'>
                <h2 className='screen-title'>Lịch sử mua hàng</h2>
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={value} onChange={handleChange} aria-label='My orders tabs' centered>
                            <Tab label='Tất cả' {...a11yProps(0)} />
                            <Tab label='Chưa thanh toán' {...a11yProps(1)} />
                            <Tab label='Đã thanh toán' {...a11yProps(2)} />
                            <Tab label='Đà hoàn thành' {...a11yProps(3)} />
                        </Tabs>
                    </Box>
                    <TabPanel value={value} index={0}>
                        <div className='all-service'>
                            {orderData.length === 0 ? <h3>Thông tin đơn hàng trống!</h3> : null}
                            {orderData.map((item) => {
                                if (item.status === 'NEW') { item.status = 'Chưa thanh toán'; }
                                else if (item.status === 'PROCESSING') { item.status = 'Đã thanh toán'; }
                                else if (item.status === 'FINISHED') { item.status = 'Đà hoàn thành'; }
                                return item ? (
                                    <div className='service-box' key={item.id}>
                                        <Order
                                            key={item.id}
                                            item={item}
                                        />
                                        <div className='item-footer'>
                                            <div className='service-component'>
                                                <p className='price'>Tổng thanh toán: &nbsp; </p>
                                                <p className='price price-amount'>
                                                    {item.totalPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ
                                                </p>
                                                <p className='price'> &nbsp; ({item.status})</p>
                                                <div className='service-footer service-status' >
                                                    <Stack direction='row'>
                                                        <Button href={'/orderdetail/' + item.id} className='footer-btn' variant='outlined' startIcon={<InfoIcon />}>
                                                            Chi tiết
                                                        </Button>
                                                    </Stack>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : null;
                            })}
                        </div>
                    </TabPanel>

                    <TabPanel value={value} index={1}>
                        <div className='not-pay-service'>
                            {orderData.length === 0 ? <h3>Thông tin đơn hàng trống!</h3> : null}
                            {orderData.map((item) => {
                                if (item.status === 'NEW' || item.status === 'Chưa thanh toán') {
                                    item.status = 'Chưa thanh toán'
                                    return item.id ? (
                                        <div className='service-box' key={item.id}>
                                            <Order
                                                key={item.id}
                                                item={item}
                                            />
                                            <div className='item-footer'>
                                                <div className='service-component'>
                                                    <p className='price'>Tổng thanh toán: &nbsp; </p>
                                                    <p className='price price-amount'>{item.totalPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ</p>
                                                    <p className='price'>({item.status})</p>
                                                    <div className='service-footer service-status' >
                                                        <Stack direction='row'>
                                                            <Button href={'/orderdetail/' + item.id} className='footer-btn' variant='outlined' startIcon={<InfoIcon />}>
                                                                Chi tiết
                                                            </Button>
                                                        </Stack>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null;
                                } else return null;
                            })}
                        </div>
                    </TabPanel>

                    <TabPanel value={value} index={2}>
                        <div className='payed-service'>
                            {orderData.length === 0 ? <h3>Thông tin đơn hàng trống!</h3> : null}
                            {orderData.map((item) => {
                                if (item.status === 'PROCESSING' || item.status === 'Đã thanh toán') {
                                    item.status = 'Đã thanh toán'
                                    return item.id ? (
                                        <div className='service-box' key={item.id}>
                                            <Order
                                                key={item.id}
                                                item={item}
                                            />
                                            <div className='item-footer'>
                                                <div className='service-component'>
                                                    <p className='price'>Tổng thanh toán: &nbsp; </p>
                                                    <p className='price price-amount'>{item.totalPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ</p>
                                                    <p className='price'>({item.status})</p>
                                                    <div className='service-footer service-status' >
                                                        <Stack direction='row'>
                                                            <Button href={'/orderdetail/' + item.id} className='footer-btn' variant='outlined' startIcon={<InfoIcon />}>
                                                                Chi tiết
                                                            </Button>
                                                        </Stack>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null;
                                } else return null;
                            })}
                        </div>
                    </TabPanel>

                    <TabPanel value={value} index={3}>
                        <div className='completed-service'>
                            {orderData.length === 0 ? <h3>Thông tin đơn hàng trống!</h3> : null}
                            {orderData.map((item) => {
                                if (item.status === 'FINISHED' || item.status === 'Đà hoàn thành') {
                                    item.status = 'Đà hoàn thành'
                                    return item.id ? (
                                        <div className='service-box' key={item.id}>
                                            <Order
                                                key={item.id}
                                                item={item}
                                            />
                                            <div className='item-footer'>
                                                <div className='service-component'>
                                                    <p className='price'>Tổng thanh toán: &nbsp; </p>
                                                    <p className='price price-amount'>{item.totalPrice.toLocaleString(undefined, { maximumFractionDigits: 2, })}đ</p>
                                                    <p className='price'>({item.status})</p>
                                                    <div className='service-footer service-status' >
                                                        <Stack direction='row'>
                                                            <Button href={'/orderdetail/' + item.id} className='footer-btn' variant='outlined' startIcon={<InfoIcon />}>
                                                                Chi tiết
                                                            </Button>
                                                        </Stack>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null;
                                } else return null;
                            })}
                        </div>
                    </TabPanel>
                </Box>
            </div>
            <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                    {snackBarMsg}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default MyOrder
