const pageNotFound = require("./../../assets/images/error-image.png");

const NotFound = () => {
  return (
    <div
      style={{
        minHeight: "65vh",
        textAlign: "center",
        transform: "translateY(20%)",
        fontSize: "21px",
      }}
      className="flex-column"
    >
      <div>
        <img src={pageNotFound} alt="Not found" width="150px" height="150px" />
      </div>
      <div style={{ marginTop: "17px" }}>Trang bạn tìm kiếm không tồn tại!</div>
    </div>
  );
};

export default NotFound;
