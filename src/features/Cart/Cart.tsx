import "./cart.scss";
import {
  Backdrop,
  Button,
  Checkbox,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Snackbar,
  SnackbarCloseReason,
} from "@mui/material";
import CartItem from "./Component/CartItem";
import { useEffect, useState } from "react";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import request from "../../shared/services/ApiService";
import { ACCESS_TOKEN } from "../../constants";
import { Box } from "@mui/system";
import ClearIcon from "@mui/icons-material/Clear";

export interface CartData {
  createdAt: Date;
  id: number;
  image: string;
  mainServiceId: number;
  mainServiceShopShopName: string;
  mainServiceTitle: string;
  price: number;
  quantity: number;
  subServiceId: number;
  subServiceTitle: string;
  usedEnd: Date;
  usedStart: Date;
}
export interface CartChange {
  cartItemId: number;
  quantity: number;
  usedStart: Date;
}
export type CartItemType = {
  createdAt: Date;
  usedEnd: Date;
  usedStart: Date | null;
  id: number;
  image: string;
  mainServiceId: number;
  mainServiceShopShopName: string;
  mainServiceTitle: string;
  price: number;
  quantity: number;
  subServiceId: number;
  subServiceTitle: string;
  isChecked: boolean;
  isOutOfDate: boolean;
};

const Cart: React.FC = () => {
  const [isAllChecked, setIsAllChecked] = useState(false);
  const [haveOutOfDate, setHaveOutOfDate] = useState(false);
  const [deleteIds, setDeleteIds] = useState([] as number[]);
  const [rememberId, setRememberId] = useState([] as number[]);
  const [cartItems, setCartItems] = useState([] as CartItemType[]);
  const [isChange, setIsChange] = useState(false);
  const [changeIds, setChangeIds] = useState([] as number[]);
  const [openNotify, setOpenNotify] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    let response = await request<CartData[]>("GET", `/sp/cart`)
      .then((res) => {
        let cartItemData = res.data;
        // console.log(res);
        // console.log(moment());
        setCartItems(() =>
          cartItemData.map((item: CartData) => {
            if (moment(item.usedStart) >= moment().subtract(1, "days")) {
              return { ...item, isChecked: false, isOutOfDate: false };
            } else {
              setHaveOutOfDate(true);
              return { ...item, isChecked: false, isOutOfDate: true };
            }
          })
        );
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/cart", { replace: true });
      });
    setLoading(true);
    // console.log(response);
  }

  useEffect(() => {
    setLoading(false);
    fetchData();
  }, []);

  const [openDialog, setOpenDialog] = useState(false);
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setRememberId([] as number[]);
  };
  const handleConfirmDelete = () => {
    setIsChange(true);
    deleteIds.push(...rememberId);
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (rememberId.includes(item.id)) {
          return ack;
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );

    setOpenDialog(false);
  };
  window.onbeforeunload = function (
    this: WindowEventHandlers,
    ev: BeforeUnloadEvent
  ) {
    console.log(isChange);
    if (isChange) {
      console.log("isChange");
      ev.preventDefault();
      ev.returnValue = "Are you sure you want to close?";
    }
  };
  async function updateCartItem(changeItems: CartChange[]) {
    let response = await request("PUT", `/sp/cart`, null, {
      requests: changeItems,
    })
      .then(() => {
        // console.log("AA");
        setOpenNotify(true);
        setIsChange(false);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/cart", { replace: true });
      });
    setLoading(true);
    fetchData();
  }
  async function deleteCartItem() {
    // console.log(deleteIds);
    let response = await request("DELETE", `/sp/cart`, null, {
      cartIds: deleteIds,
    })
      .then(() => {
        setOpenNotify(true);
        setIsChange(false);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/cart", { replace: true });
      });
    setLoading(true);
    setDeleteIds([] as number[]);
    if (openDialog) setOpenDialog(false);
    // fetchData();
    // console.log(response);
  }
  const handleAllcheck = () => {
    if (isAllChecked === false) {
      setIsAllChecked(true);
      setCartItems((prev) =>
        prev.reduce((ack, item) => {
          if (!item.isOutOfDate) {
            // setIsAllChecked(false);
            return [...ack, { ...item, isChecked: true }];
          } else {
            return [...ack, item];
          }
        }, [] as CartItemType[])
      );
    } else {
      setIsAllChecked(false);
      setCartItems((prev) =>
        prev.reduce((ack, item) => {
          return [...ack, { ...item, isChecked: false }];
        }, [] as CartItemType[])
      );
    }
  };

  const handleCheck = (id: number) => {
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          setIsAllChecked(false);
          return [...ack, { ...item, isChecked: !item.isChecked }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );
  };

  const handlePlus = (id: number) => {
    setIsChange(true);
    if (changeIds.indexOf(id) === -1) changeIds.push(id);
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          return [...ack, { ...item, quantity: item.quantity + 1 }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );

    // cartItems.find((item) => {
    //   if (item.id === id)
    //     if (item.usedStart !== null)
    //       updateCartItem(id, item.usedStart, item.quantity + 1);
    // });
  };

  const handleMinus = (id: number) => {
    setIsChange(true);
    if (changeIds.indexOf(id) === -1) changeIds.push(id);
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (item.quantity === 1) {
            setRememberId([id]);
            setOpenDialog(true);
            // return ack;
            return [...ack, item];
          } else return [...ack, { ...item, quantity: item.quantity - 1 }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );

    // cartItems.find((item) => {
    //   if (item.id === id)
    //     if (item.usedStart !== null)
    //       if (item.quantity > 1)
    //         updateCartItem(id, item.usedStart, item.quantity - 1);
    //       else {
    //         deleteIds.push(id);
    //         setOpenDialog(true);
    //       }
    // });
  };

  const deleteItem = (id: number) => {
    setRememberId([id]);
    setOpenDialog(true);
  };

  const deleteCheckedItem = () => {
    setRememberId(cartItems.filter((item) => item.isChecked).map((a) => a.id));
    setOpenDialog(true);
  };
  const deleteAllOutOfDate = () => {
    setRememberId(
      cartItems.filter((item) => item.isOutOfDate).map((a) => a.id)
    );
    setOpenDialog(true);
  };

  const countItem = (items: CartItemType[]) =>
    items.reduce((ack: number, item) => (item.isChecked ? ack + 1 : ack), 0);

  const calculateTotal = (items: CartItemType[]) =>
    items.reduce(
      (ack: number, item) =>
        item.isChecked ? ack + item.quantity * item.price : ack,
      0
    );

  const handleDateChange = (id: number, newDate: Date | null) => {
    setIsChange(true);
    if (changeIds.indexOf(id) === -1) changeIds.push(id);
    setCartItems((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          return [...ack, { ...item, usedStart: newDate }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );

    // cartItems.find((item) => {
    //   if (item.id === id)
    //     if (newDate !== null)
    //       updateCartItem(
    //         id,
    //         moment(newDate).add(1, "days").toDate(),
    //         item.quantity
    //       );
    // });
  };
  const saveChange = () => {
    if (changeIds.length > 0) {
      setLoading(false);
      let newArr = cartItems.filter((item) => {
        return changeIds
          .filter((el) => !rememberId.includes(el))
          .includes(item.id);
      });
      let changeItems = newArr.map((item) => ({
        cartItemId: item.id,
        quantity: item.quantity,
        usedStart: item.usedStart,
      }));
      updateCartItem(changeItems as CartChange[]);
      // console.log("changeItems", changeItems);
    }
    if (deleteIds.length > 0) {
      setLoading(false);
      // console.log("rememberId", rememberId);
      deleteCartItem();
    }
    // setIsChange(false);
  };

  let navigate = useNavigate();
  // const purchaseContext = useContext(PurchaseContext);
  const handleOrder = () => {
    if (isChange) alert("Hãy lưu thay đổi trước khi rời trang.");
    else {
      const newArr = cartItems.filter((item) => {
        return item.isChecked && !item.isOutOfDate;
      });
      let purchaseItems = newArr.map(({ isChecked, ...rest }) => {
        return rest;
      });
      if (purchaseItems.length !== 0) {
        localStorage.setItem("purchaseItems", JSON.stringify(purchaseItems));
        localStorage.setItem("countItem", countItem(cartItems).toString());
        localStorage.setItem(
          "calculateTotal",
          calculateTotal(cartItems).toString()
        );
        let path = `/purchase`;
        navigate(path);
        // }
        // window.location.href = "/purchase";
      } else alert("Hãy chọn sản phẩm bạn muốn đặt!");
    }
  };
  return (
    <div className="cart">
      <Container className="cart-container">
        {/* <div className="title"> */}
        <h2>Giỏ hàng</h2>
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <div className="header-table">
          <Checkbox checked={isAllChecked} onClick={handleAllcheck} />
          <p id="p-product">Sản phẩm</p>
          <p id="p-start-date">Ngày bắt đầu sử dụng</p>
          <p>Đơn giá</p>
          <p>Số lượng</p>
          <p>Số tiền</p>
          <p>Thao tác</p>
        </div>
        <br></br>
        <div className="items">
          {/* <div className="shop">
            <Checkbox />
            <p>Shop_name</p>
          </div> */}
          {cartItems.length === 0 ? <h3>Giỏ hàng đang trống!</h3> : null}
          {cartItems.map((item) => {
            return !item.isOutOfDate ? (
              <CartItem
                key={item.id}
                item={item}
                plus={handlePlus}
                minus={handleMinus}
                deleteItem={deleteItem}
                checkItem={handleCheck}
                handleDateChange={handleDateChange}
              />
            ) : null;
          })}
        </div>
        <br></br>
        {haveOutOfDate ? (
          <div className="out-of-date">
            <div className="top">
              <h3>Dịch vụ đã hết hạn</h3>
              <Button onClick={deleteAllOutOfDate}>
                Xóa các dịch vụ đã hết hạn
              </Button>
            </div>
            <div className="items">
              {cartItems.map((item) => {
                return item.isOutOfDate ? (
                  <CartItem
                    key={item.id}
                    item={item}
                    plus={handlePlus}
                    minus={handleMinus}
                    deleteItem={deleteItem}
                    checkItem={handleCheck}
                    handleDateChange={handleDateChange}
                  />
                ) : null;
              })}
            </div>
            <br></br>
          </div>
        ) : null}
        <div className="cart-bottom">
          {/* <div className="voucher">
            <input type={"text"} placeholder="Voucher" />
            <Button>Chọn hoặc nhập mã</Button>
          </div> */}
          {/* <hr></hr> */}
          <div className="total">
            <div className="all-delete">
              <Checkbox checked={isAllChecked} onClick={handleAllcheck} />
              <Button onClick={handleAllcheck}>Chọn tất cả</Button>
              <Button onClick={deleteCheckedItem}>Xóa</Button>
              {isChange ? (
                // <div>
                <Button onClick={saveChange}>Lưu Thay đổi</Button>
              ) : // <p color="red">Hãy lưu thay đổi trước khi rời trang!</p>
              // </div>
              null}
            </div>
            <div className="checkout">
              <p>
                Tổng thanh toán (
                {countItem(cartItems).toLocaleString(undefined, {
                  maximumFractionDigits: 0,
                })}{" "}
                sản phẩm):{" "}
                {calculateTotal(cartItems).toLocaleString(undefined, {
                  maximumFractionDigits: 0,
                })}
                đ
              </p>

              <Button
                size="small"
                disableElevation
                variant="contained"
                onClick={handleOrder}
              >
                Mua hàng
              </Button>
            </div>
          </div>
        </div>
      </Container>
      <Dialog
        open={openDialog}
        fullWidth
        onClose={handleCloseDialog}
        className="dialog-container"
      >
        {/* <div className="btn-close-dialog">
            <IconButton onClick={handleCloseDialog}>
              <ClearIcon />
            </IconButton>
          </div> */}
        <DialogTitle className="dialog-title">
          {/* Thêm quản lý */}
          <Box display="flex" alignItems="center">
            <Box flexGrow={1}>Xóa sản phẩm</Box>
            <Box>
              <IconButton onClick={handleCloseDialog}>
                <ClearIcon />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent>
          Bạn có chắc muốn xóa {rememberId.length > 1 ? " những " : " "} sản
          phẩm này?
        </DialogContent>
        <DialogActions>
          <Button onClick={handleConfirmDelete}>Xác nhận</Button>
          <Button onClick={handleCloseDialog}>Hủy</Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={openNotify}
        autoHideDuration={1000}
        onClose={handleCloseNotify}
        message="Lưu thành công!"
        // action={action}
      />
    </div>
  );
};

export default Cart;
