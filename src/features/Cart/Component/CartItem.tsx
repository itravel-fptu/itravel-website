import "./cart-item.scss";
import { Button, Checkbox, TextField } from "@mui/material";
import { CartItemType } from "../Cart";
import { DatePicker, DesktopDatePicker, LocalizationProvider } from "@mui/lab";
import DateAdapter from "@mui/lab/AdapterMoment";
import moment from "moment";
import { useNavigate } from "react-router-dom";

type Props = {
  item: CartItemType;
  plus: (id: number) => void;
  minus: (id: number) => void;
  deleteItem: (id: number) => void;
  checkItem: (id: number) => void;
  handleDateChange: (id: number, newDate: Date | null) => void;
};
const CartItem: React.FC<Props> = ({
  item,
  plus,
  minus,
  deleteItem,
  checkItem,
  handleDateChange,
}) => {
  let navigate = useNavigate();
  return (
    <div className="cart-item">
      {/* <div > */}

      <Checkbox
        checked={item.isChecked}
        onClick={() => checkItem(item.id)}
        disabled={item.isOutOfDate ? true : false}
      />
      <div className="img-title">
        {/* <div className="img"> */}
        <div
          className="img-container"
          onClick={() => navigate("/service/" + item.subServiceId)}
        >
          <img src={item.image} />
        </div>
        {/* </div> */}
        <div className="title">
          <b>{item.mainServiceTitle}</b>
          <p>{item.subServiceTitle}</p>
          <p>
            <b>Shop: </b>
            {item.mainServiceShopShopName}
          </p>
        </div>
      </div>
      <div className="start-date">
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DatePicker
            inputFormat="DD/MM/yyyy"
            value={moment(item.usedStart).toDate()}
            onChange={(newDate: Date | null) => {
              handleDateChange(item.id, newDate);
              console.log("done");
            }}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </div>
      <div className="price">
        <p>
          {item.price.toLocaleString(undefined, {
            maximumFractionDigits: 2,
          })}
          đ
        </p>
      </div>
      <div className="amount">
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() => minus(item.id)}
        >
          -
        </Button>
        <p>
          <span className="amount-span">{item.quantity}</span>
        </p>
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() => plus(item.id)}
        >
          +
        </Button>
      </div>
      <div className="total-price">
        <p>
          {(item.price * item.quantity).toLocaleString(undefined, {
            maximumFractionDigits: 2,
          })}
          đ
        </p>
      </div>
      <div className="act">
        <Button onClick={() => deleteItem(item.id)}>Xóa</Button>
      </div>
      {/* </div> */}
    </div>
  );
};

export default CartItem;
