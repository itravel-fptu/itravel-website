import { Navigate, useLocation, useSearchParams } from "react-router-dom";
import { ACCESS_TOKEN } from "../../constants";
import useAuth from "../../hooks/useAuth";

const OAuth2RedirectHandler = () => {
  let [searchParams, setSearchParams] = useSearchParams();
  let location = useLocation();
  let token = searchParams.get("accessToken");
  let auth = useAuth();

  let from = sessionStorage.getItem("FROM") || "/";
  sessionStorage.removeItem("FROM");
  console.log("FROM TO LOGIN: " + from);

  if (token) {
    localStorage.setItem(ACCESS_TOKEN, token);
    auth.login();
    return <Navigate to={from} replace />;
  }

  console.log(location);
  let error = "LOGIN FAIL";
  return <Navigate to={from} state={{ error: error }} replace />;
};

export default OAuth2RedirectHandler;
