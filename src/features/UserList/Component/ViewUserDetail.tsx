import { Button, Container, InputLabel, TextField } from "@mui/material";
import moment from "moment";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ACCESS_TOKEN } from "../../../constants";
import request from "../../../shared/services/ApiService";
import { account } from "../UserList";
import "./view-user-detail.scss";

const ViewUserDetail: React.FC = () => {
  const { userId } = useParams();
  const [userInfo, setUserInfo] = useState<account>();
  let navigate = useNavigate();
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      return Promise.reject("No access token set.");
    }
    let response = await request<account>("GET", `/system/account/` + userId)
      .then((res) => {
        // let data = res.data;
        setUserInfo(res.data);
        console.log(res.data);
        // setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/userlist", { replace: true });
      });
  }
  useEffect(() => {
    fetchData();
    // console.log("rows", rows);
  }, []);
  const handleBack = () => {
    navigate(-1);
  };
  return (
    // <div>
    <Container className="view-user-detail">
      <h2>Thông tin người dùng</h2>
      <div className="wrap">
        <div className="container">
          <div className="content">
            {userInfo ? (
              <div>
                <div className="div-top">
                  <div className="img-id-name">
                    <img src="https://pbs.twimg.com/profile_images/758084549821730820/_HYHtD8F_400x400.jpg" />
                    <div className="div-right">
                      <div className="label-textfield">
                        <InputLabel className="label">ID</InputLabel>
                        <p>{userInfo.id}</p>
                      </div>
                      <div className="label-textfield">
                        <InputLabel className="label">Họ tên</InputLabel>
                        <p>{userInfo.fullName}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="div-body">
                  <div className="div-left">
                    <div className="label-textfield">
                      <InputLabel className="label">Email</InputLabel>
                      <p>{userInfo.email}</p>
                    </div>
                    <div className="label-textfield">
                      <InputLabel className="label">Số điện thoại</InputLabel>
                      <p>{userInfo.phoneNumber}</p>
                    </div>
                    <div className="label-textfield">
                      <InputLabel className="label">Ngày sinh</InputLabel>
                      <p>{moment(userInfo.birthday).format("DD-MM-YYYY")}</p>
                    </div>
                  </div>
                  <div className="div-right">
                    <div className="label-textfield">
                      <InputLabel className="label">Giới tính</InputLabel>
                      <p>{userInfo.gender}</p>
                    </div>
                    <div className="label-textfield">
                      <InputLabel className="label">Ngày đăng ký</InputLabel>
                      <p>
                        {moment(userInfo.createdAt).format("hh:mm DD-MM-YYYY")}
                      </p>
                    </div>
                    <div className="label-textfield">
                      <InputLabel className="label">Trạng thái</InputLabel>
                      <p>{userInfo.status}</p>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div>Không tìm thấy thông tin người dùng.</div>
            )}
            <Button variant="contained" onClick={handleBack}>
              Quay lại
            </Button>
          </div>
        </div>
      </div>
    </Container>
    // </div>
  );
};

export default ViewUserDetail;
