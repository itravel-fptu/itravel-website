import "./user-list.scss";
import variables from "../../assets/scss/_variables.scss";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  createTheme,
  IconButton,
  InputAdornment,
  InputBase,
  Pagination,
  Snackbar,
  SnackbarCloseReason,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  ThemeProvider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import InfoIcon from "@mui/icons-material/Info";
import { KeyboardEventHandler, useEffect, useState } from "react";
import { FilterAlt } from "@mui/icons-material";
import ClearIcon from "@mui/icons-material/Clear";
import SideBarMod from "../../shared/Component/SideBarMod";
import { StatusType } from "../../shared/types/StatusType";
import { useNavigate } from "react-router-dom";
import styled from "@emotion/styled";
import { ACCESS_TOKEN } from "../../constants";
import request from "../../shared/services/ApiService";
import moment from "moment";

export interface account {
  birthday: Date;
  createdAt: Date;
  email: string;
  fullName: string;
  gender: string;
  id: number;
  imageLink: string;
  modifiedAt: Date;
  phoneNumber: string;
  roles: string;
  status: string;
}
export interface accountResult {
  page: number;
  results: [account[]];
  totalPages: number;
  totalResults: number;
}
const UserList: React.FC = () => {
  const [rows, setRows] = useState([] as account[]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [searchText, setSearchText] = useState("");
  const [loading, setLoading] = useState(false);
  const [openNotify, setOpenNotify] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };
  // console.log(rows.length);
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    let response = await request<accountResult>("GET", `/mod/accounts`, {
      page: pageNumber,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        // JSON.parse(JSON.stringify(data.results));
        setRows(data.results[0].map((item) => item));
        console.log(data);
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/userlist", { replace: true });
      });
    setLoading(true);
    // console.log("rows", rows);
    // console.log("rowslength", rows.length);
  }
  useEffect(() => {
    fetchData();
    return () => {
      setRows([]);
    };
  }, [pageNumber]);

  async function lockAcc(id: number) {
    let response = await request("PUT", `/mod/lock/` + id)
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/userlist", { replace: true });
      });
    fetchData();
  }
  async function unlockAcc(id: number) {
    let response = await request("PUT", `/mod/unlock/` + id)
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/userlist", { replace: true });
      });
    fetchData();
  }
  async function search() {
    let response = await request<accountResult>("GET", `/mod/search/accounts`, {
      page: pageNumber,
      query: searchText,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        // JSON.parse(JSON.stringify(data.results));
        setRows(data.results[0].map((item) => item));
        // console.log(data);
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/userlist", { replace: true });
      });
    // fetchData();
  }
  const [open, setOpen] = useState(false);
  const [statusList, setStatusList] = useState([
    { id: 1, title: "Active", isChoose: false },
    { id: 2, title: "Locked", isChoose: false },
  ] as StatusType[]);
  const [filterValue, setFilterValue] = useState([] as string[]);
  const handleClickOpen = () => {
    setOpen(open ? false : true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    let text = e.target.value;
    setSearchText(text);
    if (text === "") {
      fetchData();
    }
  };
  const handleStatus = (id: number) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleRemoveOption = (title: string) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.title === title) {
          const arr = filterValue.filter((val) => val !== item.title);
          setFilterValue(arr);
          return [...ack, { ...item, isChoose: false }];
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleApplyFilter = () => {
    setOpen(false);
  };
  let navigate = useNavigate();
  const handleViewDetail = (id: number) => {
    let path = "/manager/user/" + id;
    navigate(path);
  };
  const handleRemoveAllFilter = () => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        // const arr = filterValue.filter((val) => val !== item.title);
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setFilterValue([]);
  };
  const handleChangePage = (page: number) => {
    setPageNumber(page);
  };
  const handleEnter = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (e.key === "Enter") search();
  };
  const theme = createTheme({
    palette: {
      primary: { main: variables.colorMain },
      info: { main: "#fff" },
    },
    components: {
      MuiTableHead: {
        styleOverrides: {
          root: {
            backgroundColor: variables.colorMain,
            position: "sticky",
            top: 0,
          },
        },
      },
    },
  });
  const StyledTableRow = styled(TableRow)(() => ({
    "&:nth-of-type(even)": {
      backgroundColor: variables.colorRowOdd,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));
  return (
    <ThemeProvider theme={theme}>
      <Container className="user-container">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h2>Danh sách người dùng</h2>
        <div className="search-filter">
          <div className="search-bar">
            <IconButton onClick={search}>
              <SearchIcon className="search-icon" />
            </IconButton>
            <InputBase
              type="search"
              style={{ width: 400 }}
              placeholder="Tìm kiếm theo tên hoặc email"
              value={searchText}
              onChange={handleSearchText}
              onKeyDown={handleEnter}
              // InputProps={{ disableUnderline: true }}
            />
          </div>
          <Button
            className="btn-filter"
            variant="contained"
            onClick={handleClickOpen}
          >
            <FilterAlt />
            Bộ lọc
          </Button>
        </div>
        <div className="filter" hidden={open ? false : true}>
          <IconButton onClick={handleClose} className="btn-close">
            <ClearIcon fontSize="small" />
          </IconButton>
          <div className="filter-value-container">
            {filterValue.length != 0 ? (
              <p className="selected-title">Đã chọn:</p>
            ) : null}
            {filterValue.length != 0
              ? filterValue.map((statusValue) => (
                  <div className="filter-value" key={statusValue}>
                    <p>{statusValue}</p>{" "}
                    <IconButton
                      onClick={() => handleRemoveOption(statusValue)}
                      className="btn-close"
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  </div>
                ))
              : null}
            {filterValue.length != 0 ? (
              <Button onClick={handleRemoveAllFilter}>Xóa tất cả</Button>
            ) : null}
          </div>
          <p className="status-title">Tình trạng</p>
          {statusList.map((status) => (
            <Button
              key={status.id}
              className={
                status.isChoose
                  ? "btn-filter-option isChoose"
                  : "btn-filter-option"
              }
              variant="contained"
              color="info"
              onClick={() => handleStatus(status.id)}
            >
              {status.title}
            </Button>
          ))}
          <Button
            className="btn-apply-filter"
            variant="contained"
            onClick={handleApplyFilter}
          >
            Lọc
          </Button>
        </div>

        <div className="table-container">
          <TableContainer className="container-table">
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell width={"50"}>ID</TableCell>
                  {/* <TableCell width={"120"} align="center">
                    Hình ảnh
                  </TableCell> */}
                  <TableCell width={"200"}>Họ tên</TableCell>
                  <TableCell width={"200"}>Email</TableCell>
                  <TableCell width={"170"}>Số điện thoại</TableCell>
                  <TableCell width={"190"}>Ngày đăng ký</TableCell>
                  <TableCell width={"150"} align="center">
                    Tình trạng
                  </TableCell>
                  <TableCell width={"150"} align="center">
                    Tùy chọn
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {rows.length == 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <h3>Không có người dùng nào</h3>
                    </TableCell>
                  </TableRow>
                ) : (
                  rows.map((row) => (
                    <StyledTableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      {/* <TableCell className="cell-img" align="center">
                        <img src={row.image} />
                      </TableCell> */}
                      <TableCell>
                        <div className="long-text">{row.fullName}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.email}</div>
                      </TableCell>
                      <TableCell>{row.phoneNumber}</TableCell>
                      <TableCell>
                        {moment(row.createdAt).format("hh:mm DD-MM-YYYY")}
                      </TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center" style={{ display: "flex" }}>
                        <Button
                          onClick={() => handleViewDetail(row.id)}
                          title="Chi tiết"
                        >
                          <InfoIcon />
                        </Button>
                        {row.status == "LOCKED" ? (
                          <Button
                            onClick={() => unlockAcc(row.id)}
                            title="Mở khóa"
                          >
                            <LockOpenIcon />
                          </Button>
                        ) : (
                          <Button onClick={() => lockAcc(row.id)} title="Khóa">
                            <LockIcon />
                          </Button>
                        )}
                      </TableCell>
                    </StyledTableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Pagination
              className="table-pagination"
              count={totalPage}
              page={pageNumber}
              onChange={(event: React.ChangeEvent<unknown>, page: number) =>
                handleChangePage(page)
              }
              variant="outlined"
              color="primary"
              shape="rounded"
            />
          </TableContainer>
          <Snackbar
            open={openNotify}
            autoHideDuration={1000}
            onClose={handleCloseNotify}
            message="Cập nhật thành công!"
            // action={action}
          />
          {/* </Box> */}
        </div>
      </Container>
    </ThemeProvider>
  );
};

export default UserList;
