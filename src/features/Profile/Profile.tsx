import AddBusinessIcon from '@mui/icons-material/AddBusiness';
import HistoryIcon from '@mui/icons-material/History';
import LogoutIcon from '@mui/icons-material/Logout';
import SaveIcon from '@mui/icons-material/Save';
import { DatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { Alert, Avatar, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormControlLabel, IconButton, InputLabel, Radio, RadioGroup, Snackbar, Stack, TextField } from '@mui/material';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import { ACCESS_TOKEN } from '../../constants';
import useAuth from '../../hooks/useAuth';
import request from '../../shared/services/ApiService';
import './Profile.scss';

export interface UserProfileData {
    birthday: Date;
    createdAt: Date;
    email: string;
    fullName: string;
    gender: string;
    id: number;
    imageLink: string;
    modifiedAt: Date;
    phoneNumber: string;
    roles: [
        {
            id: number,
            roleName: string
        }]
    status: string
}

export interface RolesData {
    id: number,
    roleName: string,
}

const Profile: React.FC = () => {
    let auth = useAuth();
    const [openShopRegis, setOpenShopRegis] = React.useState(false);
    const handleOpenShopRegis = () => {
        if (role > 1) {
            setSnackBarMsg('Bạn đã đăng ký bán hàng rồi, hoặc tài khoản của bạn không được hỗ trợ tính năng này!');
            setOpenErrorSnackBar(true);
        } else{
            setOpenShopRegis(true);
        }
    }
    const handleCloseShopRegis = () => setOpenShopRegis(false);

    const [snackBarMsg, setSnackBarMsg] = React.useState('Nội dung thông báo');
    const [openSuccessSnackBar, setOpenSuccessSnackBar] = React.useState(false);
    const handleCloseSuccessSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccessSnackBar(false);
    };
    const [openErrorSnackBar, setOpenErrorSnackBar] = React.useState(false);
    const handleCloseErrorSnackBar = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenErrorSnackBar(false);
    };

    const handleUpdate = () => {
        updateInfo();
        handleCloseShopRegis();
    };
    const handleRegist = () => {
        shopRegist();
        handleCloseShopRegis();
    };

    const [name, setName] = React.useState('');
    const nameHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };
    const [date, setDate] = React.useState<Date | null>(new Date());
    const DateHandleChange = (newValue: Date | null) => {
        setDate(newValue);
    };
    const [email, setEmail] = React.useState('');
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const phoneNumberHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPhoneNumber(event.target.value);
    };
    const [gender, setGender] = React.useState('');
    const genderHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setGender((event.target as HTMLInputElement).value);
    };
    const [createdAt, setCreatedAt] = React.useState('');
    const [imageLink, setImageLink] = React.useState('');
    let navigate = useNavigate();
    const [shopName, setShopName] = React.useState('');
    const shopNameHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setShopName(event.target.value);
    };
    const [shopAddress, setShopAddress] = React.useState('');
    const shopAddressHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setShopAddress(event.target.value);
    };
    const [shopDescription, setShopDescription] = React.useState('');
    const shopDescriptionHandleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setShopDescription(event.target.value);
    };
    const [role, setRole] = React.useState(0);

    async function fetchData() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        await request<UserProfileData>('GET', `/account`)
            .then((res) => {
                let profileData = res.data;
                setName(profileData.fullName);
                setDate(profileData.birthday);
                setEmail(profileData.email);
                setPhoneNumber(profileData.phoneNumber);
                setGender(profileData.gender);
                setCreatedAt(profileData.createdAt.toString());
                setImageLink(profileData.imageLink);
            })
            .catch((error) => {
                console.log('Error: ', error);
                navigate('/profile', { replace: true });
            });
        await request<RolesData[]>('GET', `/account/roles`)
            .then((respond) => {
                let rolesData = respond.data;
                setRole(rolesData.length);
            })
            .catch((error) => {
                console.log('Error: ', error);
            });
    }
    React.useEffect(() => {
        fetchData();
    }, []);

    async function updateInfo() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (name === '' || gender === '' || phoneNumber === '') {
            setSnackBarMsg('Vui lòng điền tất cả thông tin để cập nhật!');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('PUT', `/account`, null, {
                birthday: date,
                fullName: name,
                gender: gender,
                phoneNumber: phoneNumber
            })
                .then((res) => {
                    setSnackBarMsg('Cập nhật thông tin thành công.');
                    setOpenSuccessSnackBar(true);
                })
                .catch((error) => {
                    console.log('Error: ', error);
                    setSnackBarMsg('Cập nhật thông tin thất bại, có lỗi xảy ra.');
                    setOpenErrorSnackBar(true);
                });
        }
    }

    async function shopRegist() {
        let token: string | null = localStorage.getItem(ACCESS_TOKEN);
        if (!token) {
            setSnackBarMsg('Vui lòng đăng nhập để sử dụng tính năng này!');
            setOpenErrorSnackBar(true);
            return Promise.reject('No access token set.');
        }
        if (shopAddress === '' || shopName === '' || shopDescription === '') {
            setSnackBarMsg('Vui lòng điền tất cả thông tin để tạo cửa hàng!');
            setOpenErrorSnackBar(true);
        } else {
            await request<string>('POST', `/sp/shop`, null, {
                address: shopAddress,
                description: shopDescription,
                shopName: shopName,
            })
                .then((res) => {
                    setSnackBarMsg('Tạo shop thành công.');
                    setOpenSuccessSnackBar(true);
                })
                .catch((error) => {
                    console.log('Error: ', error);
                    setSnackBarMsg('Tạo shop thất bại, có lỗi xảy ra.');
                    setOpenErrorSnackBar(true);
                });
        }
    }

    const handleLogout = () => {
        auth.logout(() => {
            navigate('/', { replace: true });
        });
    };

    return (
        <div className='profile'>
            <div className='profile-wrapper'>
                <div className='row'>
                    <div className='column column-left'>
                        <div className='profile-left'>
                            <div>
                                <IconButton className='avatar-btn' color='primary' aria-label='upload picture' component='span' disabled>
                                    <Avatar
                                        src={(imageLink)}
                                        sx={{ width: '120px', height: '120px' }}
                                    />
                                </IconButton>
                            </div>
                            <hr className='solid' />
                            <div className='row-content'>
                                <a href='/myorder'>
                                    <div className='col-content col-content-left color-grey'><HistoryIcon fontSize='large' /></div>
                                    <div className='col-content col-content-right color-grey' >Lịch sử mua hàng</div>
                                </a>
                            </div>
                            <div className='row-content'>
                                <div className='col-content col-content-left color-grey' onClick={handleOpenShopRegis}><AddBusinessIcon fontSize='large' /></div>
                                <div className='col-content col-content-right color-grey' onClick={handleOpenShopRegis}>Đăng kí bán hàng</div>
                            </div>
                            <hr className='solid' />
                            <div className='row-content'>
                                <a href='/'>
                                    <div className='col-content col-content-left color-grey' onClick={handleLogout}><LogoutIcon fontSize='large' /></div>
                                    <div className='col-content col-content-right color-grey' onClick={handleLogout}>Đăng xuất</div>
                                </a>
                            </div>

                            <Dialog open={openShopRegis} onClose={handleCloseShopRegis}>
                                <DialogTitle>Tạo cửa hàng</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        Vui lòng nhập chính xác những thông tin cần thiết để tạo cửa hàng của riêng bạn.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        margin='dense'
                                        id=''
                                        value={shopName}
                                        onChange={shopNameHandleChange}
                                        label='Tên cửa hàng'
                                        fullWidth
                                        variant='standard'
                                    />
                                    <TextField
                                        margin='dense'
                                        id=''
                                        value={shopAddress}
                                        onChange={shopAddressHandleChange}
                                        label='Địa chỉ cụ thể'
                                        fullWidth
                                        variant='standard'
                                    />
                                    <InputLabel className='label'>Mô tả cửa hàng</InputLabel>
                                    <TextField
                                        multiline
                                        fullWidth={true}
                                        rows={5}
                                        value={shopDescription}
                                        onChange={shopDescriptionHandleChange}
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleCloseShopRegis}>Hủy</Button>
                                    <Button onClick={handleRegist}>Xác Nhận</Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </div>

                    <div className='column column-right margin-bottom'>
                        <div>
                            <h2 className='screen-title'>Thông tin cá nhân</h2>
                            <p className='screen-sub-title color-grey'>Cập nhật và chỉnh sửa thông tin cá nhân của bạn thường xuyên để nâng cao trải nghiệm</p>
                            <hr className='solid' />
                            <div className='col-content col-input-left'>
                                <p>Họ và tên đầy đủ</p>
                            </div>
                            <div className='col-content col-input-right'>
                                <p>Ngày tháng năm sinh</p>
                            </div>
                            <div className='row'>
                                <div className='col-content col-input-left'>
                                    <TextField className='input-left' id='' value={name} onChange={nameHandleChange} label='Họ tên' variant='outlined' />
                                </div>
                                <div className='col-content col-input-right'>
                                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                                        <Stack spacing={3} className='input-left'>
                                            <DatePicker
                                                label='Ngày sinh'
                                                inputFormat='dd/MM/yyyy'
                                                value={date}
                                                onChange={DateHandleChange}
                                                renderInput={(params) => <TextField {...params} />}
                                            />
                                        </Stack>
                                    </LocalizationProvider>
                                </div>
                            </div>
                            <div className='col-content col-input-left'>
                                <p>Địa chỉ Email</p>
                            </div>
                            <div className='col-content col-input-right'>
                                <p>Số điện thoại liên lạc</p>
                            </div>
                            <div className='row'>
                                <div className='col-content col-input-left'>
                                    <TextField className='input-left' id='' value={email} variant='outlined' disabled />
                                </div>
                                <div className='col-content col-input-right'>
                                    <TextField className='input-left' id='' value={phoneNumber} onChange={phoneNumberHandleChange} label='Số điện thoại' variant='outlined' />
                                </div>
                            </div>
                            <div className='col-content col-input-left '>
                                <p>Ngày mở tài khoản</p>
                            </div>
                            <div className='row'>
                                <TextField className='col-input' id='' value={createdAt.slice(8, 10) + createdAt.slice(4, 8) + createdAt.slice(0, 4)} variant='outlined' disabled />
                            </div>
                            <div className='col-content col-input'>
                                <p>Giới tính</p>
                            </div>
                            <div className='row'>
                                <div className='col-content col-input-left '>
                                    <FormControl>
                                        <RadioGroup
                                            row
                                            aria-labelledby='demo-row-radio-buttons-group-label'
                                            name='row-radio-buttons-group'
                                            value={gender}
                                            onChange={genderHandleChange}
                                        >
                                            <FormControlLabel value='MALE' control={<Radio />} label='Nam' />
                                            <FormControlLabel value='FEMALE' control={<Radio />} label='Nữ' />
                                            <FormControlLabel value='OTHER' control={<Radio />} label='Khác' />
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                                <div className='col-content col-input-right'>
                                    <Button className='btn-save'
                                        size='large'
                                        color='secondary'
                                        onClick={handleUpdate}
                                        startIcon={<SaveIcon />}
                                        variant='contained'
                                    >
                                        Cập nhật
                                    </Button>
                                    <Snackbar open={openSuccessSnackBar} autoHideDuration={2000} onClose={handleCloseSuccessSnackBar}>
                                        <Alert onClose={handleCloseSuccessSnackBar} severity='success' sx={{ width: '100%' }}>
                                            {snackBarMsg}
                                        </Alert>
                                    </Snackbar>
                                    <Snackbar open={openErrorSnackBar} autoHideDuration={2000} onClose={handleCloseErrorSnackBar}>
                                        <Alert onClose={handleCloseErrorSnackBar} severity='error' sx={{ width: '100%' }}>
                                            {snackBarMsg}
                                        </Alert>
                                    </Snackbar>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Profile
