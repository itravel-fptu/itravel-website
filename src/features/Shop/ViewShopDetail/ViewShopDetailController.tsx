import request from "../../../shared/services/ApiService";
import { IShopDetail } from "../../../shared/types/ShopType";

export const getShopDetails = async (shopId: number): Promise<IShopDetail> => {
  const url = `/sp/shop/${shopId}`;

  const results = await request<IShopDetail>("GET", url).then(
    (res) => res.data
  );

  return results;
};

export type ShopServiceListResultType = {
  page: number;
  serviceInfos: [
    {
      categoryId: number;
      categoryName: string;
      cityId: number;
      cityName: string;
      createdAt: string;
      id: number;
      images: [string];
      price: number;
      rateAverage: number;
      rateCount: number;
      shopId: number;
      shopShopName: string;
      status: string;
      title: string;
    }
  ];
  totalPage: 0;
  totalItem: 0;
};

export const getShopServiceList = async (
  shopId: number
): Promise<ShopServiceListResultType> => {
  const url = `/sp/service/shop/${shopId}`;

  const params = {
    page: 1,
    size: 10,
  };

  const results = await request<ShopServiceListResultType>(
    "GET",
    url,
    params
  ).then((res) => res.data);

  return results;
};
