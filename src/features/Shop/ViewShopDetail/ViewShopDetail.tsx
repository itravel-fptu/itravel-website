import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useQuery } from "react-query";
import moment from "moment";

// Components
import { ServiceCardType } from "../../../shared/types/ServiceType";
import ServiceCard from "../../../shared/Component/ServiceCard";
import { Container, Grid } from "@mui/material";
// Styles
import "./view-shop-detail.scss";
// Data & Controller
import {
  getShopDetails,
  getShopServiceList,
  ShopServiceListResultType,
} from "./ViewShopDetailController";

export interface IShopDetail {
  name: string;
  avatarPath: string;
  description: string;
  serviceCount: number;
  createdAt: string;
}
const ViewShopDetail: React.FunctionComponent = () => {
  const { shopId } = useParams();
  const { data: shopDetails } = useQuery("shopDetails", () =>
    getShopDetails(Number(shopId))
  );

  const { data: shopServiceList, refetch } =
    useQuery<ShopServiceListResultType>(
      "shopServiceList",
      () => getShopServiceList(Number(shopId)),
      {
        refetchOnWindowFocus: false,
        enabled: false,
      }
    );

  useEffect(() => {
    refetch();
  }, [shopDetails]);

  return (
    <>
      <Container className="shop-page">
        {shopDetails && (
          <div>
            <div className="shop-page__info flex-row">
              <div className="shop-info__section-left">
                <img
                  className="shop-info__avatar"
                  src={shopDetails.avatar}
                  alt=""
                />
                <h3 className="shop-info__name">{shopDetails.shopName}</h3>
              </div>
              <div className="shop-info__section-right flex-column">
                <p className="shop-info__description">
                  {shopDetails.description}
                </p>
                <div className="shop-info__item">
                  Dịch vụ cung cấp:{" "}
                  <span className="shop-info__value">
                    {shopDetails.serviceCount}
                  </span>
                </div>
                <div className="shop-info__item">
                  Ngày tham gia:{" "}
                  <span className="shop-info__value">
                    {moment(shopDetails.createdAt).format("DD-MM-YYYY")}
                  </span>
                </div>
              </div>
            </div>
            <div className="shop-page__service-list">
              <div className="service-list__title">Tất cả dịch vụ</div>
              <section className="popular-services">
                <Grid container spacing={6}>
                  {shopServiceList?.serviceInfos.map(
                    (service: ServiceCardType, index: number) => (
                      <Grid item key={service.id} sm={6} md={4} lg={3}>
                        <Link to={`/service/${service.id}`}>
                          <ServiceCard service={service} />
                        </Link>
                      </Grid>
                    )
                  )}
                </Grid>
              </section>
              {shopDetails.serviceCount === 0 && (
                <div className="service-list__empty">
                  Shop chưa cung cấp dịch vụ nào!
                </div>
              )}
            </div>
          </div>
        )}
      </Container>
    </>
  );
};

export default ViewShopDetail;
