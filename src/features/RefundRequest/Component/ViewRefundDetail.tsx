import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  Snackbar,
  SnackbarCloseReason,
  TextField,
} from "@mui/material";
import moment from "moment";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ACCESS_TOKEN } from "../../../constants";
import request from "../../../shared/services/ApiService";
import { Refund, RefundResult } from "../RefundRequestList";
import "./view-refund-detail.scss";

export interface RefundRequest {
  createdAt: Date;
  id: number;
  orderBillId: string;
  orderItemCreatedAt: Date;
  orderItemId: number;
  refuseReason: string;
  reportType: string;
  requestReason: string;
  shopName: string;
  status: string;
  usedEnd: Date;
  usedStart: Date;
  userName: string;
}
const ViewRefundDetail: React.FC = () => {
  const { id } = useParams();
  const [refundRequest, setRefundRequest] = useState<RefundRequest>();
  const [refuseReason, setRefuseReason] = useState("");
  const [isEmpty, setIsEmpty] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openNotify, setOpenNotify] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    let response = await request<RefundRequest>("GET", `/pal/refund/` + id, {
      refundRequestId: id,
    })
      .then((res) => {
        // let data = res.data;
        setRefundRequest(res.data);
        console.log(res.data);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/refund/" + id, { replace: true });
      });
    setLoading(true);
  }
  useEffect(() => {
    fetchData();
  }, []);
  async function accept() {
    let response = await request("PUT", `/pal/refund/accept/` + id, {
      refundRequestId: id,
    })
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/refund/" + id, { replace: true });
      });
    fetchData();
  }
  async function deny() {
    let response = await request("PUT", `/pal/refund/deny/` + id, {
      refundRequestId: id,
      refuseReason: refuseReason,
    })
      .then(() => {
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/refund/" + id, { replace: true });
      });
    fetchData();
  }
  let navigate = useNavigate();
  const handleBack = () => {
    navigate(-1);
  };
  const handleChangeRefuseReason = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRefuseReason(e.target.value);
  };
  const handleRefuse = () => {
    if (refuseReason === "") setIsEmpty(true);
    else deny();
  };
  return (
    // <div className="view-refund-detail">
    <Container className="view-refund-detail">
      <Backdrop
        sx={{
          color: "#fff",
          zIndex: (theme) => theme.zIndex.drawer + 1,
        }}
        open={!loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <h2>Chi tiết yêu cầu</h2>
      <div className="wrap">
        <div className="container">
          <div className="content">
            {refundRequest ? (
              <div>
                <div className="div-left-right">
                  <div className="div-left">
                    <div className="label-textfield">
                      <p className="label">ID</p>
                      <p className="textfield"> {id} </p>
                    </div>
                    <div className="label-textfield">
                      <p className="label">Người đặt hàng</p>
                      <p className="textfield"> {refundRequest.userName}</p>
                    </div>
                    <div className="label-textfield">
                      <p className="label">Ngày đặt hàng</p>
                      <p className="textfield">
                        {" "}
                        {moment(refundRequest.orderItemCreatedAt).format(
                          "hh:mm DD-MM-YYYY"
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="div-right">
                    <div className="label-textfield">
                      <p className="label">Mã sản phẩm ĐH</p>
                      <p className="textfield"> {refundRequest.orderItemId}</p>
                    </div>
                    <div className="label-textfield">
                      <p className="label">Cửa hàng</p>
                      <p className="textfield"> {refundRequest.shopName}</p>
                    </div>
                    <div className="label-textfield">
                      <p className="label">Ngày yêu cầu</p>
                      <p className="textfield">
                        {" "}
                        {moment(refundRequest.createdAt).format(
                          "hh:mm DD-MM-YYYY"
                        )}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="label-textfield">
                  <p className="label">Mã đơn hàng</p>
                  <p> {refundRequest.orderBillId}</p>
                </div>
                <div className="label-textfield">
                  <p className="label">Thời gian sử dụng</p>
                  <p>
                    {moment(refundRequest.usedStart).format("hh:mm DD-MM-YYYY")}{" "}
                    ~ {moment(refundRequest.usedEnd).format("hh:mm DD-MM-YYYY")}
                  </p>
                </div>
                <div className="label-textfield">
                  <p className="label">Lý do</p>
                  <p>{refundRequest.requestReason}</p>
                </div>
                {refundRequest.status !== "ACCEPT" ? (
                  <div className="label-textfield">
                    <p className="label">Lý do từ chối</p>
                    {refundRequest.status === "DENY" ? (
                      <p>{refundRequest.refuseReason}</p>
                    ) : (
                      <TextField
                        className="textarea"
                        multiline
                        rows={3}
                        // maxRows={Infinity}
                        value={refuseReason}
                        error={isEmpty}
                        helperText={
                          isEmpty ? "Vui lòng điền vào trường này." : ""
                        }
                        onChange={handleChangeRefuseReason}
                      />
                    )}
                  </div>
                ) : null}
                {refundRequest.status === "PROCESSING" ? (
                  <div>
                    <Button
                      className="btn-accept"
                      variant="contained"
                      onClick={accept}
                    >
                      Đồng ý
                    </Button>
                    <Button variant="contained" onClick={handleRefuse}>
                      Từ chối
                    </Button>
                  </div>
                ) : (
                  <div className="label-textfield">
                    <p className="label">Trạng thái</p>
                    <p>{refundRequest.status}</p>
                  </div>
                )}
              </div>
            ) : (
              <div>Không tìm thấy yêu cầu hoàn trả.</div>
            )}
            <Button
              className="btn-right"
              variant="contained"
              onClick={handleBack}
            >
              Quay lại
            </Button>
          </div>
        </div>
      </div>
      <Snackbar
        open={openNotify}
        autoHideDuration={1000}
        onClose={handleCloseNotify}
        message="Cập nhật thành công!"
        // action={action}
      />
    </Container>
    // </div>
  );
};

export default ViewRefundDetail;
