import "./refund-request-list.scss";
import variables from "../../assets/scss/_variables.scss";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  createTheme,
  IconButton,
  InputBase,
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  ThemeProvider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { useEffect, useState } from "react";
import { FilterAlt } from "@mui/icons-material";
import ClearIcon from "@mui/icons-material/Clear";
import InfoIcon from "@mui/icons-material/Info";
import SideBarMod from "../../shared/Component/SideBarMod";
import { StatusType } from "../../shared/types/StatusType";
import { useNavigate } from "react-router-dom";
import styled from "@emotion/styled";
import request from "../../shared/services/ApiService";
import { ACCESS_TOKEN } from "../../constants";
import moment from "moment";

export interface Refund {
  accountId: number;
  createdAt: Date;
  id: number;
  orderBillId: string;
  orderItemCreatedAt: Date;
  orderItemId: number;
  refuseReason: string;
  reportType: string;
  requestReason: string;
  status: string;
}
export interface RefundResult {
  page: number;
  results: [Refund[]];
  totalPages: number;
  totalResults: number;
}
const RefundRequestList: React.FC = () => {
  const [rows, setRows] = useState([] as Refund[]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [searchText, setSearchText] = useState("");
  const [loading, setLoading] = useState(false);
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    let response = await request<RefundResult>("GET", `/pal/refund/all`, {
      page: pageNumber,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.results[0].map((item) => item));
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/refundrequestlist", { replace: true });
      });
    setLoading(true);
  }
  useEffect(() => {
    fetchData();
    return () => {
      setRows([]);
    };
  }, [pageNumber]);

  async function search() {
    let response = await request<RefundResult>(
      "GET",
      `/pal/refund/all/search`,
      {
        orderBillId: searchText,
        page: pageNumber,
        size: 6,
      }
    )
      .then((res) => {
        let data = res.data;
        setRows(data.results[0].map((item) => item));
        // console.log(data.results);
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/refundrequestlist", { replace: true });
      });
    // fetchData();
  }
  const [open, setOpen] = useState(false);
  const [statusList, setStatusList] = useState([
    { id: 1, title: "Processing", isChoose: false },
    { id: 2, title: "Accept", isChoose: false },
    { id: 3, title: "Deny", isChoose: false },
  ] as StatusType[]);
  const [filterValue, setFilterValue] = useState([] as string[]);
  const handleClickOpen = () => {
    setOpen(open ? false : true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const blockShop = (id: number) => {
    // console.log(id);
    setRows((prevRows) =>
      prevRows.map((row) =>
        row.id === id
          ? row.status == ""
            ? { ...row, status: "Khóa" }
            : { ...row, status: "" }
          : row
      )
    );
  };
  const handleStatus = (id: number) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleRemoveOption = (title: string) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.title === title) {
          const arr = filterValue.filter((val) => val !== item.title);
          setFilterValue(arr);
          return [...ack, { ...item, isChoose: false }];
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleApplyFilter = () => {
    setOpen(false);
  };
  let navigate = useNavigate();
  const handleViewDetail = (id: number) => {
    let path = "/manager/refund/" + id;
    navigate(path);
  };
  const handleRemoveAllFilter = () => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        // const arr = filterValue.filter((val) => val !== item.title);
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setFilterValue([]);
  };
  const handleSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    let text = e.target.value;
    setSearchText(text);
    if (text === "") {
      fetchData();
    }
  };
  const handleEnter = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (e.key === "Enter") search();
  };
  const handleChangePage = (page: number) => {
    // console.log("page", page);
    setPageNumber(page);
  };
  const theme = createTheme({
    palette: {
      primary: { main: variables.colorMain },
      info: { main: "#fff" },
    },
    components: {
      MuiTableHead: {
        styleOverrides: {
          root: {
            backgroundColor: variables.colorMain,
            position: "sticky",
            top: 0,
          },
        },
      },
    },
  });
  const StyledTableRow = styled(TableRow)(() => ({
    "&:nth-of-type(even)": {
      backgroundColor: variables.colorRowOdd,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));
  return (
    // <div className="refund-request-list">
    <ThemeProvider theme={theme}>
      <Container className="refund-container">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h2>Danh sách yêu cầu hoàn trả</h2>
        <div className="search-filter">
          <div className="search-bar">
            <IconButton onClick={search}>
              <SearchIcon className="search-icon" />
            </IconButton>
            <InputBase
              type="search"
              style={{ width: 400 }}
              placeholder="Tìm kiếm theo mã đơn hàng"
              value={searchText}
              onChange={handleSearchText}
              onKeyDown={handleEnter}
            />
          </div>
          <Button
            className="btn-filter"
            variant="contained"
            onClick={handleClickOpen}
          >
            <FilterAlt />
            Bộ lọc
          </Button>
        </div>
        <div className="filter" hidden={open ? false : true}>
          <IconButton onClick={handleClose} className="btn-close">
            <ClearIcon fontSize="small" />
          </IconButton>
          <div className="filter-value-container">
            {filterValue.length != 0 ? (
              <p className="selected-title">Đã chọn:</p>
            ) : null}
            {filterValue.length != 0
              ? filterValue.map((statusValue) => (
                  <div className="filter-value" key={statusValue}>
                    <p>{statusValue}</p>{" "}
                    <IconButton
                      onClick={() => handleRemoveOption(statusValue)}
                      className="btn-close"
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  </div>
                ))
              : null}
            {filterValue.length != 0 ? (
              <Button onClick={handleRemoveAllFilter}>Xóa tất cả</Button>
            ) : null}
          </div>
          <p className="status-title">Tình trạng</p>
          {statusList.map((status) => (
            <Button
              key={status.id}
              className={
                status.isChoose
                  ? "btn-filter-option isChoose"
                  : "btn-filter-option"
              }
              variant="contained"
              color="info"
              onClick={() => handleStatus(status.id)}
            >
              {status.title}
            </Button>
          ))}
          <Button
            className="btn-apply-filter"
            variant="contained"
            onClick={handleApplyFilter}
          >
            Lọc
          </Button>
        </div>

        <div className="table-container">
          <TableContainer className="container-table">
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell width={"50"}>ID</TableCell>
                  <TableCell width={"200"}>Mã đơn hàng</TableCell>
                  <TableCell width={"150"}>Mã SP ĐH</TableCell>
                  <TableCell width={"180"}>Ngày đặt</TableCell>
                  <TableCell width={"180"}>Ngày gửi đơn</TableCell>
                  <TableCell width={"260"}>Lý do</TableCell>
                  <TableCell width={"150"} align="center">
                    Trạng thái
                  </TableCell>
                  <TableCell width={"150"} align="center">
                    Tùy chọn
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {rows.length == 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <h3>Không có yêu cầu hoàn trả nào</h3>
                    </TableCell>
                  </TableRow>
                ) : (
                  rows.map((row) => (
                    <StyledTableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.orderBillId}</div>
                      </TableCell>
                      <TableCell>{row.orderItemId}</TableCell>
                      <TableCell>
                        {moment(row.orderItemCreatedAt).format(
                          "hh:mm DD-MM-YYYY"
                        )}
                      </TableCell>
                      <TableCell>
                        {moment(row.createdAt).format("hh:mm DD-MM-YYYY")}
                      </TableCell>
                      <TableCell>{row.requestReason}</TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center" style={{ display: "flex" }}>
                        <Button
                          style={{ margin: "auto" }}
                          onClick={() => handleViewDetail(row.id)}
                          title="Chi tiết"
                        >
                          <InfoIcon />
                        </Button>
                      </TableCell>
                    </StyledTableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Pagination
              className="table-pagination"
              count={totalPage}
              page={pageNumber}
              onChange={(event: React.ChangeEvent<unknown>, page: number) =>
                handleChangePage(page)
              }
              variant="outlined"
              color="primary"
              shape="rounded"
            />
          </TableContainer>
        </div>
      </Container>
    </ThemeProvider>
  );
};

export default RefundRequestList;
