import "./moderator-list.scss";
import variables from "../../assets/scss/_variables.scss";
import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Container,
  createTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputBase,
  InputLabel,
  Pagination,
  Snackbar,
  SnackbarCloseReason,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  ThemeProvider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ClearIcon from "@mui/icons-material/Clear";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { useEffect, useState } from "react";
import { Add, FilterAlt } from "@mui/icons-material";
import { StatusType } from "../../shared/types/StatusType";
import styled from "@emotion/styled";
import request from "../../shared/services/ApiService";
import { account, accountResult } from "../UserList/UserList";
import { ACCESS_TOKEN } from "../../constants";
import { useNavigate } from "react-router-dom";
import axios, { AxiosError } from "axios";
import moment from "moment";
// import { Status } from "../ShopList/ShopList";
const ModeratorList: React.FC = () => {
  const [rows, setRows] = useState([] as account[]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [openNotify, setOpenNotify] = useState(false);
  const [mess, setMess] = useState("");
  // const [openUpdateNotify, setOpenUpdateNotify] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setMess("");
    setOpenNotify(false);
  };
  // const handleClickUpdateNotify = () => {
  //   setOpenUpdateNotify(true);
  // };
  // const handleCloseUpdateNotify = (
  //   event: Event | React.SyntheticEvent<any, Event>,
  //   reason: SnackbarCloseReason
  // ) => {
  //   if (reason === "clickaway") {
  //     return;
  //   }
  //   setOpenUpdateNotify(false);
  // };
  let navigate = useNavigate();
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      return Promise.reject("No access token set.");
    }
    let response = await request<accountResult>("GET", `/admin/accounts`, {
      page: pageNumber,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.results[0].map((item) => item));
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/modlist", { replace: true });
        const err = error as AxiosError;
        if (err.code === "400") setIsWrongEmail(true);
      });
    // console.log("load");
    setLoading(true);
  }
  useEffect(() => {
    setLoading(false);
    fetchData();
    return () => {
      setRows([]);
    };
  }, [pageNumber]);
  async function addMod() {
    let response = await request("POST", `/admin/account/mod`, {
      email: emailValue,
    })
      .then(() => {
        setOpenDialog(false);
        setIsWrongEmail(false);
        setEmailValue("");
        setMess("Thêm quản lý thành công!");
        setOpenNotify(true);
      })
      .catch((error: Error | AxiosError) => {
        console.log("Error: ", error.message);
        navigate("/manager/modlist", { replace: true });
        if (axios.isAxiosError(error)) {
          if (error.response?.status === 400) setIsWrongEmail(true);
        }
      });
    fetchData();
  }
  async function lockAcc(id: number) {
    let response = await request("PUT", `/admin/lock/` + id)
      .then(() => {
        fetchData();
        setMess("Cập nhật thành công!");
        setOpenNotify(true);
      })
      .catch((error: Error | AxiosError) => {
        console.log("Error: ", error);
        navigate("/manager/modlist", { replace: true });
        if (axios.isAxiosError(error)) {
          if (error.response?.status === 400) {
            setMess("Không thể khóa tài khoản Admin.");
            setOpenNotify(true);
          }
        }
      });
  }
  async function unlockAcc(id: number) {
    let response = await request("PUT", `/admin/unlock/` + id)
      .then(() => {
        fetchData();
        setMess("Cập nhật thành công!");
        setOpenNotify(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/modlist", { replace: true });
      });
  }
  async function search() {
    let response = await request<accountResult>(
      "GET",
      `/admin/search/accounts`,
      {
        page: pageNumber,
        query: searchText,
        size: 6,
      }
    )
      .then((res) => {
        let data = res.data;
        // JSON.parse(JSON.stringify(data.results));
        setRows(data.results[0].map((item) => item));
        // console.log(data);
        setTotalPage(data.totalPages);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/modlist", { replace: true });
      });
    // fetchData();
  }
  const [openDialog, setOpenDialog] = useState(false);
  const [openFilter, setOpenFilter] = useState(false);
  const [statusList, setStatusList] = useState([
    { id: 1, title: "ACTIVE", isChoose: false },
    { id: 2, title: "LOCKED", isChoose: false },
  ]);
  const [filterValue, setFilterValue] = useState([] as string[]);
  const [emailValue, setEmailValue] = useState("");
  const [isEmpty, setIsEmpty] = useState(false);
  const [isInvalidEmail, setIsInvalidEmail] = useState(false);
  const [isWrongEmail, setIsWrongEmail] = useState(false);
  const [searchText, setSearchText] = useState("");
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setIsEmpty(false);
    setIsInvalidEmail(false);
    setIsWrongEmail(false);
    setEmailValue("");
  };
  const handleClickOpenFilter = () => {
    setOpenFilter(openFilter ? false : true);
  };
  const handleCloseFilter = () => {
    setOpenFilter(false);
  };
  const handleSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    let text = e.target.value;
    setSearchText(text);
    if (text === "") {
      fetchData();
      console.log("searchText");
    }
  };
  const handleEnter = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (e.key === "Enter") search();
  };

  const handleStatus = (id: number) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleRemoveOption = (title: string) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.title === title) {
          const arr = filterValue.filter((val) => val !== item.title);
          setFilterValue(arr);
          return [...ack, { ...item, isChoose: false }];
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleApplyFilter = () => {
    setOpenFilter(false);
    console.log(filterValue);
  };
  const handleEmailValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmailValue(e.target.value);
  };
  const validEmail = new RegExp(
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  );
  const handleAddMod = () => {
    if (emailValue !== "")
      if (validEmail.test(emailValue)) {
        setIsEmpty(false);
        setIsInvalidEmail(false);
        addMod();
      } else {
        setIsEmpty(false);
        setIsInvalidEmail(true);
      }
    else {
      setIsEmpty(true);
    }
  };
  const handleRemoveAllFilter = () => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        // const arr = filterValue.filter((val) => val !== item.title);
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setFilterValue([]);
  };

  const handleChangePage = (page: number) => {
    setPageNumber(page);
  };
  const theme = createTheme({
    palette: {
      primary: { main: variables.colorMain },
      info: { main: "#fff" },
    },
    components: {
      MuiTableHead: {
        styleOverrides: {
          root: {
            backgroundColor: variables.colorMain,
            position: "sticky",
            top: 0,
          },
        },
      },
    },
  });
  const StyledTableRow = styled(TableRow)(() => ({
    "&:nth-of-type(even)": {
      backgroundColor: variables.colorRowOdd,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  return (
    // <div className="mod-list">
    <ThemeProvider theme={theme}>
      <Container className="mod-container">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <h2>Danh sách quản lý</h2>
        <div className="search-add">
          <div className="search-bar">
            <IconButton onClick={search}>
              <SearchIcon className="search-icon" />
            </IconButton>
            <InputBase
              type="search"
              style={{ width: 400 }}
              placeholder="Tìm kiếm theo tên hoặc email"
              value={searchText}
              onChange={handleSearchText}
              onKeyDown={handleEnter}
              // InputProps={{ disableUnderline: true }}
            />
          </div>
          <div className="filter-add">
            <Button
              className="btn-filter"
              variant="contained"
              onClick={handleClickOpenFilter}
            >
              <FilterAlt />
              Bộ lọc
            </Button>
            <Button
              className="btn-add"
              variant="contained"
              onClick={handleClickOpenDialog}
            >
              <Add /> Thêm quản lý
            </Button>
          </div>
        </div>
        <div className="filter" hidden={openFilter ? false : true}>
          <IconButton onClick={handleCloseFilter} className="btn-close">
            <ClearIcon fontSize="small" />
          </IconButton>
          <div className="filter-value-container">
            {filterValue.length != 0 ? (
              <p className="selected-title">Đã chọn:</p>
            ) : null}
            {filterValue.length != 0
              ? filterValue.map((statusValue) => (
                  <div className="filter-value" key={statusValue}>
                    <p>{statusValue}</p>{" "}
                    <IconButton
                      onClick={() => handleRemoveOption(statusValue)}
                      className="btn-close"
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  </div>
                ))
              : null}
            {filterValue.length != 0 ? (
              <Button onClick={handleRemoveAllFilter}>Xóa tất cả</Button>
            ) : null}
          </div>
          <p className="status-title">Tình trạng</p>
          {statusList.map((status) => (
            <Button
              key={status.id}
              className={
                status.isChoose
                  ? "btn-filter-option isChoose"
                  : "btn-filter-option"
              }
              variant="contained"
              color="info"
              onClick={() => handleStatus(status.id)}
            >
              {status.title}
            </Button>
          ))}
          <Button
            className="btn-apply-filter"
            variant="contained"
            onClick={handleApplyFilter}
          >
            Lọc
          </Button>
        </div>
        <div className="table-container">
          <TableContainer className="container-table">
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell width={"50"}>ID</TableCell>
                  {/* <TableCell width={"100"} align="center">
                      Hình ảnh
                    </TableCell> */}
                  <TableCell width={"250"}>Họ tên</TableCell>
                  <TableCell width={"250"}>Email</TableCell>
                  <TableCell width={"170"}>Ngày đăng ký</TableCell>
                  <TableCell width={"150"}>Tình trạng</TableCell>
                  <TableCell width={"100"}>Tùy chọn</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {rows.length == 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <h3>Không có quản lý nào</h3>
                    </TableCell>
                  </TableRow>
                ) : (
                  rows.map((row, index) => (
                    // <StyledTableRow>
                    <StyledTableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      {/* <TableCell className="cell-img" align="center">
                          <img src={row.image} />
                        </TableCell> */}
                      <TableCell>
                        <div className="long-text">{row.fullName}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.email}</div>
                      </TableCell>
                      <TableCell>
                        {moment(row.createdAt).format("hh:mm DD-MM-YYYY")}
                      </TableCell>
                      <TableCell>{row.status}</TableCell>
                      <TableCell>
                        {row.status == "LOCKED" ? (
                          <Button
                            onClick={() => unlockAcc(row.id)}
                            title="Mở khóa"
                          >
                            <LockOpenIcon />
                          </Button>
                        ) : (
                          <Button onClick={() => lockAcc(row.id)} title="Khóa">
                            <LockIcon />
                          </Button>
                        )}
                      </TableCell>
                    </StyledTableRow>
                    // </StyledTableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Pagination
              className="table-pagination"
              count={totalPage}
              page={pageNumber}
              onChange={(event: React.ChangeEvent<unknown>, page: number) =>
                handleChangePage(page)
              }
              variant="outlined"
              color="primary"
              shape="rounded"
            />
          </TableContainer>
          {/* </Box> */}
        </div>
      </Container>
      <Dialog
        open={openDialog}
        fullWidth
        onClose={handleCloseDialog}
        className="dialog-container"
      >
        <DialogTitle className="dialog-title">
          {/* Thêm quản lý */}
          <Box display="flex" alignItems="center">
            <Box flexGrow={1}>Thêm quản lý</Box>
            <Box>
              <IconButton onClick={handleCloseDialog}>
                <ClearIcon />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent>
          <InputLabel>Email cần cấp quyền</InputLabel>
          <TextField
            autoFocus
            margin="dense"
            type="email"
            fullWidth
            value={emailValue}
            onChange={handleEmailValue}
            error={isEmpty || isInvalidEmail || isWrongEmail}
            helperText={
              isEmpty
                ? "Vui lòng điền vào trường này."
                : isInvalidEmail
                ? "Vui lòng điền đúng Email."
                : isWrongEmail
                ? "Email này đã được cấp quyền quản lý hoặc không tồn tại trong hệ thống."
                : ""
            }
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAddMod}>Xác nhận</Button>
          <Button onClick={handleCloseDialog}>Hủy</Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={openNotify}
        autoHideDuration={1000}
        onClose={handleCloseNotify}
        message={mess}
        // action={action}
      />
      {/* <Snackbar
        open={openUpdateNotify}
        autoHideDuration={1000}
        onClose={handleCloseUpdateNotify}
        message="Cập nhật thành công!"
        // action={action}
      /> */}
    </ThemeProvider>
    // </div>
  );
};

export default ModeratorList;
