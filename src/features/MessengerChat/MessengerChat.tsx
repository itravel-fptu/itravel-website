import { useEffect } from "react";

const MessengerChat = () => {
  useEffect(() => {
    const script = document.createElement("script");

    script.src = "./MessengerChat.js";
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <div>
      <div id="fb-root"></div>

      <div id="fb-customer-chat" className="fb-customerchat"></div>
      <script>{}</script>
    </div>
  );
};

export default MessengerChat;
