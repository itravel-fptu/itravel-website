import { Backdrop, CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Navigate, Outlet, useNavigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import { Header } from "../../parts";
import SideBarMod from "../../shared/Component/SideBarMod";
import { IUser } from "../../shared/types/UserType";
import { authRoles as Role } from "../../shared/constants/authRoles";

type PrivateManagerType = {
  acceptedRole: string;
};

const PrivateManager: React.FC<PrivateManagerType> = ({ acceptedRole }) => {
  let auth = useAuth();
  let navigate = useNavigate();
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    const loadCurrentUser = async () => {
      await auth
        .loadCurrentlyLoggedInUser((user: IUser) => {
          console.log("Get USER in MANAGER DONE: ", auth.user);
          if (!user) {
            navigate("/", { replace: true });
          } else if (
            user.roles.filter((role) => role.roleName === acceptedRole)
              .length === 0
          ) {
            console.error("Unauthorized");
            navigate("/", { replace: true });
          }
          if (
            user.roles.filter((role) => role.roleName === Role.ADMIN).length !==
            0
          )
            setIsAdmin(true);
          // refetch();
        })
        .catch((error) => {
          console.log("Error: ", error);
          navigate("/", { replace: true });
        });
    };
    loadCurrentUser();
  }, []);

  // if (auth.isLoading) return <div>Loading...</div>;

  return (
    <div>
      <Header />
      <div className="flex-row">
        <SideBarMod isAdmin={isAdmin} />
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={auth.isLoading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Outlet />
      </div>
    </div>
  );
};

export default PrivateManager;
