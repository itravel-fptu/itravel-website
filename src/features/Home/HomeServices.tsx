import request from "../../shared/services/ApiService";
import { SearchResultType } from "../../shared/services/SearchService";

export const getNewestServices = async (
  pageSize?: number,
  page?: number
): Promise<SearchResultType> => {
  const params = {
    size: 10,
    page: page,
  };

  const url = `/sp/service/new`;

  const results = await request<SearchResultType>("GET", url, params).then(
    (res) => res.data
  );

  return results;
};

export const getRandomServices = async (
  pageSize?: number,
  page?: number
): Promise<SearchResultType> => {
  const params = {
    size: 10,
    page: page,
  };

  const url = `/sp/service/random`;

  const results = await request<SearchResultType>("GET", url, params).then(
    (res) => res.data
  );

  return results;
};

export const getPopularServices = async (
  pageSize?: number,
  page?: number
): Promise<SearchResultType> => {
  const params = {
    size: 10,
    page: page,
  };

  const url = `/sp/service/popular`;

  const results = await request<SearchResultType>("GET", url, params).then(
    (res) => res.data
  );

  return results;
};
