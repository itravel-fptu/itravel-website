import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { Link, useNavigate } from "react-router-dom";

// Components
import CategoryCard from "./Components/CategoryCard";
import ServiceCard from "./../../shared/Component/ServiceCard";
import ServiceCardSlider from "./Components/ServiceCardSlider";

import { Container, Grid, LinearProgress } from "@mui/material";

// Types
import { ServiceCardType } from "./../../shared/types/ServiceType";

// Styles & Assets
import "./home.scss";
import SearchIcon from "@mui/icons-material/Search";

// Data
import { CATEGORIES } from "./../../shared/constants/category";
import {
  getNewestServices,
  getPopularServices,
  getRandomServices,
} from "./HomeServices";
import {
  SearchResultType,
  searchServiceByKeyword,
} from "../../shared/services/SearchService";

const resultImagePlaceholder = require("./../../assets/images/service-placeholder-image.jpg");

const Home: React.FC = () => {
  let navigate = useNavigate();
  const [searchKeyword, setSearchKeyword] = useState("");
  const [isShowSearchPanel, setIsShowSearchPanel] = useState(false);

  const { data: popularServices, isLoading: isPopularServicesLoading } =
    useQuery<SearchResultType>("popularServices", () => getRandomServices());

  const { data: randomServices, isLoading: isRandomServicesLoading } =
    useQuery<SearchResultType>("randomServices", () => getRandomServices());

  const { data: newestServices, isLoading: isNewestServicesLoading } =
    useQuery<SearchResultType>("newestServices", () => getNewestServices());

  const {
    data: searchResultHomepage,
    refetch,
    isLoading: isSearchLoading,
  } = useQuery<SearchResultType>(
    "searchResultHomepage",
    () => searchServiceByKeyword(searchKeyword, 10),
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  const handleFilter = (event: React.FormEvent<HTMLInputElement>): void => {
    const value = event.currentTarget.value;
    setSearchKeyword(value);
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (searchKeyword !== "") {
        refetch();
      } else {
        setIsShowSearchPanel(false);
      }
    }, 1500);

    return () => clearTimeout(delayDebounceFn);
  }, [searchKeyword]);

  const handleEnterSearch = (event: any) => {
    if (event.key === "Enter") {
      navigate(`/search?keyword=${event.target.value}`);
    }
  };

  if (isPopularServicesLoading || isNewestServicesLoading) {
    return (
      <div className="isLoading">
        <LinearProgress />
      </div>
    );
  }

  return (
    <div className="homepage">
      <section className="hero">
        <div className="hero-background flex-column">
          <h1 className="hero-title">KHÁM PHÁ CẢ THẾ GIỚI</h1>
          <div className="hero-search">
            <div className="search-bar flex-row flex-jc-sb">
              <input
                type="text"
                placeholder="Tìm kiếm địa điểm, hoạt động"
                value={searchKeyword}
                onChange={handleFilter}
                onFocus={() => setIsShowSearchPanel(true)}
                onBlur={() => setIsShowSearchPanel(false)}
                onKeyDown={handleEnterSearch}
              />
              <div className="search-bar__button">
                <SearchIcon
                  fontSize="large"
                  sx={{ color: "#fff", m: "auto" }}
                />
              </div>
            </div>
            <div
              className="search-results"
              style={
                isShowSearchPanel ? { display: "initial" } : { display: "none" }
              }
            >
              <div className="search-results__list">
                {searchResultHomepage &&
                  searchResultHomepage?.serviceInfos.map(
                    (service: ServiceCardType) => (
                      <Link to={`/service/${service.id}`} key={service.id}>
                        <div
                          className="search-results__item flex-row"
                          key={service.id}
                        >
                          <img
                            src={
                              service.images[0]
                                ? service.images[0]
                                : resultImagePlaceholder
                            }
                            alt={service.title}
                          />
                          <div className="info">
                            <span>{service.categoryName}</span>
                            <h3>{service.title}</h3>
                            <span>{service.cityName}</span>
                          </div>
                        </div>
                      </Link>
                    )
                  )}
              </div>
            </div>
          </div>
        </div>
      </section>
      <Container>
        <section className="service-categories">
          <h2>Khám phá danh mục</h2>
          <Grid
            container
            rowSpacing={{ xs: "0.5em", md: "1em" }}
            columnSpacing={{ xs: "0.5em", md: "1em" }}
          >
            {CATEGORIES.map((category) => (
              <Grid item key={category.id} xs={8} sm={10} md={6} lg={4}>
                <Link to={`/search?category_id=${category.id}`}>
                  <CategoryCard category={category} />
                </Link>
              </Grid>
            ))}
          </Grid>
        </section>

        <section>
          <h2>Dịch vụ nổi bật</h2>
          {popularServices && (
            <ServiceCardSlider travelServices={popularServices.serviceInfos} />
          )}
        </section>

        <section className="popular-services">
          <h2>Có thể bạn sẽ thích</h2>
          <Grid container spacing={6}>
            {randomServices?.serviceInfos.map(
              (service: ServiceCardType, index: number) =>
                index < 8 ? (
                  <Grid item key={service.id} sm={6} md={4} lg={3}>
                    <Link to={`/service/${service.id}`}>
                      <ServiceCard service={service} />
                    </Link>
                  </Grid>
                ) : (
                  ""
                )
            )}
          </Grid>
        </section>

        <section className="newest-services">
          <h2>Dịch vụ mới nhất</h2>
          <Grid container spacing={6}>
            {newestServices?.serviceInfos.map(
              (service: ServiceCardType, index: number) =>
                index < 6 ? (
                  <Grid item key={service.id} sm={6} md={4} lg={3}>
                    <Link to={`/service/${service.id}`}>
                      <ServiceCard service={service} />
                    </Link>
                  </Grid>
                ) : (
                  ""
                )
            )}
          </Grid>
        </section>
      </Container>
    </div>
  );
};

export default Home;
