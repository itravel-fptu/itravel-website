import IconButton from "@mui/material/IconButton"
import ArrowForwardIosRoundedIcon from '@mui/icons-material/ArrowForwardIosRounded';

type ArrowButtonProps = {
    className?: any;
    style?: any;
    onClick?: any;
}

const NextArrowButton: React.FC<ArrowButtonProps> = ({ className, style, onClick }) => {

    return (
        <div className={className}
            onClick={onClick}>
            <IconButton color="primary" aria-label="next button">
                <ArrowForwardIosRoundedIcon />
            </IconButton>
        </div>
    )
}

export default NextArrowButton
