// Components
import { Card, CardContent, CardMedia } from "@mui/material";

// Types
import { CategoryCardType } from "../HomeTypes";

type CategoryCardProps = {
  category: CategoryCardType;
};

const CategoryCard: React.FC<CategoryCardProps> = ({ category }) => {
  return (
    <Card
      className="category-card flex-row"
      sx={{ border: 1, borderColor: "grey.400" }}
    >
      <div className="card-image">
        <CardMedia
          component="img"
          image={category.background_path}
          alt={category.title}
        />
      </div>
      <div className="card-content">
        <CardContent sx={{ p: "0.5rem" }}>
          <h3 className="card-content__title">{category.title}</h3>
          <p className="card-description__title">{category.description}</p>
        </CardContent>
      </div>
    </Card>
  );
};

export default CategoryCard;
