// Components
import Slider from "react-slick";
import { Link } from "react-router-dom";

import ServiceCard from "./../../../shared/Component/ServiceCard";

// Types
import { ServiceCardType } from "./../../../shared/types/ServiceType"

// Styles
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type ServiceCardSliderPropsType = {
  travelServices: ServiceCardType[];
}

const ServiceCardSlider: React.FC<ServiceCardSliderPropsType> = ({travelServices}) => {
  const settings = {
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    draggable: false
  }

  return (
    <Slider {...settings} className="slide">
      {travelServices.map((service) => (
              <Link to={`/service/${service.id}`} key={service.id}>
                <ServiceCard service={service} />
              </Link>
            ))}
    </Slider>
  )
}

export default ServiceCardSlider
