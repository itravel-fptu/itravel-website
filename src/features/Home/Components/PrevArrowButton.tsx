import IconButton from "@mui/material/IconButton"
import ArrowBackIosRoundedIcon from '@mui/icons-material/ArrowBackIosRounded';


type ArrowButtonProps = {
    className?: any;
    style?: any;
    onClick?: any;
}


const PrevArrowButton: React.FC<ArrowButtonProps> = (props) => {
    const {className, style, onClick } = props;
    return (
        <div className={className}
            onClick={onClick}>
            <IconButton className="arrow-button" color="primary" aria-label="prev button">
                <ArrowBackIosRoundedIcon />
            </IconButton>
        </div>
    )
}

export default PrevArrowButton
