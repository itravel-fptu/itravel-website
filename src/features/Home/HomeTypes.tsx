export type CategoryCardType = {
    id: number;
    title: string;
    description: string;
    background_path: string;
}

export type ServiceCardType = {
    id: number;
    title: string;
    city: string;
    category: string;
    rating_average: number;
    rating_count: number;
    background_path: string;
}