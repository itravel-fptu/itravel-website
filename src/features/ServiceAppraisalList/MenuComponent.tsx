import { Button, MenuItem, Popover } from "@mui/material";
import React, { useRef, useState } from "react";
import MoreVertIcon from "@mui/icons-material/MoreVert";
type Props = {
  rowId: number;
  handleAccept: (id: number) => void;
  handleRefuse: (id: number) => void;
};
const MenuComponent: React.FC<Props> = ({
  rowId,
  handleAccept,
  handleRefuse,
}) => {
  const buttonRef = useRef(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const openAnchor = Boolean(anchorEl);
  //   useEffect(() => {
  //     setAnchorEl(buttonRef.current);
  //   }, [buttonRef]);
  const handleClickAnchor = () => {
    setAnchorEl(buttonRef.current);
    console.log(anchorEl);
  };
  const handleCloseAnchor = () => {
    setAnchorEl(null);
  };
  const id = openAnchor ? "simple-popover" : undefined;
  return (
    <div>
      <Button
        ref={buttonRef}
        aria-describedby={id}
        // variant="contained"
        onClick={handleClickAnchor}
      >
        <MoreVertIcon />
      </Button>
      <Popover
        id={id}
        open={openAnchor}
        anchorEl={anchorEl}
        onClose={handleCloseAnchor}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <MenuItem key="1" onClick={() => handleAccept(rowId)}>
          Chấp nhận
        </MenuItem>
        <MenuItem key="2" onClick={() => handleRefuse(rowId)}>
          Từ chối
        </MenuItem>
      </Popover>
      {/* <Button
                              className="btn-act"
                              onClick={() => handleAccept(row.id)}
                              title="Chấp nhận"
                            >
                              <CheckCircleIcon />
                            </Button>
                            <Button
                              className="btn-act"
                              onClick={() => handleRefuse(row.id)}
                              title="Từ chối"
                            >
                              <HighlightOffIcon />
                            </Button> */}
    </div>
  );
};

export default MenuComponent;
