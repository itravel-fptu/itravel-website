import "./service-appraisal-list.scss";
import variables from "../../assets/scss/_variables.scss";
import {
  Backdrop,
  Button,
  CircularProgress,
  Container,
  createTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputBase,
  InputLabel,
  Pagination,
  Snackbar,
  SnackbarCloseReason,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  ThemeProvider,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import InfoIcon from "@mui/icons-material/Info";
import { useEffect, useState } from "react";
import { FilterAlt } from "@mui/icons-material";
import ClearIcon from "@mui/icons-material/Clear";
import { StatusType } from "../../shared/types/StatusType";
import { useNavigate, useParams } from "react-router-dom";
import styled from "@emotion/styled";
import MenuComponent from "./MenuComponent";
import { ACCESS_TOKEN } from "../../constants";
import request from "../../shared/services/ApiService";
import moment from "moment";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";
export interface ServiceInfo {
  categoryId: number;
  categoryName: string;
  cityId: number;
  cityName: string;
  createdAt: Date;
  id: number;
  images: [string];
  ratingAverage: number;
  ratingCount: number;
  shopId: number;
  shopShopName: string;
  status: string;
  title: string;
}
export interface ServiceResult {
  page: number;
  serviceInfos: ServiceInfo[];
  size: number;
  totalItem: number;
  totalPage: number;
}
const ServiceAppraisalList: React.FC = () => {
  const param = useParams<{ shopid?: string }>();
  const shopId = param.shopid;
  const [rows, setRows] = useState([] as ServiceInfo[]);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [loading, setLoading] = useState(false);
  const [isFilter, setIsFilter] = useState(false);
  const [shopName, setShopName] = useState("");
  const [openNotify, setOpenNotify] = useState(false);
  const handleClickNotify = () => {
    setOpenNotify(true);
  };
  const handleCloseNotify = (
    event: Event | React.SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotify(false);
  };
  async function fetchData() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    await request<ServiceResult>("GET", `/sp/service`, {
      page: pageNumber,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.serviceInfos.map((item) => item));
        // console.log("fetch");
        setTotalPage(data.totalPage);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/serviceappraisallist", { replace: true });
      });
    setLoading(true);
  }
  async function filterByShop() {
    let token: string | null = localStorage.getItem(ACCESS_TOKEN);
    if (!token) {
      setLoading(true);
      return Promise.reject("No access token set.");
    }
    await request<ServiceResult>("GET", `/sp/service/shop/` + shopId, {
      page: pageNumber,
      shopId: shopId,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.serviceInfos.map((item) => item));
        setShopName(data.serviceInfos[0].shopShopName);
        setTotalPage(data.totalPage);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/serviceappraisallist", { replace: true });
      });
    setLoading(true);
  }
  useEffect(() => {
    setLoading(false);
    // console.log(param);
    // console.log(shopId);
    isFilter ? filter() : shopId === undefined ? fetchData() : filterByShop();
    return () => {
      setRows([]);
    };
  }, [pageNumber, shopId]);
  async function search() {
    await request<ServiceResult>("GET", `/sp/service/search`, {
      page: pageNumber,
      name: searchText,
      size: 6,
    })
      .then((res) => {
        let data = res.data;
        setRows(data.serviceInfos.map((item) => item));
        setTotalPage(data.totalPage);
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/serviceappraisallist", { replace: true });
      });
    // fetchData();
  }
  async function filter() {
    let response = await request<ServiceResult>(
      "POST",
      `/sp/mod/service`,
      // null,
      {
        page: pageNumber,
        size: 6,
      },
      {
        shopId: shopId === undefined ? 0 : shopId,
        statuses: filterValue,
      }
    )
      .then((res) => {
        let data = res.data;
        setRows(data.serviceInfos.map((item) => item));
        setTotalPage(data.totalPage);
        setIsFilter(true);
      })
      .catch((error) => {
        console.log("Error: ", error);
        shopId === undefined
          ? navigate("/manager/serviceappraisallist", { replace: true })
          : navigate("/manager/serviceappraisallist/" + shopId, {
              replace: true,
            });
      });
    setLoading(true);
    // fetchData();
  }
  async function verifyService(reason: string, id: number, status: string) {
    await request<ServiceResult>("PUT", `/sp/verify/service/` + id, null, {
      refuseReason: reason,
      serviceId: id,
      status: status,
    })
      .then(() => {
        setOpenNotify(true);
        filterValue.length === 0 ? fetchData() : filter();
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/serviceappraisallist", { replace: true });
      });
    setLoading(true);
    // fetchData();
  }
  async function lockService(id: number, status: string) {
    await request<ServiceResult>("PUT", `/sp/mod/service/` + id, {
      serviceId: id,
      status: status,
    })
      .then(() => {
        setOpenNotify(true);
        filterValue.length === 0 ? fetchData() : filter();
      })
      .catch((error) => {
        console.log("Error: ", error);
        navigate("/manager/serviceappraisallist", { replace: true });
      });
    setLoading(true);
    // fetchData();
  }
  const [open, setOpen] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [statusList, setStatusList] = useState([
    { id: 1, title: "WAITING", isChoose: false },
    { id: 2, title: "ACTIVE", isChoose: false },
    { id: 3, title: "UNAVAILABLE", isChoose: false },
    { id: 4, title: "DELETED", isChoose: false },
    { id: 5, title: "BLOCKED", isChoose: false },
    { id: 6, title: "REJECT", isChoose: false },
  ] as StatusType[]);
  const [categoryList, setCategoryList] = useState([
    { id: 1, title: "Điểm tham quan và vui chơi", isChoose: false },
    { id: 2, title: "Dịch vụ ăn uống", isChoose: false },
    { id: 3, title: "Hoạt động thể thao", isChoose: false },
    { id: 4, title: "Phương tiện di chuyển", isChoose: false },
    { id: 5, title: "Sự kiện", isChoose: false },
    { id: 6, title: "Thư giãn và làm đẹp", isChoose: false },
  ] as StatusType[]);
  const [filterValue, setFilterValue] = useState([] as string[]);
  const [refuseId, setRefuseId] = useState(-1);
  const [refuseReason, setRefuseReason] = useState("");
  const [isEmpty, setIsEmpty] = useState(false);
  const [searchText, setSearchText] = useState("");
  const handleClickOpen = () => {
    setOpen(open ? false : true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setIsEmpty(false);
  };
  const handleAccept = (id: number) => {
    setLoading(false);
    verifyService("", id, "ACTIVE");
  };
  const handleRefuse = (id: number) => {
    setRefuseReason("");
    setOpenDialog(true);
    setRefuseId(id);
  };
  const handleConfirmRefuse = () => {
    // console.log("call");
    if (refuseReason !== "") {
      setLoading(false);
      verifyService(refuseReason, refuseId, "REJECT");
      setOpenDialog(false);
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  };
  const handleRefuseReason = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRefuseReason(e.target.value);
  };
  const handleSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    let text = e.target.value;
    setSearchText(text);
    if (text === "") {
      fetchData();
      // console.log("searchText");
    }
  };
  const handleEnter = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (e.key === "Enter") search();
  };
  const handleStatus = (id: number) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleCategory = (id: number) => {
    setCategoryList((prev) =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (!item.isChoose) {
            setFilterValue(filterValue.concat(item.title));
            return [...ack, { ...item, isChoose: true }];
          } else {
            const arr = filterValue.filter((val) => val !== item.title);
            setFilterValue(arr);
            return [...ack, { ...item, isChoose: false }];
          }
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleRemoveOption = (title: string) => {
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        if (item.title === title) {
          const arr = filterValue.filter((val) => val !== item.title);
          setFilterValue(arr);
          return [...ack, { ...item, isChoose: false }];
        } else {
          return [...ack, item];
        }
      }, [] as StatusType[])
    );
  };
  const handleApplyFilter = () => {
    setLoading(false);
    setOpen(false);
    if (filterValue.length !== 0) {
      setPageNumber(1);
      filter();
    } else {
      setIsFilter(false);
      shopId === undefined ? fetchData() : filterByShop();
    }
  };
  let navigate = useNavigate();
  const handleViewDetail = (id: number) => {
    let path = "/service/" + id;
    navigate(path);
  };
  const handleRemoveAllFilter = () => {
    setLoading(false);
    setOpen(false);
    setStatusList((prev) =>
      prev.reduce((ack, item) => {
        // const arr = filterValue.filter((val) => val !== item.title);
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setCategoryList((prev) =>
      prev.reduce((ack, item) => {
        return [...ack, { ...item, isChoose: false }];
      }, [] as StatusType[])
    );
    setFilterValue([]);
    shopId === undefined ? fetchData() : filterByShop();
  };
  const handleChangePage = (page: number) => {
    // console.log("page", page);
    setPageNumber(page);
  };
  const handleRemoveShop = () => {
    let path = "/manager/serviceappraisallist";
    navigate(path);
  };
  const theme = createTheme({
    palette: {
      primary: { main: variables.colorMain },
      info: { main: "#fff" },
    },
    components: {
      MuiTableHead: {
        styleOverrides: {
          root: {
            backgroundColor: variables.colorMain,
            position: "sticky",
            top: 0,
          },
        },
      },
    },
  });
  const StyledTableRow = styled(TableRow)(() => ({
    "&:nth-of-type(even)": {
      backgroundColor: variables.colorRowOdd,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));
  return (
    // <div className="">
    <ThemeProvider theme={theme}>
      <Container className="service-appraisal-list">
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <div className="div-title">
          <h2>Danh sách dịch vụ</h2>
          {shopId !== undefined ? (
            <div className="shop-name">
              Cửa hàng: {shopName !== "" ? shopName : null}
              <IconButton
                onClick={() => handleRemoveShop()}
                className="btn-close"
              >
                <ClearIcon fontSize="small" />
              </IconButton>
            </div>
          ) : null}
        </div>
        <div className="search-filter">
          <div className="search-bar">
            <IconButton onClick={search}>
              <SearchIcon className="search-icon" />
            </IconButton>
            <InputBase
              type="search"
              style={{ width: 400 }}
              placeholder="Tìm kiếm theo tên dịch vụ"
              value={searchText}
              onChange={handleSearchText}
              onKeyDown={handleEnter}
            />
          </div>
          <Button
            className="btn-filter"
            variant="contained"
            onClick={handleClickOpen}
          >
            <FilterAlt />
            Bộ lọc
          </Button>
        </div>
        <div className="filter-box" hidden={open ? false : true}>
          <div className="filter-container">
            <div className="filter-left">
              {filterValue.length !== 0 ? (
                <div className="filter-value-container">
                  <p className="selected-title">Đã chọn:</p>
                  {filterValue.map((statusValue) => (
                    <div className="filter-value" key={statusValue}>
                      <p>{statusValue}</p>{" "}
                      <IconButton
                        onClick={() => handleRemoveOption(statusValue)}
                        className="btn-close"
                      >
                        <ClearIcon fontSize="small" />
                      </IconButton>
                    </div>
                  ))}
                  {/* </div> */}
                  <Button
                    className="remove-all-filter"
                    onClick={handleRemoveAllFilter}
                  >
                    Xóa tất cả
                  </Button>
                </div>
              ) : null}
              <div className="filter-options">
                <p className="status-title">Tình trạng</p>
                {statusList.map((status) => (
                  <Button
                    key={status.id}
                    className={
                      status.isChoose
                        ? "btn-filter-option isChoose"
                        : "btn-filter-option"
                    }
                    variant="contained"
                    color="info"
                    onClick={() => handleStatus(status.id)}
                  >
                    {status.title}
                  </Button>
                ))}
                <br />
                {/* <p className="status-title">Danh mục</p>
                {categoryList.map((category) => (
                  <Button
                    key={category.id}
                    className={
                      category.isChoose
                        ? "btn-filter-option isChoose"
                        : "btn-filter-option"
                    }
                    variant="contained"
                    color="info"
                    onClick={() => handleCategory(category.id)}
                  >
                    {category.title}
                  </Button>
                ))} */}
              </div>
            </div>
            <div className="filter-right">
              <IconButton onClick={handleClose} className="btn-close">
                <ClearIcon fontSize="small" />
              </IconButton>
              <br />
              <br />
              <Button
                className="btn-apply-filter"
                variant="contained"
                onClick={handleApplyFilter}
              >
                Lọc
              </Button>
            </div>
          </div>
        </div>

        <div className="table-container">
          <TableContainer className="container-table">
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell width={"50"}>ID</TableCell>
                  {/* <TableCell width={"120"} align="center">
                    Hình ảnh
                  </TableCell> */}
                  <TableCell width={"200"}>Tên dịch vụ</TableCell>
                  <TableCell width={"200"}>Danh mục</TableCell>
                  <TableCell width={"200"}>Cửa hàng</TableCell>
                  <TableCell width={"170"}>Ngày tạo</TableCell>
                  <TableCell width={"150"} align="center">
                    Tình trạng
                  </TableCell>
                  <TableCell width={"150"} align="center">
                    Tùy chọn
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {rows.length === 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <h3>Không có dịch vụ nào</h3>
                    </TableCell>
                  </TableRow>
                ) : (
                  rows.map((row) => (
                    <StyledTableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.id}
                      </TableCell>
                      {/* <TableCell className="cell-img" align="center">
                        <img src={row.image} />
                      </TableCell> */}
                      <TableCell>
                        <div className="long-text">{row.title}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.categoryName}</div>
                      </TableCell>
                      <TableCell>
                        <div className="long-text">{row.shopShopName}</div>
                      </TableCell>
                      <TableCell>
                        {moment(row.createdAt).format("hh:mm DD-MM-YYYY")}
                      </TableCell>
                      <TableCell align="center">{row.status}</TableCell>
                      <TableCell align="center" style={{ display: "flex" }}>
                        <Button
                          className="btn-act"
                          onClick={() => handleViewDetail(row.id)}
                          title="Chi tiết"
                        >
                          <InfoIcon />
                        </Button>

                        {row.status === "WAITING" ? (
                          <MenuComponent
                            rowId={row.id}
                            handleAccept={handleAccept}
                            handleRefuse={handleRefuse}
                          />
                        ) : null}
                        {row.status === "BLOCKED" ? (
                          <Button
                            onClick={() => lockService(row.id, "ACTIVE")}
                            title="Mở khóa"
                          >
                            <LockOpenIcon />
                          </Button>
                        ) : null}
                        {row.status === "ACTIVE" ? (
                          <Button
                            onClick={() => lockService(row.id, "BLOCKED")}
                            title="Khóa"
                          >
                            <LockIcon />
                          </Button>
                        ) : null}
                      </TableCell>
                    </StyledTableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Pagination
              className="table-pagination"
              count={totalPage}
              page={pageNumber}
              onChange={(event: React.ChangeEvent<unknown>, page: number) =>
                handleChangePage(page)
              }
              variant="outlined"
              color="primary"
              shape="rounded"
            />
          </TableContainer>
          {/* </Box> */}
        </div>
      </Container>
      <Dialog
        open={openDialog}
        fullWidth
        onClose={handleCloseDialog}
        className="dialog-container"
      >
        <DialogTitle>Từ chối dịch vụ</DialogTitle>
        <DialogContent>
          <InputLabel>Lý do</InputLabel>
          <TextField
            autoFocus
            margin="dense"
            fullWidth
            variant="standard"
            value={refuseReason}
            error={isEmpty}
            helperText={isEmpty ? "Vui lòng điền vào trường này." : ""}
            onChange={handleRefuseReason}
          />
        </DialogContent>
        <DialogActions>
          <Button type="submit" onClick={handleConfirmRefuse}>
            Xác nhận
          </Button>
          <Button onClick={handleCloseDialog} name="cancel">
            Hủy
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={openNotify}
        autoHideDuration={1000}
        onClose={handleCloseNotify}
        message="Cập nhật thành công!"
        // action={action}
      />
    </ThemeProvider>
    // </div>
  );
};

export default ServiceAppraisalList;
