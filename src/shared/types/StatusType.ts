export interface StatusType {
  id: number;
  title: string;
  isChoose: boolean;
}
