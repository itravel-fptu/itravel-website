export interface ShopType {
  id: number;
  avatar: string;
  description: string;
  name: string;
  serviceCount: number;
  createdAt: string;
}

export interface IShopDetail {
  address: string;
  avatar: string;
  createdAt: string;
  description: string;
  id: number;
  ownerId: number;
  ownerName: string;
  serviceCount: number;
  shopName: string;
  status: string;
}
