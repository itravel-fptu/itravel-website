export interface INotification {
  id: number;
  title: string;
  message: string;
  hasSeen: boolean;
  createdAt: string;
  receiverId: number;
}
