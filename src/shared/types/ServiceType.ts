import { SERVICE_STATUS } from "../constants/status";

export type People = {
  name: string;
  age: number;
};

export interface CategoryCardType {
  id: number;
  title: string;
  description: string;
  background_path: string;
}

export type ServiceCardType = {
  id: number;
  title: string;
  cityName: string;
  categoryName: string;
  lowestPrice?: number;
  rateAverage: number;
  rateCount: number;
  images: string[];
  shopShopName: string;
};

export type ServiceType = {
  id: number;
  shopId: number;
  title: string;
  description: string;
  cityName: string;
  categoryName: string;
  status: string;
  price: number;
  rateAverage: number;
  rateCount: number;
  createdAt: string;
};

export type NewServiceType = {
  title: string;
  description: string;
  address: string;
  cityId: number;
  eventStart: string;
  eventEnd: string;
  categoryId: number;
  duration: number;
  images: string[];
  subServices: NewSubServiceType[];
};

export type NewSubServiceType = {
  title: string;
  price: number;
  stockAmount: number;
};
export interface ISubService {
  id: number;
  mainServiceId?: number;
  title: string;
  price: number;
  stockAmount: number;
}
export interface IUpdateSubService extends ISubService {
  action: "update" | "delete";
}


export type UpdateServiceType = {
  address: string;
  categoryId: number;
  cityId: number;
  description: string;
  eventEnd: string;
  eventStart: string;
  duration: number;
  images: string[];
  status: string;
  subServices: NewSubServiceType[];
  title: string;
};