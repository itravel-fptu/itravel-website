export type CategoryType = {
    id: number;
    title: string;
    description: string;
    background_path: string;
}