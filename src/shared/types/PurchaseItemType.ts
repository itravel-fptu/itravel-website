export type PurchaseItemType = {
  createdAt: Date;
  dateEnd: Date;
  dateStart: Date | null;
  id: number;
  image: string;
  mainServiceId: number;
  mainServiceShopName: string;
  mainServiceTitle: string;
  price: number;
  quantity: number;
  subServiceId: number;
  subServiceTitle: string;
  // isChecked: boolean;
  // isOutOfDate: boolean;
};
