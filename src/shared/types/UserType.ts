export interface IUser {
  id: 0;
  fullName: string;
  birthday: string;
  gender: string;
  email: string;
  phoneNumber: string;
  roles: IRole[];
  status: string;
  imageLink: string;
  modifiedAt: string;
  createdAt: string;
}

export interface IRole {
  id: number;
  roleName: string;
}