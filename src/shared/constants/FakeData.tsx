import { CategoryCardType, ServiceCardType } from "./../types/ServiceType";

export const categories: CategoryCardType[] = [
  {
    id: 1,
    title: "Điểm tham quan & vui chơi",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path:
      "https://i.insider.com/5d38b0b336e03c401422cdf8?width=750&format=jpeg&auto=webp",
  },
  {
    id: 2,
    title: "Dịch vụ ăn uống",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",

    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 3,
    title: "Hoạt động thể thao",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 4,
    title: "Phương tiện di chuyển",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 5,
    title: "Sự kiện",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
  {
    id: 6,
    title: "Thư giãn & làm đẹp",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    background_path: "https://wallpaperaccess.com/full/309407.jpg",
  },
];

export const travelServices: ServiceCardType[] = [
  {
    id: 1,
    title: "1. Vé vào cổng Bà Nà Bà Nà",
    cityName: "Đà Nẵng, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 2,
    title: "2. Tour tham quan vòng quanh Đà Nẵng bằng xe buýt 2 tầng",
    cityName: "Đà Nẵng",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 3,
    title: "3. Tour 1 ngày farmstay Hội An",
    cityName: "Hội An, Quảng Nam, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 4,
    title: "4. Vé vào cổng Bà Nà",
    cityName: "Đà Nẵng",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 5,
    title: "5. Vé vào cổng Bà Nà",
    cityName: "Đà Nẵng",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 6,
    title: "6. Vé vào cổng Bà Nà",
    cityName: "Đà Nẵng",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 7,
    title: "7. Vé vào cổng Bà Nà Bà Nà",
    cityName: "Đà Nẵng, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 8,
    title: "8. Day la title test",
    cityName: "Đà Nẵng, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 9,
    title: "9. Vé vào cổng Bà Nà Bà Nà",
    cityName: "Đà Nẵng, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
  {
    id: 10,
    title: "10. Vé vào cổng Bà Nà Bà Nà",
    cityName: "Đà Nẵng, Việt Nam",
    categoryName: "Điểm tham quan & vui chơi",
    rateAverage: 4.3,
    rateCount: 10,
    images: [
      "https://www.vietnamonline.com/media/uploads/froala_editor/images/VNO-Sun%20World%20Ba%20Na%20Hills.jpg",
    ],
    shopShopName: "Froala",
  },
];
