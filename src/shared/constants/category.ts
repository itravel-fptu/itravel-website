import { CategoryType } from "../types/CategoryType";
const category1 = require("../../assets/images/category1.jpeg");
const category2 = require("../../assets/images/category2.png");
const category3 = require("../../assets/images/category3.png");
const category4 = require("../../assets/images/category4.png");
const category5 = require("../../assets/images/category5.png");
const category6 = require("../../assets/images/category6.png");

export const CATEGORIES: CategoryType[] = [
  {
    id: 1,
    title: "Điểm tham quan & vui chơi",
    description:
      "Vé vào cửa khu vui chơi và giải trí.",
    background_path: category1,
  },
  {
    id: 2,
    title: "Dịch vụ ăn uống",
    description:
      "Trải nghiệm đồ ăn và thức uống đặc sản tuyệt vời của địa phương mà bạn ghé thăm.",

    background_path: category2,
  },
  {
    id: 3,
    title: "Hoạt động thể thao",
    description:
      "Tham gia các hoạt động thể thao trên trời và dưới biển.",
    background_path: category3,
  },
  {
    id: 4,
    title: "Phương tiện di chuyển",
    description:
      "Di chuyển dễ dàng giữa các địa điểm với chi phí tốt nhất.",
    background_path: category4,
  },
  {
    id: 5,
    title: "Sự kiện",
    description:
      "Các sự kiện nổi bật trong thời gian sắp tới đang chờ đón bạn.",
    background_path: category5,
  },
  {
    id: 6,
    title: "Thư giãn & làm đẹp",
    description:
      "Thư giãn và chăm sóc bản thân với những dịch vụ chất lượng nhất.",
    background_path: category6,
  },
];
