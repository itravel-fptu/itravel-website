export const SERVICE_STATUS = {
  WAITING: "Chờ phê duyệt",
  ACTIVE: "Đang hoạt động",
  UNAVAILABLE: "Không khả dụng",
  DELETED: "Đã xoá",
  BLOCKED: "Bị khoá",
  REJECT: "Bị từ chối"
};

export const SHOP_STATUS = {
  ACTIVE: "Đang hoạt động",
  DELETED: "Bị xoá",
  LOCKED: "Bị khoá",
};

export const ACCOUNT_STATUS = {
  ACTIVE: "Đang hoạt động",
  DELETED: "Đã xoá",
  LOCKED: "Đã khoá",
  BLOCKED: "Bị khoá",
};

export const ORDER_STATUS = {
  NEW: "Khởi tạo",
  PROCESSING: "Đang xử lý",
  FINISHED: "Đã hoàn thành",
  REFUND: "Đã hoàn tiền",
  CANCEL: "Bị huỷ bỏ",
};
