import { ACCESS_TOKEN, API_BASE_URL } from "../../constants";
import request from "./ApiService";

export function getCurrentUser<T>() {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);
  
  if (!token) {
    return Promise.reject("No access token set.");
  }

  let url = `/account`;
  return request<T>("GET", url);
}