import { ACCESS_TOKEN } from "../../constants";
import { ServiceCardType } from "../types/ServiceType";
import request from "./ApiService";

export type SearchResultType = {
  page: number;
  serviceInfos: ServiceCardType[];
  size: number;
  totalItem: number;
  totalPage: number;
};

export const searchServiceByKeyword = async (
  searchValue: string,
  size: number
): Promise<SearchResultType> => {
  let token: string | null = localStorage.getItem(ACCESS_TOKEN);

  const params = {
    name: searchValue,
    size: 10,
  };

  const url = `/sp/service/search`;

  const results = await request<SearchResultType>("GET", url, params).then(
    (res) => res.data
  );

  return results;
};
