import PersonIcon from "@mui/icons-material/Person";
import StoreIcon from "@mui/icons-material/Store";
import TravelExploreIcon from "@mui/icons-material/TravelExplore";
import DescriptionIcon from "@mui/icons-material/Description";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import "./sidebar-mod.scss";
import { Link, useLocation } from "react-router-dom";

type SideMod = {
  isAdmin: boolean;
};
const NavBarMod: React.FC<SideMod> = ({ isAdmin }) => {
  var location = useLocation();
  const activeTab = (path: string[]) => {
    if (path.includes(location.pathname)) {
      return "menu-item active";
    }
    return "menu-item";
  };
  return (
    <div className="nav-bar">
      <div className="nav-content">
        <div className="menu">
          {isAdmin ? (
            <div className={activeTab(["/manager/modlist"])}>
              <ManageAccountsIcon />
              <Link to="/manager/modlist">Danh sách quản lý</Link>
            </div>
          ) : null}
          <div className={activeTab(["/manager", "/manager/userlist"])}>
            <PersonIcon />
            <Link to="/manager/userlist">Danh sách người dùng</Link>
          </div>
          <div className={activeTab(["/manager/shoplist"])}>
            <StoreIcon />
            <Link to="/manager/shoplist">Danh sách cửa hàng</Link>
          </div>
          <div className={activeTab(["/manager/serviceappraisallist"])}>
            <TravelExploreIcon />
            <Link to="/manager/serviceappraisallist">Danh sách dịch vụ</Link>
          </div>
          <div className={activeTab(["/manager/refundrequestlist"])}>
            <DescriptionIcon />
            <Link to="/manager/refundrequestlist">
              Danh sách yêu cầu hoàn trả
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBarMod;
