import { useState } from "react";
import { Link } from "react-router-dom";
import useAuth from "../../../hooks/useAuth";
// MUI assets
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
import Avatar from "@mui/material/Avatar";
// Assets
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import AssignmentIcon from "@mui/icons-material/Assignment";
// Styles
import "./seller-sidebar.scss";

const SellerSidebar: React.FC = () => {
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  let auth = useAuth();

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={open ? "seller-sidebar" : "seller-sidebar-small"}>
      {open ? (
        <CloseIcon onClick={handleClose} className={"close-icon"} />
      ) : (
        <MenuIcon onClick={handleClickOpen} className="open-icon" />
      )}
      <br></br>
      <div className="nav-content">
        <div className="user">
          {auth.user ? (
            <img className="user__avatar" src={auth.user.imageLink} />
          ) : (
            <Avatar className="user__avatar" />
          )}
          <p className="user__name">
            {auth.user ? auth.user.fullName : "Người bán"}
          </p>
        </div>
        <hr></hr>
        <div className="menu">
          <div className="menu-item">
            <div className="menu-title">
              <LibraryBooksIcon className="menu-icon" />
              <p>Quản lý dịch vụ</p>
            </div>
            <div className="link">
              <Link to="service/list">Danh sách dịch vụ</Link>
            </div>
            <div className="link">
              <Link to="service/new">Thêm dịch vụ mới</Link>
            </div>
          </div>
          <div className="menu-item">
            <div className="menu-title">
              <AssignmentIcon className="menu-icon" />
              <p>Quản lý đơn hàng</p>
            </div>
            <Link to="order/list" className="link">
              Danh sách đơn hàng
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SellerSidebar;
