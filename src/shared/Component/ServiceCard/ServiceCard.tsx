// Components

// Types
import { ServiceCardType } from "../../types/ServiceType";

// Styles
import "./service-card.scss";

// Assets
import StarIcon from "@mui/icons-material/Star";

type ServiceCardProps = {
  service: ServiceCardType;
};

const ServiceCard: React.FC<ServiceCardProps> = ({ service }) => {
  const resultImagePlaceholder = require("./../../../assets/images/service-placeholder-image.jpg");

  return (
    <div className="service-card">
      <img
        className="card-image"
        src={service.images[0] ? service.images[0] : resultImagePlaceholder}
        alt={service.title}
      />
      <div className="card-content flex-column">
        <div className="card-content__title">
          <h3>{service.title}</h3>
        </div>
        <p
          style={{
            marginBottom: "5px",
            height: "20px",
            fontSize: "14px",
          }}
        >
          {service.shopShopName}
        </p>
        <div className="card-content__info flex-column">
          <div>{service.cityName}</div>
          <div className="rating flex-row flex-ai-c">
            <StarIcon sx={{ color: "#ffcd3c", ml: "-4px" }} />
            <span className="rating-score">{service.rateAverage}</span>
            <span className="rating-count">{service.rateCount} đánh giá</span>
          </div>
          <div className="price">
            <p>Giá chỉ từ đ {service.lowestPrice?.toLocaleString("vi-VN")}</p>
          </div>
        </div>
        <div></div>
      </div>
    </div>
  );
};

export default ServiceCard;
