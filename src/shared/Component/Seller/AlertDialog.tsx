import "./alert-dialog.scss";
// MUI components
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
// MUI assets
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import ErrorIcon from "@mui/icons-material/Error";

type AlertDialogPropsType = {
  type: "success" | "error";
  open: boolean;
  title?: string;
  message: string;
  handleConfirmClick: () => void;
};

const AlertDialog: React.FC<AlertDialogPropsType> = ({
  type,
  open,
  title,
  message,
  handleConfirmClick,
}) => {
  return (
    <div className="alert-dialog">
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth
      >
        <DialogTitle id="alert-dialog-title">
          <Stack className="alert-dialog__title">
            {type === "success" ? (
              <CheckCircleIcon color="success" className="alert-dialog__icon" />
            ) : (
              ""
            )}
            {type === "error" ? (
              <ErrorIcon color="error" className="alert-dialog__icon" />
            ) : (
              ""
            )}
            {type === "success" ? (title ? title : "Thành công") : ""}
            {type === "error" ? (title ? title : "Ooops") : ""}
          </Stack>
        </DialogTitle>
        <DialogContent className="alert-dialog__content">
          <DialogContentText id="alert-dialog-description">
            {message}
          </DialogContentText>
        </DialogContent>
        <DialogActions className="alert-dialog__actions">
          {type === "success" ? (
            <Button variant="contained" onClick={handleConfirmClick} autoFocus>
              Xác nhận
            </Button>
          ) : (
            ""
          )}
          {type === "error" ? (
            <Button
              variant="contained"
              color="error"
              onClick={handleConfirmClick}
              autoFocus
            >
              Đã hiểu
            </Button>
          ) : (
            ""
          )}
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AlertDialog;
