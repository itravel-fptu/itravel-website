import { Alert, Snackbar } from "@mui/material";

type ToastMessagePropsType = {
  open: boolean;
  message: string;
  handleClose: () => void;
};

const ToastMessage: React.FC<ToastMessagePropsType> = ({
  open,
  message,
  handleClose,
}) => {
  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default ToastMessage;
