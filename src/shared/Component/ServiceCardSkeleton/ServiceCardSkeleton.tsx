import { Skeleton } from "@mui/material";
import "./service-card-skeleton.scss";

const ServiceCardSkeleton = () => {
  return (
    <div className="service-card-skeleton">
      <Skeleton variant="rectangular" className="card-image" />
      <div className="card-content">
        <Skeleton variant="text" width="90%" />
        <Skeleton variant="text" width="90%" />
        <Skeleton variant="text" width="65%" />
      </div>
    </div>
  );
};

export default ServiceCardSkeleton;
