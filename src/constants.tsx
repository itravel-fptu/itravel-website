const keys = require("./config/keys");

const LANGUAGE: string = "vi-VN";
const ACCESS_TOKEN: string = "accessToken";

const FACEBOOK_APP_ID: string = process.env.REACT_APP_FACEBOOK_APP_ID || "";
const FACEBOOK_PAGE_ID: string = "108656715151707";

const API_BASE_URL: string = keys.netlifyBaseUrl;
const OAUTH2_REDIRECT_URI: string = keys.redirectUri + "/oauth2/redirect";

const GOOGLE_BASE_URL: string = keys.apiBaseUrl;
const GOOGLE_AUTH_URL: string = `${GOOGLE_BASE_URL}/auth/login?redirectUri=${OAUTH2_REDIRECT_URI}`;
// const GOOGLE_AUTH_URL: string = `${API_BASE_URL}/oauth2/authorization/google?redirectUri=${OAUTH2_REDIRECT_URI}`;

const PAYMENT_REDIRECT_URI: string = keys.redirectUri + "/payment/redirect";
export {
  LANGUAGE,
  ACCESS_TOKEN,
  FACEBOOK_APP_ID,
  FACEBOOK_PAGE_ID,
  API_BASE_URL,
  OAUTH2_REDIRECT_URI,
  GOOGLE_AUTH_URL,
  PAYMENT_REDIRECT_URI,
};
