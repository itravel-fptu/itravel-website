import { ReactElement, useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import { GOOGLE_AUTH_URL } from "../../constants";
// Components
import NotificationList from "../../features/NotificationList";
import AccountMenu from "./AccountMenu";
// MUI
import { TextField } from "@mui/material";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
// Styles & Assets
import { ReactComponent as Logo } from "./../../assets/images/icon.svg";
import iTravelLogo from "./../../assets/images/icon-logo.png";
import "./header.scss";
import { useQuery } from "react-query";
import {
  SearchResultType,
  searchServiceByKeyword,
} from "../../shared/services/SearchService";
const resultImagePlaceholder = require("./../../assets/images/service-placeholder-image.jpg");

const Header: React.FC = (): ReactElement => {
  let location = useLocation();
  let navigate = useNavigate();
  let auth = useAuth();
  const [searchValue, setSearchValue] = useState("");
  const [isShowSearchPanel, setIsShowSearchPanel] = useState(false);

  const {
    data: searchResult,
    refetch,
    isLoading: isSearchLoading,
  } = useQuery<SearchResultType>(
    "searchResult",
    () => searchServiceByKeyword(searchValue, 10),
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  );

  const handleLogin = () => {
    sessionStorage.setItem("FROM", location.pathname);
    window.location.replace(GOOGLE_AUTH_URL);
  };

  const handleLogout = () => {
    auth.logout(() => {
      navigate("/", { replace: true });
    });
  };

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };

  const handleEnterSearch = (event: any) => {
    if (event.key === "Enter") {
      navigate(`/search?keyword=${event.target.value}`);
      setSearchValue("");
    }
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (searchValue !== "") {
        refetch();
      } else {
        setIsShowSearchPanel(false);
      }
    }, 1500);
    return () => clearTimeout(delayDebounceFn);
  }, [searchValue]);

  return (
    <div id="header" className="flex-row flex-ai-c flex-jc-sb">
      <Link to="/">
        <Logo className="header__logo" />
        {/* <img src={iTravelLogo} alt="logo" className="header__logo" /> */}
      </Link>
      <div className="search hide-for-mobile">
        <TextField
          className="search__textfield"
          value={searchValue}
          size="small"
          id="outlined-basic"
          variant="outlined"
          placeholder="Tìm kiếm"
          autoComplete="off"
          onChange={handleSearch}
          onKeyDown={handleEnterSearch}
          onFocus={() => setIsShowSearchPanel(true)}
          onBlur={() => setIsShowSearchPanel(false)}
        />
        <div
          className="search-results"
          style={
            isShowSearchPanel
              ? searchResult
                ? { display: "initial" }
                : { display: "none" }
              : { display: "none" }
          }
        >
          <div className="search-results__list">
            {searchResult &&
              searchResult?.serviceInfos.map((service) => (
                <Link to={`/service/${service.id}`} key={service.id}>
                  <div
                    className="search-results__item flex-row"
                    key={service.id}
                  >
                    <img
                      src={
                        service.images[0]
                          ? service.images[0]
                          : resultImagePlaceholder
                      }
                      alt={service.title}
                    />
                    <div className="info">
                      <div className="info-category">
                        {service.categoryName}
                      </div>
                      <div className="info-title">{service.title}</div>
                      <div className="info-city">{service.cityName}</div>
                    </div>
                  </div>
                </Link>
              ))}
            {searchResult?.totalItem === 0 && (
              <div className="search-results__item-empty">{"Không có kết quả trùng khớp :("}</div>
            )}
          </div>
        </div>
      </div>
      <div className="right flex-row flex-ai-c">
        {auth.user && (
          <div className="cart-icon">
            <Link to="/cart">
              <ShoppingCartIcon />
            </Link>
          </div>
        )}
        {auth.user && <NotificationList />}

        <AccountMenu
          user={auth.user}
          handleLogin={handleLogin}
          handleLogout={handleLogout}
        />
      </div>
    </div>
  );
};

export default Header;
