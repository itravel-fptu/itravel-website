import { Google, Logout, Person, Store, Groups } from "@mui/icons-material";
import {
  Avatar,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { IUser } from "../../shared/types/UserType";

type AccountMenuType = {
  user: IUser | null;
  handleLogin: () => void;
  handleLogout: () => void;
};

const AccountMenu: React.FC<AccountMenuType> = ({
  user,
  handleLogin,
  handleLogout,
}) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        onClick={handleClick}
        size="small"
        sx={{ ml: 2 }}
        aria-controls={open ? "account-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
      >
        <Avatar sx={{ width: "35px", height: "35px" }}>
          {user && <img src={user.imageLink} className="avatar" />}
        </Avatar>
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        {user && (
          <MenuItem>
            <Link to="/profile" className="flex-row flex-ai-c">
              <ListItemIcon>
                <Person />
              </ListItemIcon>
              Tài khoản của tôi
            </Link>
          </MenuItem>
        )}

        {user &&
          user.roles.filter((role) => role.roleName === "ROLE_SELLER").length >
            0 && (
            <MenuItem>
              <Link to="/seller" className="flex-row flex-ai-c">
                <ListItemIcon>
                  <Store />
                </ListItemIcon>
                Trang cửa hàng
              </Link>
            </MenuItem>
          )}

        {user &&
          (user?.roles.filter((role) => role.roleName === "ROLE_ADMIN").length >
            0 ||
            user?.roles.filter((role) => role.roleName === "ROLE_MODERATOR")
              .length > 0) && (
            <MenuItem>
              <Link to="/manager" className="flex-row flex-ai-c">
                <ListItemIcon>
                  <Groups />
                </ListItemIcon>
                Trang quản lý
              </Link>
            </MenuItem>
          )}

        {user && <hr />}

        {user ? (
          <MenuItem onClick={handleLogout}>
            <ListItemIcon>
              <Logout fontSize="small" />
            </ListItemIcon>
            Đăng xuất
          </MenuItem>
        ) : (
          <MenuItem onClick={handleLogin}>
            <ListItemIcon>
              <Google fontSize="small" />
            </ListItemIcon>
            Đăng nhập với Google
          </MenuItem>
        )}
      </Menu>
    </div>
  );
};

export default AccountMenu;
