import { Container } from "@mui/material";
import { Link } from "react-router-dom";
import "./footer.scss";

const Footer: React.FC = () => {
  return (
    <div className="footer-container">
      <Container>
        <div className="footer-top">
          <div className="div-email">
            <p>
              <b>CÔNG TY TNHH ITRAVEL</b>
            </p>
            <br></br>
            <p>Địa chỉ: Nam kỳ Khởi Nghĩa, khu đô thị FPT, Đà Nẵng, Việt Nam</p>
            <p>Email: iTravel@gmail.com</p>
            <p>Hotline: 0987654321</p>
            {/* <p>Đăng ký ngay để liên tục được cập nhật những ưu đãi tốt nhất</p>
            <input type={"text"} placeholder="Nhập email của bạn"></input>
            <input type={"submit"} value={"Đăng ký"}></input> */}
          </div>
          <div id="div-link">
            <div className="div-block">
              <p>
                <b>VỀ ITRAVEL</b>
              </p>
              <br></br>
              <Link to="/about">Về chúng tôi</Link>
              <Link to="/">Liên hệ</Link>
              <Link to="/">Trợ giúp</Link>
            </div>
            <div className="div-block">
              <p>
                <b>ĐỐI TÁC</b>
              </p>
              <br></br>
              <Link to="/">Đăng ký cửa hàng</Link>
              <Link to="/">Trợ giúp</Link>
            </div>
            <div className="div-block">
              <p>
                <b>ĐIỀU KHOẢN</b>
              </p>
              <br></br>
              <Link to="/">Điều khoản sử dụng</Link>
              <Link to="/">Chính sách bảo mật</Link>
              <Link to="/">Chính sách và quy định</Link>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <p id="p-copyright">COPYRIGHT © 2021 ITRAVEL</p>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
