import Routes from "./routes";
import "./assets/scss/main.scss";
import { useEffect, useState } from "react";
import useAuth from "./hooks/useAuth";
import { useLocation } from "react-router-dom";

function App() {
  let auth = useAuth();
  let location = useLocation();

  useEffect(() => {
    const loadCurrentUser = async () => {
      await auth.loadCurrentlyLoggedInUser(() => {
      });
    };
    loadCurrentUser();
  }, [location]);

  return <Routes />;
}

export default App;
