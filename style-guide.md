# Front-end Style Guide

## Typography

### Body Copy
Font size: 18px, 22px

### Font
- Family:
  [Be Vietnam Pro] https://fonts.google.com/specimen/Be+Vietnam+Pro
  [Roboto] https://fonts.google.com/specimen/Roboto

- Weights: 300, 400, 600
